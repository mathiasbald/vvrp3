-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 17, 2018 at 08:01 AM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `seguradora`
--
CREATE DATABASE IF NOT EXISTS `seguradora` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `seguradora`;

-- --------------------------------------------------------

--
-- Table structure for table `apolices`
--

DROP TABLE IF EXISTS `apolices`;
CREATE TABLE `apolices` (
  `id` int(10) UNSIGNED NOT NULL,
  `numeroapolice` int(15) NOT NULL,
  `datacontratacao` date NOT NULL,
  `premio` double NOT NULL,
  `valorsinistromorte` double NOT NULL,
  `valorsinistroincapacitado` double NOT NULL,
  `ativo` tinyint(1) NOT NULL,
  `idcandidato` int(10) UNSIGNED NOT NULL,
  `idcartaocredito` int(10) UNSIGNED NOT NULL,
  `solicitacaooriginal` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `apolices`
--

TRUNCATE TABLE `apolices`;
--
-- Dumping data for table `apolices`
--

INSERT INTO `apolices` (`id`, `numeroapolice`, `datacontratacao`, `premio`, `valorsinistromorte`, `valorsinistroincapacitado`, `ativo`, `idcandidato`, `idcartaocredito`, `solicitacaooriginal`) VALUES
(2, 1807130001, '2018-07-13', 1, 1, 1, 0, 5, 1, 4);

-- --------------------------------------------------------

--
-- Table structure for table `bandeirascartao`
--

DROP TABLE IF EXISTS `bandeirascartao`;
CREATE TABLE `bandeirascartao` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `bandeirascartao`
--

TRUNCATE TABLE `bandeirascartao`;
--
-- Dumping data for table `bandeirascartao`
--

INSERT INTO `bandeirascartao` (`id`, `nome`) VALUES
(1, 'American Express'),
(2, 'Diners Club'),
(3, 'Visa'),
(4, 'MasterCard'),
(5, 'Elo'),
(6, 'Discover');

-- --------------------------------------------------------

--
-- Table structure for table `beneficiarios`
--

DROP TABLE IF EXISTS `beneficiarios`;
CREATE TABLE `beneficiarios` (
  `idcandidato` int(10) UNSIGNED NOT NULL,
  `idbeneficiario` int(10) UNSIGNED NOT NULL,
  `parentesco` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `beneficiarios`
--

TRUNCATE TABLE `beneficiarios`;
--
-- Dumping data for table `beneficiarios`
--

INSERT INTO `beneficiarios` (`idcandidato`, `idbeneficiario`, `parentesco`) VALUES
(4, 5, 5);

-- --------------------------------------------------------

--
-- Table structure for table `cartaocredito`
--

DROP TABLE IF EXISTS `cartaocredito`;
CREATE TABLE `cartaocredito` (
  `id` int(10) UNSIGNED NOT NULL,
  `numero` bigint(12) NOT NULL,
  `datavencimento` date NOT NULL,
  `nome` varchar(80) NOT NULL,
  `codigoseguranca` smallint(6) NOT NULL,
  `idbandeira` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `cartaocredito`
--

TRUNCATE TABLE `cartaocredito`;
--
-- Dumping data for table `cartaocredito`
--

INSERT INTO `cartaocredito` (`id`, `numero`, `datavencimento`, `nome`, `codigoseguranca`, `idbandeira`) VALUES
(1, 1234123412341234, '2018-07-31', 'CANDIDADO TESTE', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `contatos`
--

DROP TABLE IF EXISTS `contatos`;
CREATE TABLE `contatos` (
  `idcontato` int(11) NOT NULL,
  `nome` varchar(45) NOT NULL,
  `idtipoparentesco` int(10) UNSIGNED NOT NULL,
  `email` varchar(45) NOT NULL,
  `telefone` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `contatos`
--

TRUNCATE TABLE `contatos`;
-- --------------------------------------------------------

--
-- Table structure for table `funcionarios`
--

DROP TABLE IF EXISTS `funcionarios`;
CREATE TABLE `funcionarios` (
  `idpessoa` int(10) UNSIGNED NOT NULL,
  `datacontratacao` date NOT NULL,
  `tipofuncionario` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `funcionarios`
--

TRUNCATE TABLE `funcionarios`;
--
-- Dumping data for table `funcionarios`
--

INSERT INTO `funcionarios` (`idpessoa`, `datacontratacao`, `tipofuncionario`) VALUES
(6, '2017-08-15', 2),
(7, '2017-02-05', 1);

-- --------------------------------------------------------

--
-- Table structure for table `parcelas`
--

DROP TABLE IF EXISTS `parcelas`;
CREATE TABLE `parcelas` (
  `id` int(10) UNSIGNED NOT NULL,
  `valor` double NOT NULL,
  `datavencimento` date NOT NULL,
  `datapgto` date NOT NULL,
  `idapolice` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `parcelas`
--

TRUNCATE TABLE `parcelas`;
-- --------------------------------------------------------

--
-- Table structure for table `pessoas`
--

DROP TABLE IF EXISTS `pessoas`;
CREATE TABLE `pessoas` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(80) NOT NULL,
  `cpf` varchar(14) NOT NULL,
  `email` varchar(100) NOT NULL,
  `telefone` varchar(15) NOT NULL,
  `datanascimento` date NOT NULL,
  `sexo` varchar(1) NOT NULL,
  `cep` varchar(9) NOT NULL,
  `cidade` varchar(50) NOT NULL,
  `endereco` varchar(200) NOT NULL,
  `bairro` varchar(40) NOT NULL,
  `vivo` tinyint(1) NOT NULL,
  `incapacitado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `pessoas`
--

TRUNCATE TABLE `pessoas`;
--
-- Dumping data for table `pessoas`
--

INSERT INTO `pessoas` (`id`, `nome`, `cpf`, `email`, `telefone`, `datanascimento`, `sexo`, `cep`, `cidade`, `endereco`, `bairro`, `vivo`, `incapacitado`) VALUES
(4, 'Candidato Teste 1', '000.000.000-01', 'teste1@teste.com', '(00)00000-0000', '1998-07-04', 'M', '00000-000', 'Toma', 'Rua Lá', 'Cá', 1, 0),
(5, 'Candidato Teste 2', '000.000.000-02', 'teste2@teste.com', '(00)00000-0000', '1990-01-01', 'M', '00000-000', 'Toma', 'Rua Lá', 'Cá', 1, 0),
(6, 'Avaliador teste 1', '000.000.000-03', 'teste3@teste.com', '(00)00000-0000', '1990-01-01', 'M', '00000-000', 'Toma', 'Rua Lá', 'Cá', 1, 0),
(7, 'Corretor teste 1', '000.000.000-04', 'teste4@teste.com', '(00)00000-0000', '1990-01-01', 'M', '00000-000', 'Toma', 'Rua Lá', 'Cá', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `predisposicoes`
--

DROP TABLE IF EXISTS `predisposicoes`;
CREATE TABLE `predisposicoes` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `predisposicoes`
--

TRUNCATE TABLE `predisposicoes`;
--
-- Dumping data for table `predisposicoes`
--

INSERT INTO `predisposicoes` (`id`, `nome`) VALUES
(1, 'Câncer'),
(2, 'Diabetes'),
(3, 'Demência'),
(4, 'Coração'),
(5, 'Hipertensão'),
(6, 'Pulmonar'),
(7, 'Osteoporose'),
(8, 'Degeneração');

-- --------------------------------------------------------

--
-- Table structure for table `problemasaude`
--

DROP TABLE IF EXISTS `problemasaude`;
CREATE TABLE `problemasaude` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(80) NOT NULL,
  `descricao` text NOT NULL,
  `idsolicitacao` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `problemasaude`
--

TRUNCATE TABLE `problemasaude`;
--
-- Dumping data for table `problemasaude`
--

INSERT INTO `problemasaude` (`id`, `nome`, `descricao`, `idsolicitacao`) VALUES
(1, 'Renite', 'Nariz escorre pra caramba', 5),
(24, 'problema 1', 'descricao desse problema aqui ', 26);

-- --------------------------------------------------------

--
-- Table structure for table `sinistros`
--

DROP TABLE IF EXISTS `sinistros`;
CREATE TABLE `sinistros` (
  `id` int(10) UNSIGNED NOT NULL,
  `idapolice` int(10) UNSIGNED NOT NULL,
  `idcontato` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `descricao` text,
  `tiposinistro` int(10) UNSIGNED NOT NULL,
  `autorizado` tinyint(1) DEFAULT NULL,
  `pareceravaliador` text NOT NULL,
  `idavaliador` int(10) UNSIGNED DEFAULT NULL,
  `certidao` blob
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `sinistros`
--

TRUNCATE TABLE `sinistros`;
-- --------------------------------------------------------

--
-- Table structure for table `solicitacoes`
--

DROP TABLE IF EXISTS `solicitacoes`;
CREATE TABLE `solicitacoes` (
  `id` int(10) UNSIGNED NOT NULL,
  `numerosolicitacao` bigint(15) UNSIGNED DEFAULT NULL,
  `datasolicitacao` date NOT NULL,
  `valorsolicitacao` decimal(10,2) NOT NULL,
  `valorpremio` decimal(10,2) NOT NULL,
  `datavisitacandidato` datetime DEFAULT NULL,
  `aprovadasolicitacao` tinyint(1) DEFAULT NULL,
  `motivoreprovacao` text,
  `fumante` tinyint(1) DEFAULT NULL,
  `alcoolista` tinyint(1) DEFAULT NULL,
  `idpessoa` int(10) UNSIGNED NOT NULL,
  `isvalido` tinyint(1) NOT NULL DEFAULT '1',
  `idalterador` int(11) DEFAULT NULL,
  `idsolicitacaooriginal` int(11) DEFAULT NULL,
  `dataalteracao` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `solicitacoes`
--

TRUNCATE TABLE `solicitacoes`;
--
-- Dumping data for table `solicitacoes`
--

INSERT INTO `solicitacoes` (`id`, `numerosolicitacao`, `datasolicitacao`, `valorsolicitacao`, `valorpremio`, `datavisitacandidato`, `aprovadasolicitacao`, `motivoreprovacao`, `fumante`, `alcoolista`, `idpessoa`, `isvalido`, `idalterador`, `idsolicitacaooriginal`, `dataalteracao`) VALUES
(4, 201807040000, '2018-07-13', '1.00', '1.00', '2018-07-14 00:00:00', 1, NULL, 0, 0, 5, 1, NULL, NULL, NULL),
(5, 201807040001, '2018-07-13', '1.00', '1.00', '2018-07-14 00:00:00', NULL, NULL, 1, 1, 4, 1, NULL, NULL, NULL),
(26, 201807170000, '2018-07-17', '100000.00', '883.37', NULL, NULL, NULL, 0, 1, 4, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sol_pred`
--

DROP TABLE IF EXISTS `sol_pred`;
CREATE TABLE `sol_pred` (
  `idsolicitacao` int(10) UNSIGNED NOT NULL,
  `idpred` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `sol_pred`
--

TRUNCATE TABLE `sol_pred`;
--
-- Dumping data for table `sol_pred`
--

INSERT INTO `sol_pred` (`idsolicitacao`, `idpred`) VALUES
(5, 4),
(26, 5),
(26, 3);

-- --------------------------------------------------------

--
-- Table structure for table `tipofuncionario`
--

DROP TABLE IF EXISTS `tipofuncionario`;
CREATE TABLE `tipofuncionario` (
  `id` int(10) UNSIGNED NOT NULL,
  `descricao` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `tipofuncionario`
--

TRUNCATE TABLE `tipofuncionario`;
--
-- Dumping data for table `tipofuncionario`
--

INSERT INTO `tipofuncionario` (`id`, `descricao`) VALUES
(1, 'Corretor'),
(2, 'Avaliador');

-- --------------------------------------------------------

--
-- Table structure for table `tipoparentesco`
--

DROP TABLE IF EXISTS `tipoparentesco`;
CREATE TABLE `tipoparentesco` (
  `id` int(10) UNSIGNED NOT NULL,
  `descricao` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `tipoparentesco`
--

TRUNCATE TABLE `tipoparentesco`;
--
-- Dumping data for table `tipoparentesco`
--

INSERT INTO `tipoparentesco` (`id`, `descricao`) VALUES
(1, 'Pai'),
(2, 'Mãe'),
(3, 'Filho'),
(4, 'Filha'),
(5, 'Irmão'),
(6, 'Irmã'),
(7, 'Avô'),
(8, 'Avó'),
(9, 'Neto'),
(10, 'Neta'),
(11, 'Tio'),
(12, 'Tia'),
(13, 'Sobrinho'),
(14, 'Sobrinha'),
(15, 'Primo'),
(16, 'Prima'),
(17, 'Outro');

-- --------------------------------------------------------

--
-- Table structure for table `tipopessoa`
--

DROP TABLE IF EXISTS `tipopessoa`;
CREATE TABLE `tipopessoa` (
  `id` int(10) UNSIGNED NOT NULL,
  `descricao` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `tipopessoa`
--

TRUNCATE TABLE `tipopessoa`;
--
-- Dumping data for table `tipopessoa`
--

INSERT INTO `tipopessoa` (`id`, `descricao`) VALUES
(1, 'Candidato'),
(2, 'Segurado'),
(3, 'Funcionário'),
(4, 'Não usuário');

-- --------------------------------------------------------

--
-- Table structure for table `tiposinistro`
--

DROP TABLE IF EXISTS `tiposinistro`;
CREATE TABLE `tiposinistro` (
  `id` int(10) UNSIGNED NOT NULL,
  `descricao` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `tiposinistro`
--

TRUNCATE TABLE `tiposinistro`;
--
-- Dumping data for table `tiposinistro`
--

INSERT INTO `tiposinistro` (`id`, `descricao`) VALUES
(1, 'Morte'),
(2, 'Incapacidade/Invalidez');

-- --------------------------------------------------------

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
  `idpessoa` int(10) UNSIGNED NOT NULL,
  `username` varchar(150) NOT NULL,
  `password` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `usuarios`
--

TRUNCATE TABLE `usuarios`;
--
-- Dumping data for table `usuarios`
--

INSERT INTO `usuarios` (`idpessoa`, `username`, `password`) VALUES
(4, 'candidato1', 'E1C1B3839580EA731FE6479DAA72A091E40E77B45485A4B1F097B9EEB2295D27E211746507F32C2219E722F2C23CE9A8C817F0C3E493C1255CF7C76F26EEBE7A'),
(5, 'candidato2', 'BB16CE081E0A497D0B6F12DF5F1B021619FCFCF76067116AA624424CDC3DF979F7211D81DB2F32DFF7D80A13C90C98464715F51263B5597EE8134AB44A414EC4'),
(6, 'avaliador1', 'E7BCDC4C69F871F7CAFC4B74D87CECD08DB8C8DDA5607BFDB6E468EB6A8AA98029F32A82AB9AC29DEAE91FD774770CA99B9A03622ACB958592972EC64F602B09'),
(7, 'corretor1', '91683692C0529FD0E6181ADF9D144C15EE6A9901B320F6C279C47F4E39A5080D31630CBD6724BC1E71798CE2D8AA09AA1C63E55D67CB4780EE2E401EC34DBE6E');

-- --------------------------------------------------------

--
-- Table structure for table `vinculo`
--

DROP TABLE IF EXISTS `vinculo`;
CREATE TABLE `vinculo` (
  `idpessoa` int(10) UNSIGNED NOT NULL,
  `idtipo` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `vinculo`
--

TRUNCATE TABLE `vinculo`;
--
-- Dumping data for table `vinculo`
--

INSERT INTO `vinculo` (`idpessoa`, `idtipo`) VALUES
(4, 1),
(5, 2),
(6, 3),
(7, 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `apolices`
--
ALTER TABLE `apolices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idcartaocredito` (`idcartaocredito`),
  ADD KEY `apolices_ibfk_1` (`idcandidato`),
  ADD KEY `solicitacaooriginal` (`solicitacaooriginal`);

--
-- Indexes for table `bandeirascartao`
--
ALTER TABLE `bandeirascartao`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `beneficiarios`
--
ALTER TABLE `beneficiarios`
  ADD PRIMARY KEY (`idcandidato`,`idbeneficiario`),
  ADD KEY `idbeneficiario` (`idbeneficiario`),
  ADD KEY `parentesco` (`parentesco`);

--
-- Indexes for table `cartaocredito`
--
ALTER TABLE `cartaocredito`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idbandeira` (`idbandeira`);

--
-- Indexes for table `contatos`
--
ALTER TABLE `contatos`
  ADD PRIMARY KEY (`idcontato`,`idtipoparentesco`),
  ADD KEY `fk_contatos_tipoparentesco_idx` (`idtipoparentesco`);

--
-- Indexes for table `funcionarios`
--
ALTER TABLE `funcionarios`
  ADD KEY `tipofuncionario` (`tipofuncionario`),
  ADD KEY `funcionarios_ibfk_1` (`idpessoa`);

--
-- Indexes for table `parcelas`
--
ALTER TABLE `parcelas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idapolice` (`idapolice`);

--
-- Indexes for table `pessoas`
--
ALTER TABLE `pessoas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cpf` (`cpf`,`email`);

--
-- Indexes for table `predisposicoes`
--
ALTER TABLE `predisposicoes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `problemasaude`
--
ALTER TABLE `problemasaude`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idsolicitacao` (`idsolicitacao`);

--
-- Indexes for table `sinistros`
--
ALTER TABLE `sinistros`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idapolice` (`idapolice`),
  ADD KEY `tiposinistro` (`tiposinistro`),
  ADD KEY `idavaliador` (`idavaliador`);

--
-- Indexes for table `solicitacoes`
--
ALTER TABLE `solicitacoes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `solicitacoes_ibfk_1` (`idpessoa`);

--
-- Indexes for table `sol_pred`
--
ALTER TABLE `sol_pred`
  ADD KEY `idpred` (`idpred`),
  ADD KEY `idsolicitacao` (`idsolicitacao`);

--
-- Indexes for table `tipofuncionario`
--
ALTER TABLE `tipofuncionario`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tipoparentesco`
--
ALTER TABLE `tipoparentesco`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tipopessoa`
--
ALTER TABLE `tipopessoa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tiposinistro`
--
ALTER TABLE `tiposinistro`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD KEY `idpessoa` (`idpessoa`);

--
-- Indexes for table `vinculo`
--
ALTER TABLE `vinculo`
  ADD PRIMARY KEY (`idpessoa`,`idtipo`),
  ADD KEY `idtipo` (`idtipo`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `apolices`
--
ALTER TABLE `apolices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `bandeirascartao`
--
ALTER TABLE `bandeirascartao`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `cartaocredito`
--
ALTER TABLE `cartaocredito`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contatos`
--
ALTER TABLE `contatos`
  MODIFY `idcontato` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `parcelas`
--
ALTER TABLE `parcelas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pessoas`
--
ALTER TABLE `pessoas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `predisposicoes`
--
ALTER TABLE `predisposicoes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `problemasaude`
--
ALTER TABLE `problemasaude`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `sinistros`
--
ALTER TABLE `sinistros`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `solicitacoes`
--
ALTER TABLE `solicitacoes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `tipofuncionario`
--
ALTER TABLE `tipofuncionario`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tipoparentesco`
--
ALTER TABLE `tipoparentesco`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `tipopessoa`
--
ALTER TABLE `tipopessoa`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tiposinistro`
--
ALTER TABLE `tiposinistro`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `apolices`
--
ALTER TABLE `apolices`
  ADD CONSTRAINT `apolices_ibfk_1` FOREIGN KEY (`idcandidato`) REFERENCES `usuarios` (`idpessoa`),
  ADD CONSTRAINT `apolices_ibfk_2` FOREIGN KEY (`idcartaocredito`) REFERENCES `cartaocredito` (`id`),
  ADD CONSTRAINT `apolices_ibfk_3` FOREIGN KEY (`solicitacaooriginal`) REFERENCES `solicitacoes` (`id`);

--
-- Constraints for table `beneficiarios`
--
ALTER TABLE `beneficiarios`
  ADD CONSTRAINT `beneficiarios_ibfk_1` FOREIGN KEY (`idcandidato`) REFERENCES `usuarios` (`idpessoa`),
  ADD CONSTRAINT `beneficiarios_ibfk_2` FOREIGN KEY (`idbeneficiario`) REFERENCES `pessoas` (`id`),
  ADD CONSTRAINT `beneficiarios_ibfk_3` FOREIGN KEY (`parentesco`) REFERENCES `tipoparentesco` (`id`);

--
-- Constraints for table `cartaocredito`
--
ALTER TABLE `cartaocredito`
  ADD CONSTRAINT `cartaocredito_ibfk_1` FOREIGN KEY (`idbandeira`) REFERENCES `bandeirascartao` (`id`);

--
-- Constraints for table `contatos`
--
ALTER TABLE `contatos`
  ADD CONSTRAINT `fk_contatos_tipoparentesco` FOREIGN KEY (`idtipoparentesco`) REFERENCES `tipoparentesco` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `funcionarios`
--
ALTER TABLE `funcionarios`
  ADD CONSTRAINT `funcionarios_ibfk_1` FOREIGN KEY (`idpessoa`) REFERENCES `usuarios` (`idpessoa`),
  ADD CONSTRAINT `funcionarios_ibfk_2` FOREIGN KEY (`tipofuncionario`) REFERENCES `tipofuncionario` (`id`);

--
-- Constraints for table `parcelas`
--
ALTER TABLE `parcelas`
  ADD CONSTRAINT `parcelas_ibfk_1` FOREIGN KEY (`idapolice`) REFERENCES `apolices` (`id`);

--
-- Constraints for table `problemasaude`
--
ALTER TABLE `problemasaude`
  ADD CONSTRAINT `problemasaude_ibfk_1` FOREIGN KEY (`idsolicitacao`) REFERENCES `solicitacoes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sinistros`
--
ALTER TABLE `sinistros`
  ADD CONSTRAINT `sinistros_ibfk_1` FOREIGN KEY (`idapolice`) REFERENCES `apolices` (`id`),
  ADD CONSTRAINT `sinistros_ibfk_3` FOREIGN KEY (`tiposinistro`) REFERENCES `tiposinistro` (`id`),
  ADD CONSTRAINT `sinistros_ibfk_4` FOREIGN KEY (`idavaliador`) REFERENCES `funcionarios` (`idpessoa`);

--
-- Constraints for table `solicitacoes`
--
ALTER TABLE `solicitacoes`
  ADD CONSTRAINT `solicitacoes_ibfk_1` FOREIGN KEY (`idpessoa`) REFERENCES `usuarios` (`idpessoa`);

--
-- Constraints for table `sol_pred`
--
ALTER TABLE `sol_pred`
  ADD CONSTRAINT `sol_pred_ibfk_1` FOREIGN KEY (`idpred`) REFERENCES `predisposicoes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sol_pred_ibfk_2` FOREIGN KEY (`idsolicitacao`) REFERENCES `solicitacoes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`idpessoa`) REFERENCES `pessoas` (`id`);

--
-- Constraints for table `vinculo`
--
ALTER TABLE `vinculo`
  ADD CONSTRAINT `vinculo_ibfk_1` FOREIGN KEY (`idpessoa`) REFERENCES `pessoas` (`id`),
  ADD CONSTRAINT `vinculo_ibfk_2` FOREIGN KEY (`idtipo`) REFERENCES `tipopessoa` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
