/* usuário 1 */
insert into `segurodevida`.`usuarios` (`usuario`, `senha`) 
values ('candidato1', '123456');

/* pessoa 1 */
insert into `segurodevida`.`pessoas` (`idusuario`, `nome`, `cpf`, `email`, `telefone`, `datanascimento`, `sexo`, `cep`) 
values ('1', 'candidato 1', '12345678901', 'candidato1@teste.com', '99999999999', curdate() - interval 20 year, 'm', '9999999');

/* candidato 1 */
insert into `segurodevida`.`candidatos` (`idusuario`) values ('1');

/* parcela 1 */
insert into `segurodevida`.`parcelas` (`idparcela`, `valor`, `datavencimento`, `datapgto`) values ('1', '1000', curdate() + interval 5 day, curdate() + interval 3 day);

/* cartão crédito 1 */
insert into `segurodevida`.`cartaocredito` (`numero`, `datavencimento`, `nome`, `codigoseguranca`, `idbandeirascartaocredito`) values ('9999999999999999', curdate() + interval 5 year, 'candidato 1 teste', '999', '2');

/* apolice 1 */
insert into `segurodevida`.`apolices` (`numero`, `datacontratacao`, `premio`, `valorsinistromorte`, `valorsinistroincapacitado`, `ativo`, `idparcela`, `idcartaocredito`) values ('201805200001', curdate(), '1000', '50000', '25000', '1', '1', '1');

/* segurado 1 */
insert into `segurodevida`.`segurados` (`idusuario`, `idapolice`, `vivo`, `incapacitado`) values ('1', '1', '1', '0');
