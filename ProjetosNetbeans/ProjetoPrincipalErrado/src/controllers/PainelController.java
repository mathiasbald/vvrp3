/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXPopup;
import de.jensd.fx.glyphs.GlyphsDude;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import models.Beneficiario;

/**
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public class PainelController extends Controller {

    @FXML
    private JFXButton menuUsuario;

    @FXML
    private Label tituloFrame;

    @FXML
    private FontAwesomeIconView iconeTituloFrame;

    @FXML
    private BorderPane frame;

    public PainelController() throws IOException {
        super("/views/Painel.fxml");
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        JFXListView<Label> list = new JFXListView<>();
        JFXPopup popup = new JFXPopup(list);
        Label meusDadosUsuario = new Label("Meus dados", GlyphsDude.createIcon(FontAwesomeIcon.COG));
        Label sairUsuario = new Label("Sair", GlyphsDude.createIcon(FontAwesomeIcon.SIGN_OUT));

        meusDadosUsuario.onMouseClickedProperty().set((event) -> {
            System.out.println("Não implementado ainda: " + meusDadosUsuario.getText());
            popup.hide();
        });

        sairUsuario.onMouseClickedProperty().set((event) -> {
            System.out.println("Sair chamado");
            try {
                super.setWindow(new LoginController());
            } catch (IOException ex) {
                Logger.getLogger(PainelController.class.getName()).log(Level.SEVERE, null, ex);
            }

            popup.hide();
        });

        list.getItems().add(meusDadosUsuario);
        list.getItems().add(sairUsuario);

        menuUsuario.setOnMouseClicked(e -> {
            popup.show(menuUsuario, JFXPopup.PopupVPosition.TOP, JFXPopup.PopupHPosition.RIGHT);
        });
    }

    @FXML
    private void relatarSinistro(ActionEvent event) throws IOException {
        this.setFrame("INFO_CIRCLE", "Relatar Sinistro", new RelatarSinistroController());
    }
    
    @FXML
    private void beneficiarios() throws IOException{
        this.setFrame("USERS", "Beneficiários", new ListarBeneficiariosController());
    }

    @FXML
    public void setFrame(String icone, String titulo, Controller controller) throws IOException {
        iconeTituloFrame.setGlyphName(icone);
        tituloFrame.setText(titulo);
        frame.setCenter(controller.load());
    }
}
