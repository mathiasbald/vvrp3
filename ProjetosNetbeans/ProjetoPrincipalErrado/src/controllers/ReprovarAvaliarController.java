/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTimePicker;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import models.Candidato;
import models.SolicitacaoSeguro;
import views.Alerta;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ResourceBundle;

/**
 * FXML Controller class
 *
 * @author marti
 */
public class ReprovarAvaliarController extends Controller {

    @FXML
    Label nSolicitacao;
    @FXML
    Label nomeCandidato;
    @FXML
    JFXTextArea motivo;
    private Candidato candidato;
    private SolicitacaoSeguro solicitacaoSeguro;


    public ReprovarAvaliarController(SolicitacaoSeguro selectedItem) throws IOException {
        super("/views/ReprovarAvaliar.fxml");
        solicitacaoSeguro = selectedItem;
        candidato = solicitacaoSeguro.getCandidato();

    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        nSolicitacao.setText(solicitacaoSeguro.getNumeroSolicitacao() + "");
        nomeCandidato.setText(candidato.getNome());

    }


    @FXML
    void acaoVoltar(ActionEvent event) throws IOException {
        super.setWindow(new ListaSolicitacoesAvaliarController());
    }
    @FXML
    void reprovar(ActionEvent event) throws IOException {
        if (motivo.getText().isEmpty()){
        Alerta.mostrar("Impossivel Reprovar","Informe o motivo da reprovação",true);
            return;
        }
            solicitacaoSeguro.reprovar(motivo.getText());
        Alerta.mostrar("Concluído", "Solicitacao reprovada com sucesso", true);
        System.out.println("done-reprovado");
        acaoVoltar(event);
    }



    @FXML
    void verInfosCandidato(ActionEvent event) {
        Alerta.mostrar("Informações do Candidato: ", candidato.toString(), true);
    }
}
