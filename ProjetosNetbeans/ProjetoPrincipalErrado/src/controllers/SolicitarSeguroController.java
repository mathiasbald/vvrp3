/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextFormatter;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import models.*;
import models.dao.SolicitacaoSeguroDao;
import views.Alerta;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.*;
import java.util.function.UnaryOperator;

/**
 * FXML Controller class
 *
 * @author marti
 */
public class SolicitarSeguroController extends Controller {


    //////////////////
    @FXML
    VBox problemsContainer;


    @FXML
    JFXCheckBox predCancer;

    @FXML
    JFXCheckBox predDiabete;

    @FXML
    JFXCheckBox predCardio;

    @FXML
    JFXCheckBox predPulmo;

    @FXML
    JFXCheckBox predHiper;

    @FXML
    JFXCheckBox predDemencia;

    @FXML
    JFXCheckBox predDegene;

    @FXML
    JFXCheckBox predOsteo;

    @FXML
    JFXCheckBox alcoolista;

    @FXML
    JFXCheckBox fumante;

    @FXML
    Label valorSimulado;

    @FXML
    JFXTextField tfValorDesejado;

    @FXML
    GridPane checkBoxesContainer;

    @FXML
    Label labelSolictacaoEnviada;

    private SolicitacaoSeguro solicitacaoSeguro;

    private Set<JFXCheckBox> cbSet = new HashSet<JFXCheckBox>();
    private Candidato candidato = (Candidato) Session.getInstance().getPessoa();

    public SolicitarSeguroController() throws IOException {
        super("/views/SolicitarSeguro.fxml");


    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        solicitacaoSeguro = new SolicitacaoSeguro(candidato);
        if (solicitacaoSeguro.calcularIdade() < 18) {
            autoRecuseAndExit("Você não tem idade o suficiente para fazer um seguro de vida(18 anos).\nLamentamos");
            return;
        } else if (solicitacaoSeguro.calcularIdade() > 90) {
            autoRecuseAndExit("Você já passou da idade de fazer um seguro de vida(90 anos).\nLamentamos");
            return;
        }


        UnaryOperator<TextFormatter.Change> filter = (TextFormatter.Change t) -> {
            if (t.isReplaced()) {
                if (t.getText().matches("[^0-9]")) {
                    t.setText(t.getControlText().substring(t.getRangeStart(), t.getRangeEnd()));
                }
            }
            if (t.isAdded()) {
                if (t.getText().matches("[,]") && !(t.getControlText().contains("."))) {
                    if (t.getControlText().isEmpty()) {
                        t.setText("0.");
                    } else {
                        t.setText(".");
                    }
                } else if (t.getControlText().contains(".")) {
                    if (t.getText().matches("[^0-9]")) {
                        t.setText("");
                    }
                } else if (t.getText().matches("[^0-9.]")) {
                    t.setText("");
                }
            }
            return t;
        };
        tfValorDesejado.setTextFormatter(new TextFormatter<>(filter));
        valorSimulado.setText(String.format("R$%#.2f", calcularValorPremio()));

        EventHandler eh = (EventHandler<ActionEvent>) (ActionEvent event) -> {
            System.out.println("Action actived");
            if (event.getSource() instanceof JFXCheckBox) {
                JFXCheckBox cb = (JFXCheckBox) event.getSource();
                System.out.println("Action performed on checkbox " + cb.getText());
                modifyList(cb);
                System.out.println("TotalOfCheckBoxes: " + cbSet.size());
            }
            valorSimulado.setText(String.format("R$%#.2f", calcularValorPremio()));
        };

        checkBoxesContainer.getChildren()
                .forEach((t) -> {
                            if (t instanceof JFXCheckBox) {
                                CheckBox cb = (JFXCheckBox) t;
                                System.out.println("CB: " + cb.getText());
                                cb.setOnAction(eh);
                            }
                        }
                );
        EventHandler ehncb = (event) -> {
            System.out.println("Typed");
            valorSimulado.setText(String.format("R$%#.2f", calcularValorPremio()));
        };
        alcoolista.setOnAction(ehncb);
        fumante.setOnAction(ehncb);
        tfValorDesejado.textProperty().addListener((observable) -> {
            System.out.println("Typed");
            valorSimulado.setText(String.format("R$%#.2f", calcularValorPremio()));
        });

    }

    private void modifyList(JFXCheckBox cb) {
        if (cb.isSelected()) {
            cbSet.add(cb);
        } else {
            cbSet.remove(cb);
        }
    }


    private double calcularValorPremio() {
        float valorDesejado = 0;
        String valorDesejadoString = tfValorDesejado.getText();
        if (!valorDesejadoString.isEmpty()) {
            try {
                valorDesejado = Float.parseFloat(valorDesejadoString);
            } catch (Exception e) {
                valorDesejado = 0;
            }
        }
        return solicitacaoSeguro.calcularValorPremio(valorDesejado, cbSet.size(), fumante.isSelected(), alcoolista.isSelected());
    }


    @FXML
    void addCampoProblemaClick(MouseEvent e) {
        addCampoProblemaSaude();
    }

    private void addCampoProblemaSaude() {
        problemsContainer.getChildren().addAll(new MyVBox(problemsContainer));
    }

    @FXML
    void acaoVoltar(ActionEvent event) throws IOException {
        super.setWindow(new MenuTesteController());
    }

    @FXML
    void cancelar(ActionEvent e) throws IOException {
        acaoVoltar(e);
    }

    @FXML
    void enviarSolicitacao(ActionEvent e) {
        if (tfValorDesejado.getText().isEmpty()) {
            emptyFields("Valor do seguro desejado");
            return;
        }
        if (cbSet.size() >= 4) {//verificando se ele o candidato não possui predisposições com fumo e alcool
            autoRecuse("Você não pode ter mais de 4 predisposições a doenças.\nLamentamos.");
            return;
        } else if (cbSet.size() >= 2 && alcoolista.isSelected() && fumante.isSelected()) {
            autoRecuse("Você não pode ter mais de 2 predisposições a doenças, ser fumante e alcoolista.\nLamentamos.");
            return;
        }
        ArrayList<Predisposicoes> predisposicoes = new ArrayList<>();
        if (predCancer.isSelected()) {
            predisposicoes.add(Predisposicoes.CANCER);
        }
        if (predCardio.isSelected()) {
            predisposicoes.add(Predisposicoes.CORACAO);
        }
        if (predDegene.isSelected()) {
            predisposicoes.add(Predisposicoes.DEGENERACAO);

        }
        if (predDemencia.isSelected()) {
            predisposicoes.add(Predisposicoes.DEMENCIA);

        }
        if (predDiabete.isSelected()) {
            predisposicoes.add(Predisposicoes.DIABETE);

        }
        if (predHiper.isSelected()) {
            predisposicoes.add(Predisposicoes.HIPERTENSAO);

        }
        if (predOsteo.isSelected()) {
            predisposicoes.add(Predisposicoes.OSTEOPOROSE);

        }
        if (predPulmo.isSelected()) {
            predisposicoes.add(Predisposicoes.PULMONAR);

        }

        ArrayList<ProblemaSaude> problemas = new ArrayList<>();
        for (Node n : problemsContainer.getChildren()) {
            if (n instanceof MyVBox) {
                System.out.println("Ta indo" + ((MyVBox) n).getNomeProblema() + ((MyVBox) n).getDescProblema());
                problemas.add(new ProblemaSaude(((MyVBox) n).getNomeProblema(), ((MyVBox) n).getDescProblema()));
            }

        }


        solicitacaoSeguro.setMultiple(
                SolicitacaoSeguro.gerarNumeroSolicitacao(01),
                new Date(),
                calcularValorPremio(),
                Double.parseDouble(tfValorDesejado.getText()),
                fumante.isSelected(),
                alcoolista.isSelected(),
                predisposicoes);

        for (ProblemaSaude problemaSaude : problemas) {
            solicitacaoSeguro.adicionarProblema(problemaSaude);
        }


        labelSolictacaoEnviada.setVisible(true);
        try {
            new SolicitacaoSeguroDao().insert(solicitacaoSeguro);
            createMessage("Solicitação de seguro concluída", "Agora basta aguardar sua solicitação ser avaliada e entraremos em contato.");
        } catch (SQLException e1) {
            e1.printStackTrace();
            createMessage("Falha ao enviar a solicitação de seguro", "Algo de errado aconteceu. Tente novamente mais tarde ou contate o técnico responsável pelo sistema");

        } catch (IOException e1) {
            e1.printStackTrace();
            createMessage("Falha ao enviar a solicitação de seguro", "Algo de errado aconteceu. Tente novamente mais tarde ou contate o técnico responsável pelo sistema");
        }
    }


    private void emptyFields(String... args) {
        String finalText = "";
        for (String arg : args) {
            finalText += arg + "\n";
        }
        createMessage("Alguns campos obrigatórios foram deixas em branco: ", finalText);
    }

    private void autoRecuse(String texto) {
        createMessage("Proposta Recusada!", texto);
    }

    private void autoRecuseAndExit(String texto) {
        createMessage("Você não pode solicitar um seguro!", texto);
        try {
            acaoVoltar(null);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void createMessage(String headerText, String texto) {
        Alerta.addBtn("OK", acao -> {
        });
        Alerta.mostrar(headerText, texto, true);
    }


    private class MyVBox extends VBox {
        private JFXTextField nomeProblema;
        private JFXTextArea descProblema;


        MyVBox(Pane parent) {
            super();
            //declarações e inicializações
            HBox containerTopo = new HBox();
            nomeProblema = new JFXTextField();
            StackPane botaoAddSP = new StackPane();
            Circle c = new Circle();
            Label l = new Label();
            descProblema = new JFXTextArea();

            //mudança atributos
            nomeProblema.prefWidth(375.0);
            nomeProblema.maxWidth(getWidth());
            HBox.setHgrow(nomeProblema, Priority.ALWAYS);

            nomeProblema.setStyle("    -fx-padding: 5px; -fx-border-insets: 5px; -fx-background-insets: 5px;");
            nomeProblema.setPromptText("Aqui vai o nome do problema");
            HBox.setHgrow(botaoAddSP, Priority.NEVER);
            c.setFill(Paint.valueOf("#999999"));
            c.setStrokeWidth(0);
            c.setRadius(10.0);
            l.setFont(Font.font(23));
            l.setText("-");
            Pane p = new Pane();
            p.setMinWidth(50);
            p.setMaxWidth(50);
            p.prefWidth(50);
            botaoAddSP.prefHeight(15.0);
            botaoAddSP.prefWidth(35.0);
            botaoAddSP.setStyle("    -fx-padding: 5px; -fx-border-insets: 5px; -fx-background-insets: 5px;");
            botaoAddSP.getChildren().addAll(c, l);
            containerTopo.getChildren().addAll(nomeProblema, p, botaoAddSP);

            descProblema.setStyle("    -fx-padding: 5px; -fx-border-insets: 5px; -fx-background-insets: 5px;");
            descProblema.setMaxHeight(65);
            descProblema.setPromptText("Aqui vai a descrição do problema");

            this.setStyle("    -fx-padding: 5 0 5 0; -fx-border-insets: 5 0 5 0; -fx-background-insets: 5 0 5 0;");
            this.getChildren().addAll(containerTopo, descProblema);
            this.getStyleClass().addAll("box-simple-border");
            botaoAddSP.setOnMouseClicked(mouseEvent -> {
                parent.getChildren().remove(this);
            });
        }

        String getNomeProblema() {
            return nomeProblema.getText();
        }

        String getDescProblema() {
            return descProblema.getText();
        }

    }

}
