/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.IOException;
import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.util.Duration;
import views.Main;

/**
 *
 * @author Gustavo Bittecourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public abstract class Controller implements Initializable {

    private final Controller mainController;
    protected final FXMLLoader myView;

    public Controller(String fxml) throws IOException {
        this.mainController = Main.mainController;

        this.myView = new FXMLLoader(getClass().getResource(fxml));
        this.myView.setController(this);
    }

    public Node load() throws IOException {
        return this.myView.load();
    }

    protected Node getWindow() {
        return this.mainController.getWindow();
    }
    
    protected Stage getStage(){
        return this.mainController.getStage();
    }

    protected void setWindow(Controller controller) {
        FadeTransition fadeTransition = new FadeTransition(Duration.millis(500));
        fadeTransition.setNode(this.mainController.getWindow());
        fadeTransition.setFromValue(1);
        fadeTransition.setToValue(0);

        fadeTransition.setOnFinished((ActionEvent event) -> {
            mainController.setWindow(controller);
            this.fadeIn();
        });

        fadeTransition.play();
    }

    private void fadeIn() {
        FadeTransition fadeTransition = new FadeTransition(Duration.millis(500));
        fadeTransition.setNode(this.mainController.getWindow());
        fadeTransition.setFromValue(0.5);
        fadeTransition.setToValue(1);
        fadeTransition.play();
    }
}
