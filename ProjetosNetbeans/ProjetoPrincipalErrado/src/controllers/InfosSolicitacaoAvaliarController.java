/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import com.jfoenix.controls.JFXButton;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import models.*;
import views.Alerta;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * FXML Controller class
 *
 * @author marti
 */
public class InfosSolicitacaoAvaliarController extends Controller {

    @FXML
    Label nSolicitacao;
    @FXML
    Label nomeCandidato;
    @FXML
    VBox vBoxBeneficiarios;
    @FXML
    VBox vBoxProblemas;
    @FXML
    VBox vBoxPreds;
    @FXML
    Label fumante;
    @FXML
    Label alcoolista;

    private Candidato candidato;
    private SolicitacaoSeguro solicitacaoSeguro;


    public InfosSolicitacaoAvaliarController(SolicitacaoSeguro selectedItem) throws IOException {
        super("/views/InfosSolicitacaoAvaliar.fxml");
        solicitacaoSeguro = selectedItem;
        candidato = solicitacaoSeguro.getCandidato();

    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        nSolicitacao.setText(solicitacaoSeguro.getNumeroSolicitacao() + "");
        nomeCandidato.setText(candidato.getNome());

        System.out.println(candidato.getBeneficiarios().toString());
        for (Beneficiario beneficiario : candidato.getBeneficiarios()) {
            vBoxBeneficiarios.getChildren().add(new ViewBeneficiario(beneficiario));
        }

        for (ProblemaSaude ps : solicitacaoSeguro.getProblemasSaude()) {
            vBoxProblemas.getChildren().add(new ViewProblemaSaude(ps));
        }
        for (Predisposicoes predisposicoes : solicitacaoSeguro.getPredisposicoes()) {
            vBoxPreds.getChildren().add(new ViewPredisposicao(predisposicoes));
        }

        if(solicitacaoSeguro.isAlcoolista()){
            alcoolista.setText(" Sim");
        }else{
            alcoolista.setText(" Não");
        }
        if(solicitacaoSeguro.isFumante()){
            fumante.setText(" Sim");
        }else{
            fumante.setText(" Não");
        }

    }

    private class ViewPredisposicao extends HBox {
        public ViewPredisposicao(Predisposicoes p) {
            this.setAlignment(Pos.CENTER_LEFT);
            Label pred = new Label("- " + p.toString());
            this.getChildren().addAll(pred);
        }
    }

    private class ViewProblemaSaude extends VBox {
        public ViewProblemaSaude(ProblemaSaude ps) {
            this.setAlignment(Pos.TOP_LEFT);
            HBox cima = new HBox();
            HBox baixo = new HBox();
            Label aNome = new Label("Nome do problema: ");
            aNome.setUnderline(true);
            Label nome = new Label(ps.getNomeProblema());
            cima.getChildren().addAll(aNome, nome);
            Label aDesc = new Label("Descrição: ");
            aDesc.setUnderline(true);
            Label desc = new Label(ps.getDescricaoProblema());
            baixo.getChildren().addAll(aDesc, desc);
            this.getChildren().addAll(cima, baixo);
        }
    }

    private class ViewBeneficiario extends HBox {
        public ViewBeneficiario(Beneficiario b) {
            this.setAlignment(Pos.CENTER_LEFT);
            this.paddingProperty().setValue(new Insets(0, 0, 0, 10));
            Label nome = new Label("- " + b.getNome() + " - " + b.getParentesco().toString()+"   ");
            JFXButton botao = new JFXButton("Ver Informações do Beneficiario");
            botao.getStyleClass().addAll("btn-form-infos");

            botao.setOnAction(event ->
                    Alerta.mostrar("Informações do Beneficiario: ", b.toString(), true)
            );

            this.getChildren().addAll(nome, botao);
        }

    }

    @FXML
    void acaoVoltar(ActionEvent event) throws IOException {
        super.setWindow(new ListaSolicitacoesAvaliarController());
    }

    @FXML
    void reprovar(ActionEvent event) throws IOException {
        super.setWindow(new ReprovarAvaliarController(solicitacaoSeguro));
    }

    @FXML
    void contatar(ActionEvent event) throws IOException {
        super.setWindow(new ContatarAvaliarController(solicitacaoSeguro));
    }

    @FXML
    void verInfosCandidato(ActionEvent event) {
        Alerta.mostrar("Informações do Candidato: ", candidato.toString(), true);
    }
}
