/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.fxml.FXML;
import javafx.scene.Node;

import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;

import javafx.stage.Stage;
import javafx.stage.StageStyle;
import views.Alerta;

/**
 * FMXL Controller Main
 *
 * @author Gustavo Bittecourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public class MainController extends Controller {

    private final Stage myStage;

    private final String title;
    @FXML
    private Label windowTitle;

    @FXML
    private BorderPane window;

    private boolean isMaximized;
    private double xOffset;
    private double yOffset;

    public MainController(Stage stage, String title, boolean maximized) throws IOException {
        super("/views/Main.fxml");

        this.isMaximized = maximized;
        this.myStage = stage;
        this.title = title;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void initStage() throws IOException {
        Scene scene = new Scene(this.myView.load());
        myStage.setScene(scene);

        scene.getStylesheets().add(getClass().getResource("/views/styles/jfx-dialog-layout.css").toExternalForm());

        myStage.setOnCloseRequest(windowEvent -> {
            windowEvent.consume();
            this.myStage.setIconified(false);
            this.fechar();
        });

        myStage.setTitle(title);
        windowTitle.setText(title);

        this.setWindow(new LoginController());

        myStage.initStyle(StageStyle.UNDECORATED);
        myStage.setMaximized(isMaximized);
        myStage.show();
    }

    @FXML
    private void fechar() {
        Alerta.addBtn("Sair", "#7088FF", "white", acao -> {
            System.out.println("Aceitou sair do sistema");
            myStage.close();
        });

        Alerta.addBtn("Cancelar", acao -> {
            System.out.println("Ficou no sistema");
        });

        Alerta.mostrar("Sair do sistema", "Deseja realmente sair do sistema?", true);
    }

    @FXML
    private void maximizar(MouseEvent mouseEvent) {
        System.out.println("Maximizar: chamado");
        this.myStage.setMaximized(isMaximized = (!isMaximized));
    }

    @FXML
    private void minimizar(MouseEvent mouseEvent) {
        System.out.println("Minimizar: chamado");
        this.myStage.setIconified(true);
    }

    @FXML
    private void dadosMoverJanela(MouseEvent event) {
        xOffset = myStage.getX() - event.getScreenX();
        yOffset = myStage.getY() - event.getScreenY();
    }

    @FXML
    private void moverJanela(MouseEvent event) {
        myStage.setX(event.getScreenX() + xOffset);
        myStage.setY(event.getScreenY() + yOffset);
    }

    @Override
    public Node getWindow() {
        return this.window.getCenter();
    }

    @Override
    public Stage getStage() {
        return this.myStage;
    }

    @Override
    public void setWindow(Controller controller) {
        try {
            window.setCenter(controller.load());
        } catch (IOException ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
