/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import com.jfoenix.controls.JFXCheckBox;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.*;
import models.*;
import models.mocks.MockCandidato;
import models.mocks.MockSolicitacaoSeguro1;
import models.mocks.MockSolicitacaoSeguro2;

import java.io.IOException;
import java.net.URL;
import java.util.*;

/**
 * FXML Controller class
 *
 * @author marti
 */
public class ListaSolicitacoesAvaliarController extends Controller {


    //////////////////
    @FXML
    VBox problemsContainer;
@FXML
TableView<SolicitacaoSeguro> tv;

    private Set<JFXCheckBox> cbSet = new HashSet<JFXCheckBox>();
    private Candidato candidato = new MockCandidato();

    public ListaSolicitacoesAvaliarController() throws IOException {
        super("/views/ListaSolicitacoesAvaliar.fxml");
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {


        (tv.getColumns().get(0)).setCellValueFactory(new PropertyValueFactory<>("numeroSolicitacao"));

        (tv.getColumns().get(1)).setCellValueFactory(new PropertyValueFactory<>("nomeCandidato"));

        (tv.getColumns().get(2)).setCellValueFactory(new PropertyValueFactory<>("dataSolicitacao"));

        (tv.getColumns().get(3)).setCellValueFactory(new PropertyValueFactory<>("valorPremio"));

        (tv.getColumns().get(4)).setCellValueFactory(new PropertyValueFactory<>("valorSolicitacao"));

        tv.getItems().addAll(new MockSolicitacaoSeguro1(), new MockSolicitacaoSeguro2());



    }

    @FXML
    void acaoVoltar(ActionEvent event) throws IOException {
        super.setWindow(new LoginController());
    }

    @FXML
    void avaliar(ActionEvent event) throws IOException {
        super.setWindow(new InfosSolicitacaoAvaliarController(tv.getSelectionModel().getSelectedItem()));
    }




}
