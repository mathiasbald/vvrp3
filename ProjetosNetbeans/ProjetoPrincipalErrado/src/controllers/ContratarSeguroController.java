/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import models.TextFieldFormatter;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import views.Alerta;

/**
 * FXML Controller class
 *
 * @author marti
 */
public class    ContratarSeguroController extends Controller {
    
    @FXML
    private BorderPane borderPane;

    @FXML
    private JFXButton btnDashboard;

    @FXML
    private JFXButton btnContratarSeguro;

    @FXML
    private Button btnRevisarProposta;

    @FXML
    private Button btnContratarSeguroAba;

    @FXML
    private JFXTextField textFieldNCartao;

    @FXML
    private JFXTextField textFieldNTitular;

    @FXML
    private JFXTextField textFieldMes;

    @FXML
    private JFXTextField textFieldAno;

    @FXML
    private JFXPasswordField passwordFieldCVV;


    @FXML
    private Label labelPremio;

    @FXML
    private Label labelParcelas;

    @FXML
    private JFXButton btnCancelar;

    @FXML
    private JFXButton btnFinalizar;
    
    //////////////////Configuração combo box
    @FXML
    private JFXComboBox<String> comboBoxParcelas;

    ObservableList<String> parcelasList = FXCollections.observableArrayList(
        "1X-1000,00","2X-500,00","3X-333,33","4X-250,00","5X-200,00","6X-166,67","7X-142,85",
        "8X-125,00","9X-111,11","10X-100,00","11X-90,90","12X-83,33");
    //////////////////


    public ContratarSeguroController() throws IOException {
        super("/views/ContratarSeguro.fxml");
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        comboBoxParcelas.setValue("1X-1000,00");
        comboBoxParcelas.setItems(parcelasList);
    }
    
    @FXML
    void acaoVoltar(ActionEvent event) throws IOException {
        super.setWindow(new MenuTesteController());
    }
    
    @FXML
    void cancelarContrato(ActionEvent event) throws IOException {
        
        Alerta.addBtn("Sim", acao -> {
            try {
                super.setWindow(new LoginController());
            } catch (IOException ex) {
                Logger.getLogger(ContratarSeguroController.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        Alerta.addBtn("Não", acao -> {
            System.out.println("Ficou no sistema");
        });
        
        Alerta.mostrar("Cancelar operação!", "Você realmente deseja sair da tela de contrato?", true);
    }
    
    @FXML
    void maskNCartao(KeyEvent event) {
        TextFieldFormatter mask = new TextFieldFormatter();
        mask.setMask("####-####-####-####");
        mask.setCaracteresValidos("0123456789");
        mask.setTf(textFieldNCartao);
        mask.formatter();
    }
    
    @FXML
    void maskAno(KeyEvent event) {
        TextFieldFormatter mask = new TextFieldFormatter();
        mask.setMask("##");
        mask.setCaracteresValidos("0123456789");
        mask.setTf(textFieldAno);
        mask.formatter();
    }

    @FXML
    void maskMes(KeyEvent event) {
        TextFieldFormatter mask = new TextFieldFormatter();
        mask.setMask("##");
        mask.setCaracteresValidos("0123456789");
        mask.setTf(textFieldMes);
        mask.formatter();
    }
    
    boolean validarDados(){
        if (textFieldNCartao.getText().contains(" ") || textFieldNCartao.getText().equals("")) {
            Alerta.addBtn("Ok", acao -> {});
            Alerta.mostrar("ATENÇÃO!", "Insira corretamente o número do cartão.", true);
            return false;
        }
        
        if ( !(textFieldNTitular.getText().matches("[a-zA-Z\\s]+")) ) {
            Alerta.addBtn("Ok", acao -> {});
            Alerta.mostrar("ATENÇÃO!", "Digite corretamente o nome do titular do cartão com apenas letras.", true);
            return false;
        }
        
        if (textFieldNTitular.getText().isEmpty()) {
            Alerta.addBtn("Ok", acao -> {});
            Alerta.mostrar("ATENÇÃO!", "Digite o nome do titular do cartão.", true);
            return false;
        }
        
        if (textFieldAno.getText().contains(" ")    || textFieldAno.getText().isEmpty()
            || textFieldMes.getText().contains(" ") || textFieldMes.getText().isEmpty()) {
            Alerta.addBtn("Ok", acao -> {});
            Alerta.mostrar("ATENÇÃO!", "Insira corretamente a validade do cartão.", true);
            return false;
        }
        
        if (Integer.parseInt(textFieldAno.getText()) < 18 
            || Integer.parseInt(textFieldMes.getText()) < 1
            || Integer.parseInt(textFieldMes.getText()) > 12 ) {
            Alerta.addBtn("Ok", acao -> {});
            Alerta.mostrar("ATENÇÃO!", "Cartão vencido. Insira os dados de outro cartão para continuar.", true);
            return false;
        }
        
        if ( passwordFieldCVV.getText().matches("[a-zA-Z\\s]+") 
              || passwordFieldCVV.getText().length() != 3 ) {
            Alerta.addBtn("Ok", acao -> {});
            Alerta.mostrar("ATENÇÃO!", "Digite apenas numeros com 3 dígitos no CVV.", true);
            return false;
        }
    
        return true;
    }
    
    @FXML
    void finalizar(ActionEvent event ){
        if (validarDados()) {
            Alerta.addBtn("Ok", acao -> {
                try {
                    super.setWindow(new LoginController());
                } catch (IOException ex) {
                    Logger.getLogger(ContratarSeguroController.class.getName()).log(Level.SEVERE, null, ex);
                }
            });

            Alerta.mostrar("Parabéns!", "Sua apólice foi gerada com sucesso! Todos os seus dados foram atualizados em nosso sistema.", true);
        }
    }
    
}
