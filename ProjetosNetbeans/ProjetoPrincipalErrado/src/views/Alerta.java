/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import com.jfoenix.controls.JFXAlert;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.JFXSpinner;
import java.util.ArrayList;
import java.util.List;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.stage.Modality;

/**
 *
 * @author Gustavo Satheler | Mathias Baldissera | Michael Martins
 */
public final class Alerta {

    private static final JFXAlert jfxAlert = new JFXAlert(Main.mainController.getStage());
    private static final JFXDialogLayout jfxDialogLayout = new JFXDialogLayout();
    private static final List<Node> botoes = new ArrayList<>();
    private static boolean isVisible = false;

    private Alerta() {
    }

    public static void addBtn(String texto, String buttonColor, String textButtonColor, EventHandler<ActionEvent> acao) {
        JFXButton btn = new JFXButton(texto);
        btn.setStyle("-fx-font-size: 14.0; -fx-text-fill: " + textButtonColor + "; -fx-background-color: " + buttonColor + ";");

        btn.setOnMouseClicked(mouse -> {
            desaparecer();
        });

        btn.setOnKeyPressed(teclado -> {
        });

        btn.setOnAction(acao);
        botoes.add(btn);
    }

    public static void addBtn(String texto, EventHandler<ActionEvent> acao) {
        JFXButton btn = new JFXButton(texto);

        btn.setStyle("-fx-font-size: 14.0; -fx-text-fill: black;");

        btn.setOnMouseClicked(mouse -> {
            desaparecer();
        });

        btn.setOnKeyPressed(teclado -> {
        });

        btn.setOnAction(acao);
        botoes.add(btn);
    }

    public static void mostrar(String titulo, JFXSpinner spinner) {
        if (!isVisible) {
            limparBotoes();
            setPropriedades();
            isVisible = true;
            jfxDialogLayout.setHeading(new Label(titulo));
            jfxDialogLayout.setBody(spinner);
            jfxAlert.setContent(jfxDialogLayout);
            jfxAlert.show();
        }
    }

    public static void mostrar(String titulo, String mensagem, boolean needButton) {
        if (!isVisible) {
            setPropriedades();
            isVisible = true;
            jfxDialogLayout.setHeading(new Label(titulo));
            jfxDialogLayout.setBody(new Label(mensagem));

            if (needButton) {
                if (botoes.isEmpty()) {
                    addBtn("OK", acao -> {
                        desaparecer();
                    });
                }

                jfxDialogLayout.setActions(botoes);
            }

            jfxAlert.setContent(jfxDialogLayout);
            jfxAlert.show();
            limparBotoes();
        }
    }

    public static void desaparecer() {
        if (isVisible) {
            jfxAlert.hideWithAnimation();
            limparBotoes();
            isVisible = false;
        }
    }

    public static void limparBotoes() {
        botoes.clear();
    }

    private static void setPropriedades() {
        if (!jfxAlert.getModality().equals(Modality.APPLICATION_MODAL)) {
            jfxAlert.initModality(Modality.APPLICATION_MODAL);
        }
        if (jfxAlert.overlayCloseProperty().get() != false) {
            jfxAlert.setOverlayClose(false);
        }
    }
}
