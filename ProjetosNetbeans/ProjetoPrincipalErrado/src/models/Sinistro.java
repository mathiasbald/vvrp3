/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Blob;

/**
 *
 * @author Gustavo Bittecourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public class Sinistro extends Model {

    private ApoliceSeguro apolice;
    private String causaMorte;
    private File certidaoObito;
    private int contato;

    public Sinistro() {}

    public ApoliceSeguro getApolice() {
        return apolice;
    }

    public void setApolice(ApoliceSeguro apolice) {
        this.apolice = apolice;
    }

    public String getCausaMorte() {
        return causaMorte;
    }

    public void setCausaMorte(String causaMorte) {
        this.causaMorte = causaMorte;
    }

    public File getCertidaoObito() {
        return certidaoObito;
    }

    public void setCertidaoObito(File certidaoObito) {
        this.certidaoObito = certidaoObito;
    }

    public int getContato() {
        return contato;
    }

    public void setContato(int contato) {
        this.contato = contato;
    }
    
}
