/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.dao;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import models.Contato;
import models.Model;

/**
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public class ContatoDao extends DAO {

    private int idInserido;

    public ContatoDao() {
        super();
    }

    @Override
    public void delete(Model model) {

    }

    @Override
    public void update(Model model) {

    }

    @Override
    public void insert(Model model) throws SQLException, IllegalStateException, IOException {
        Contato contato = this.converter(model);
        conexao.abrirConexao();

        PreparedStatement stmt = conexao.prepararDeclaracao("INSERT INTO sinistros (nome, idParentesco, telefone, email)"
                + "VALUES (?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);

        stmt.setString(1, contato.getNome());
        stmt.setInt(2, 17); // contato.getParentesco()
        stmt.setLong(3, contato.getTelefone());
        stmt.setString(4, contato.getNome());

        stmt.execute();

        ResultSet rs = stmt.getGeneratedKeys();
        if (rs.next()) {
            this.idInserido = rs.getInt(1);
        }

        stmt.close();
    }

    @Override
    protected List<Model> select() throws SQLException {
        return null;
    }

    private Contato converter(Model model) {
        Contato modelToConverter = null;

        if (model instanceof Contato) {
            modelToConverter = (Contato) model;
        } else {
            throw new ClassCastException("Sinistro não válido");
        }

        return modelToConverter;
    }

    public int getIdInserido() {
        return idInserido;
    }
}
