/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import models.Model;
import models.Sinistro;

/**
 *
 * @author Gustavo Bittecourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public class SinistroDao extends DAO {

    public SinistroDao() {
        super();
    }

    @Override
    public void delete(Model model) {

    }

    @Override
    public void update(Model model) {

    }
    @Override
    public void insert(Model model) throws SQLException, IllegalStateException, IOException {
        Sinistro sinistro = this.converter(model);

        File certidaoObito = sinistro.getCertidaoObito();
        FileInputStream fis = new FileInputStream(certidaoObito);
        
        conexao.abrirConexao();

        PreparedStatement stmt = conexao.prepararDeclaracao("INSERT INTO sinistro (idApolice, data, descricao, certidaoObito, autorizado, idContato)"
                + "VALUES (?, ?, ?, ?, ?, ?)");

        stmt.setLong(1, sinistro.getApolice().getNumeroApolice());
        stmt.setString(2, "NOW()");
        stmt.setString(3, "Causa da morte: " + sinistro.getCausaMorte());
        stmt.setBinaryStream(4, (InputStream) fis, (int) certidaoObito.length());
        stmt.setBoolean(5, false);
//        stmt.setInt(6, sinistro.getContato());

        fis.close();
        stmt.execute();
        stmt.close();
    }

    @Override
    public List<Model> select() throws SQLException {
        return null;
    }

    private Sinistro converter(Model model) throws ClassCastException {
        Sinistro modelToConverter = null;

        if (model instanceof Sinistro) {
            modelToConverter = (Sinistro) model;
        } else {
            throw new ClassCastException("Sinistro não válido");
        }

        return modelToConverter;
    }

}
