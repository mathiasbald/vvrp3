/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.dao;

import bancoDeDados.Conexao;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import models.Model;

/**
 * @author Gustavo Bittecourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public abstract class DAO {

    protected Conexao conexao;

    public DAO() {
        this.conexao = Conexao.getInstance();
        conexao.init("localhost", 3306, "segurodevida2", "root", "carambolatech");
    }

    /**
     *
     * @param <T>
     * @param model
     */
    public abstract <T extends Model> void delete(T model);

    public abstract <T extends Model> void update(T model) throws SQLException; 

    public abstract <T extends Model> void insert(T model) throws SQLException, IllegalStateException, IOException;

    protected abstract List<? extends Model> select() throws SQLException;


}
