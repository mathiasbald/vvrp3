package models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Candidato extends Pessoa {


    protected List<Beneficiario> beneficiarios = new ArrayList<>();

    public Candidato(String nome, String cpf, String email, String telefone, Date dataNascimento, char sexo, String cep, String cidade, String endereco, String bairro) {
        super(nome, cpf, email, telefone, dataNascimento, sexo, cep, cidade, endereco, bairro);

    }

    public boolean addBeneficiario(Beneficiario beneficiario) {
        if (beneficiario != null) {
            return beneficiarios.add(beneficiario);

        }
        return false;
    }

    public boolean removeBeneficiario(Beneficiario beneficiario) {
        if (beneficiario == null) {
            return beneficiarios.remove(beneficiario);
        }
        return false;
    }



    /**
     * @return the beneficiarios
     */
    public List<Beneficiario> getBeneficiarios() {
        return beneficiarios;
    }

    @Override
    public String toString(){
        return super.toString();
    }

}
