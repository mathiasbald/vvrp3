package models;

public class ProblemaSaude extends Model{
    private String nomeProblema;
    private String descricaoProblema;

    public ProblemaSaude(String nomeProblema, String descricaoProblema) {
        this.nomeProblema = nomeProblema;
        this.descricaoProblema = descricaoProblema;
    }

    public String getNomeProblema() {
        return nomeProblema;
    }

    public String getDescricaoProblema() {
        return descricaoProblema;
    }

    @Override
    public String toString() {
        return "ProblemaSaude{" +
                "nomeProblema='" + nomeProblema + '\'' +
                ", descricaoProblema='" + descricaoProblema + '\'' +
                '}';
    }
}
