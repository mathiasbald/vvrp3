package models;

import java.text.Normalizer;

public enum Predisposicoes {

    CANCER,

    DIABETE,

    DEMENCIA,

    CORACAO,

    HIPERTENSAO,

    PULMONAR,

    OSTEOPOROSE,

    DEGENERACAO;


    static public Predisposicoes getPredisposicao(String s) {
        s = s.toLowerCase();
        s = Normalizer.normalize(s, Normalizer.Form.NFD);
        s = s.replaceAll("[^\\p{ASCII}]", "");
        System.out.println(s);
        if (s.equals("cancer"))
            return CANCER;
        else if (s.equals("diabete") || s.equals("diabetes"))
            return DIABETE;
        else if (s.equals("demencia"))
            return DEMENCIA;
        else if (s.equals("coracao"))
            return CORACAO;
        else if (s.equals("hipertensao"))
            return HIPERTENSAO;
        else if (s.equals("pulmonar"))
            return PULMONAR;
        else if (s.equals("osteoporose"))
            return OSTEOPOROSE;
        else if (s.equals("degenereacao"))
            return DEGENERACAO;
        return null;
    }
}
