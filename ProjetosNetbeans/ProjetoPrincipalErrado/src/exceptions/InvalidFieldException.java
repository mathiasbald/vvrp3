/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Gustavo Bittecourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public class InvalidFieldException extends RuntimeException {

    public InvalidFieldException(String message) {
        super(message);
    }

    public static void validarTexto(JFXTextField field, boolean charactersOnly, String exceptionMessage) throws InvalidFieldException {
        String text = field.getText();
        
        if (charactersOnly && (field == null || text == null || text.startsWith(" ") || text.isEmpty() || text.matches("^[0-9]"))) {
            throw new InvalidFieldException(exceptionMessage);
        }

        if (field == null || text == null || text.startsWith(" ") || text.isEmpty()) {
            throw new InvalidFieldException(exceptionMessage);
        }
    }

    public static void validarEmail(JFXTextField field, String exceptionMessage) throws InvalidFieldException {
        String text = field.getText();
        InvalidFieldException.validarTexto(field, false, "Email inválido");
        Pattern pattern = Pattern.compile("^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(text);
        if (!matcher.matches()) {
            throw new InvalidFieldException(exceptionMessage);
        }
    }

    public static void validarNumero(JFXTextField number, String exceptionMessage) throws InvalidFieldException {
        String text = number.getText();
        if (number == null || text == null || text.startsWith(" ") || text.endsWith(" ") || text.isEmpty() || text.matches("^[0-9]")) {
            throw new InvalidFieldException(exceptionMessage);
        }
    }

    public static void validarQuantidadeDigitos(int number, int numberDigitsExpected, String exceptionMessage) throws InvalidFieldException {
        number = Math.abs(number);

        if ((Math.log10(number) + 1) != numberDigitsExpected) {
            throw new InvalidFieldException(exceptionMessage);
        }
    }

    public static void validarQuantidadeDigitos(long number, int numberDigitsExpected, String exceptionMessage) throws InvalidFieldException {
        number = Math.abs(number);

        if ((Math.log10(number) + 1) != numberDigitsExpected) {
            throw new InvalidFieldException(exceptionMessage);
        }
    }

    public static void validarArquivo(File file, String exceptionMessage) throws InvalidFieldException {
        if(file == null){
            throw new InvalidFieldException(exceptionMessage);
        }
    }
    
    public static void validarComboBox(JFXComboBox<String> comboBox, String exceptionMessage) throws InvalidFieldException {
        String value = comboBox.getValue();
        if(comboBox == null || value == null || value.isEmpty()) {
            throw new InvalidFieldException(exceptionMessage);
        }
    }
    
    public static void validarTelefone(JFXTextField field, String exceptionMessage) throws InvalidFieldException {
        String text = field.getText();
        
        if(field == null || text == null || text.startsWith(" ") || text.endsWith(" ") || text.isEmpty() || (text.length() < 10 || text.length() > 11)){
            throw new InvalidFieldException(exceptionMessage);
        }
    }
}
