/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bancoDeDados;

import java.net.ConnectException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Michael Martins
 */
public class ConexaoNotInfinity {

    private static final String DRIVER = "com.mysql.jdbc.Driver";
    private static final String URL = "jdbc:mysql://localhost:3306/notinfinity";
    private static final String USUARIO = "root";
    private static final String PASS = "";

    public static Connection getConexaoNotInfinity() throws SQLException {
        try {
            Class.forName(DRIVER);
            return DriverManager.getConnection(URL, USUARIO, PASS);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ConexaoNotInfinity.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException("Erro na conexao: " + ex);
        }
    }

    public static void fecharConexao(Connection c) {
        try {
            if (c != null) {
                c.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConexaoNotInfinity.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void fecharConexao(Connection c, PreparedStatement ps) {
        
        fecharConexao(c);
        
        try {
            if (ps != null) {
                ps.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConexaoNotInfinity.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void fecharConexao(Connection c, PreparedStatement ps, ResultSet rs) {
        
        fecharConexao(c, ps);
        
        try {
            if (rs != null) {
                rs.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConexaoNotInfinity.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
