/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import com.jfoenix.controls.JFXCheckBox;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.*;
import models.*;
//import models.mocks.MockCandidato;
//import models.mocks.MockSolicitacaoSeguro1;
//import models.mocks.MockSolicitacaoSeguro2;

import java.io.IOException;
import java.net.URL;
import java.util.*;

/**
 * FXML Controller class
 *
 * @author marti
 */
public class AvaliarCandidatoFinalizarController extends Controller {

    public AvaliarCandidatoFinalizarController() throws IOException {
        super("/views/AvaliarCandidatoFinalizar.fxml");
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {


    }

    @FXML
    void acaoVoltar(ActionEvent event) throws IOException {
        super.setWindow(new AvaliarCandidatoController());
    }

    @FXML
    void avaliar(ActionEvent event) throws IOException {
//        super.setWindow(new AvaliarCandidatoFinalizarController());
    }

}
