/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import com.jfoenix.controls.JFXCheckBox;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.*;
import models.*;

import java.io.IOException;
import java.net.URL;
import java.util.*;

/**
 * FXML Controller class
 *
 * @author marti
 */
public class AvaliarCandidatoController extends Controller {

    //////////////////
    @FXML
    VBox problemsContainer;
    @FXML
    TableView<SolicitacaoSeguro> tv;

    private Set<JFXCheckBox> cbSet = new HashSet<JFXCheckBox>();

    public AvaliarCandidatoController() throws IOException {
        super("/views/AvaliarCandidato.fxml");
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        (tv.getColumns().get(0)).setCellValueFactory(new PropertyValueFactory<>("numeroSolicitacao"));

        (tv.getColumns().get(1)).setCellValueFactory(new PropertyValueFactory<>("nomeCandidato"));

        (tv.getColumns().get(2)).setCellValueFactory(new PropertyValueFactory<>("dataAvaliacao"));

    }

    @FXML
    void acaoVoltar(ActionEvent event) throws IOException {
        super.setWindow(new MenuTesteController());
    }

    @FXML
    void avaliar(ActionEvent event) throws IOException {
        super.setWindow(new AvaliarCandidatoFinalizarController());
    }

}
