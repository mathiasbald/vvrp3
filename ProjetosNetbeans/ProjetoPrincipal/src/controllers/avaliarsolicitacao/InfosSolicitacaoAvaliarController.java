/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.avaliarsolicitacao;

import com.jfoenix.controls.JFXButton;
import controllers.Controller;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import models.*;
import views.Alerta;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * FXML Controller class
 *
 * @author marti
 */
public class InfosSolicitacaoAvaliarController extends Controller {

    @FXML
    Label nSolicitacao;
    @FXML
    Label nomeCandidato;
    @FXML
    VBox vBoxBeneficiarios;
    @FXML
    VBox vBoxProblemas;
    @FXML
    VBox vBoxPreds;
    @FXML
    Label fumante, alcoolista, valorPremio, valorSolicitacao;


    private Candidato candidato;
    private SolicitacaoSeguro solicitacaoSeguro;


    public InfosSolicitacaoAvaliarController(SolicitacaoSeguro selectedItem) throws IOException {
        super("/views/InfosSolicitacaoAvaliar.fxml");
        solicitacaoSeguro = selectedItem;
        candidato = solicitacaoSeguro.getCandidato();

    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        nSolicitacao.setText(solicitacaoSeguro.getNumeroSolicitacao() + "");
        nomeCandidato.setText(candidato.getNome());

        System.out.println(candidato.getBeneficiarios().toString());
        for (Beneficiario beneficiario : candidato.getBeneficiarios()) {
            vBoxBeneficiarios.getChildren().add(new ViewBeneficiario(beneficiario));
        }

        for (ProblemaSaude ps : solicitacaoSeguro.getProblemasSaude()) {
            vBoxProblemas.getChildren().add(new ViewProblemaSaude(ps));
        }
        if(solicitacaoSeguro.getProblemasSaude().size()<=0){
            Label l = new Label("Nenhum Problema");
            l.setFont(new Font(15));
            vBoxProblemas.getChildren().add(l);
        }
        for (Predisposicoes predisposicoes : solicitacaoSeguro.getPredisposicoes()) {
            vBoxPreds.getChildren().add(new ViewPredisposicao(predisposicoes));
        }
        if(solicitacaoSeguro.getPredisposicoes().size()<=0){
            Label l = new Label("Nenhuma Predisposição");
            l.setFont(new Font(15));
            vBoxPreds.getChildren().add(l);
        }

        if(solicitacaoSeguro.isAlcoolista()){
            alcoolista.setText(" Sim");
        }else{
            alcoolista.setText(" Não");
        }
        if(solicitacaoSeguro.isFumante()){
            fumante.setText(" Sim");
        }else{
            fumante.setText(" Não");
        }

        valorPremio.setText(String.format("R$%#.2f", solicitacaoSeguro.getValorPremio()));
        valorSolicitacao.setText(String.format("R$%#.2f", solicitacaoSeguro.getValorSolicitacao()));


    }

    private class ViewPredisposicao extends HBox {
        public ViewPredisposicao(Predisposicoes p) {
            System.out.println("aa"+p);
            this.setAlignment(Pos.CENTER_LEFT);
            Label pred = new Label("- " + p.toString());
            pred.setFont(new Font(15));
            this.getChildren().addAll(pred);
        }
    }

    private class ViewProblemaSaude extends VBox {
        public ViewProblemaSaude(ProblemaSaude ps) {
            this.setAlignment(Pos.TOP_LEFT);
            HBox cima = new HBox();
            HBox baixo = new HBox();
            Label aNome = new Label("Nome do problema: ");
            aNome.setUnderline(true);
            aNome.setFont(new Font(15));

            Label nome = new Label(ps.getNomeProblema());
            nome.setFont(new Font(15));
            cima.getChildren().addAll(aNome, nome);
            Label aDesc = new Label("Descrição: ");
            aDesc.setFont(new Font(15));
            aDesc.setUnderline(true);
            Label desc = new Label(ps.getDescricaoProblema());
            desc.setFont(new Font(15));
            baixo.getChildren().addAll(aDesc, desc);
            this.getChildren().addAll(cima, baixo);
        }
    }

    private class ViewBeneficiario extends HBox {
        public ViewBeneficiario(Beneficiario b) {
            this.setAlignment(Pos.CENTER_LEFT);
            this.paddingProperty().setValue(new Insets(0, 0, 0, 10));
            Label nome = new Label("- " + b.getNome() + " - " + b.getParentesco().toString()+"   ");
            nome.setFont(new Font(15));
            VBox botao = new VBox();
            FontAwesomeIconView v = new FontAwesomeIconView();
            botao.getChildren().addAll(v);
            VBox.setMargin(v,new Insets(2,2,0,2));
            v.setIcon(FontAwesomeIcon.valueOf("INFO"));
            botao.getStyleClass().addAll("btn-form-infos");

            botao.setOnMouseClicked(event ->
                    Alerta.mostrar("Informações do Beneficiario: ", b.toString(), true)
            );

            this.getChildren().addAll(nome, botao);
        }

    }

    @FXML
    void acaoVoltar(ActionEvent event) throws IOException {
        super.setWindow(new ListaSolicitacoesAvaliarController());
    }

    @FXML
    void reprovar(ActionEvent event) throws IOException {
        super.setWindow(new ReprovarAvaliarController(solicitacaoSeguro));
    }

    @FXML
    void contatar(ActionEvent event) throws IOException {
        super.setWindow(new ContatarAvaliarController(solicitacaoSeguro));
    }

    @FXML
    void verInfosCandidato(Event event) {
        Alerta.mostrar("Informações do Candidato: ", candidato.toString(), true);
    }
}
