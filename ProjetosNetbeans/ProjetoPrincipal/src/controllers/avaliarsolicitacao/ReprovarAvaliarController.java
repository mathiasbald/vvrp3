/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.avaliarsolicitacao;

import com.jfoenix.controls.JFXTextArea;
import controllers.Controller;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import models.Candidato;
import models.SolicitacaoSeguro;
import models.factories.SolicitacaoSeguroFactory;
import views.Alerta;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * FXML Controller class
 *
 * @author marti
 */
public class ReprovarAvaliarController extends Controller {

    @FXML
    Label nSolicitacao;
    @FXML
    Label nomeCandidato;
    @FXML
    JFXTextArea motivo;
    private Candidato candidato;
    private SolicitacaoSeguro solicitacaoSeguro;


    public ReprovarAvaliarController(SolicitacaoSeguro selectedItem) throws IOException {
        super("/views/ReprovarAvaliar.fxml");
        solicitacaoSeguro = selectedItem;
        candidato = solicitacaoSeguro.getCandidato();

    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        nSolicitacao.setText(solicitacaoSeguro.getNumeroSolicitacao() + "");
        nomeCandidato.setText(candidato.getNome());

    }


    @FXML
    void acaoVoltar(Event event) throws IOException {
        Alerta.addBtn("Sim", "grey", "white", acao -> {
            try {
                super.setWindow(new InfosSolicitacaoAvaliarController(solicitacaoSeguro));
            } catch (IOException e) {
                e.printStackTrace();
            }

        });
        Alerta.addBtn("Não", "grey", "white", acao -> {
            Alerta.desaparecer();
        });

        Alerta.mostrar("Confirmar Saida", "Deseja realmente sair? Todas alterações serão descartadas", true);

    }

    @FXML
    void voltarSelecionar(Event event) throws IOException {
        Alerta.addBtn("Sim", "grey", "white", acao -> {
            try {
                super.setWindow(new ListaSolicitacoesAvaliarController());
            } catch (IOException e) {
                e.printStackTrace();
            }

        });
        Alerta.addBtn("Não", "grey", "white", acao -> {
            Alerta.desaparecer();
        });

        Alerta.mostrar("Confirmar Saida", "Deseja realmente sair? Todas alterações serão descartadas", true);

    }

    @FXML
    void reprovar(ActionEvent event) throws IOException {

        if (motivo.getText().isEmpty()) {
            Alerta.mostrar("Impossivel Reprovar", "Informe o motivo da reprovação", true);
            return;
        }
        Alerta.addBtn("Sim", "green", "white", acao -> {


            solicitacaoSeguro.reprovar(motivo.getText());
            SolicitacaoSeguroFactory.atualizarSolicitacaoDeSeguro(solicitacaoSeguro);
            Alerta.mostrar("Concluído", "Solicitacao reprovada com sucesso", true);
            System.out.println("done-reprovado");
            try {
                voltarSelecionar(event);
            } catch (IOException e) {
                e.printStackTrace();
            }

        });
        Alerta.addBtn("Não", "red", "white", acao -> {
            Alerta.desaparecer();
        });

        Alerta.mostrar("Confirmar Saida", "Deseja realmente sair? Todas alterações serão descartadas", true);


    }


    @FXML
    void verInfosCandidato(ActionEvent event) {
        Alerta.mostrar("Informações do Candidato: ", candidato.toString(), true);
    }
}
