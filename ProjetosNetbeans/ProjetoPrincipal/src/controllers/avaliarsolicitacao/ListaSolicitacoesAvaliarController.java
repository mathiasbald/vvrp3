/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.avaliarsolicitacao;

import com.jfoenix.controls.JFXCheckBox;
import controllers.Controller;
import controllers.LoginController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.*;
import models.*;
import models.factories.SolicitacaoSeguroFactory;
import views.Alerta;

import java.io.IOException;
import java.net.URL;
import java.util.*;

/**
 * FXML Controller class
 *
 * @author marti
 */
public class ListaSolicitacoesAvaliarController extends Controller {


    //////////////////
    @FXML
    VBox problemsContainer;
    @FXML
    TableView<SolicitacaoSeguro> tv;

    private Set<JFXCheckBox> cbSet = new HashSet<JFXCheckBox>();
    private Corretor candidato = (Corretor) Session.getInstance().getPessoa();

    public ListaSolicitacoesAvaliarController() throws IOException {
        super("/views/ListaSolicitacoesAvaliar.fxml");
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {


        (tv.getColumns().get(0)).setCellValueFactory(new PropertyValueFactory<>("numeroSolicitacao"));

        (tv.getColumns().get(1)).setCellValueFactory(new PropertyValueFactory<>("nomeCandidato"));

        (tv.getColumns().get(2)).setCellValueFactory(new PropertyValueFactory<>("dataSolicitacaoString"));

        (tv.getColumns().get(3)).setCellValueFactory(new PropertyValueFactory<>("valorPremioString"));

        (tv.getColumns().get(4)).setCellValueFactory(new PropertyValueFactory<>("valorSolicitacaoString"));

        tv.getItems().addAll(SolicitacaoSeguroFactory.getTodasSolicitacoesComDataVisitaNaoMarcadasENaoAvaliadas());


    }

    @FXML
    void acaoVoltar(ActionEvent event) throws IOException {
        super.setWindow(new LoginController(true));
    }

    @FXML
    void avaliar(ActionEvent event) throws IOException {
        if (tv.getSelectionModel().getSelectedItem() != null)
            super.setWindow(new InfosSolicitacaoAvaliarController(tv.getSelectionModel().getSelectedItem()));
        else
            Alerta.mostrar("Não é possível continuar","Selecione ao menos uma solicitação de seguro para avaliar",true);
    }


}
