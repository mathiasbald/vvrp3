package exceptions;

public class NotFoundOnDBException extends Exception {
    public NotFoundOnDBException(String message){
        super(message);
    }
}
