/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.dao;

import exceptions.NotFoundOnDBException;
import models.*;
import models.factories.CandidatoFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Gustavo Bittecourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public class SolicitacaoSeguroDao extends DAO {

    public SolicitacaoSeguroDao() {
        super();
    }

    @Override
    public void delete(Model model) {

    }

    @Override
    public void update(Model model) throws SQLException {
        conexao.abrirConexao();
        SolicitacaoSeguro solicitacaoSeguro = this.converter(model);

        PreparedStatement stmt = conexao.prepararDeclaracao(
                "UPDATE solicitacoes SET datavisitacandidato=?, " +
                        "aprovadasolicitacao=?, " +
                        "motivoreprovacao=?, " +
                        "alteracoes=?, " +
                        "dataalteracao=? " +
                        "WHERE id=?"
        );
        if (solicitacaoSeguro.getDataVisitaCandidato() == null)
            stmt.setNull(1, Types.DATE);
        else {
            Date d = solicitacaoSeguro.getDataVisitaCandidato();
            stmt.setString(1, (d.getYear() + 1900) + "-" + (d.getMonth() + 1) + "-" + d.getDate());
        }
        if (!solicitacaoSeguro.isAprovadaSolicitacao() && solicitacaoSeguro.getMotivoReprovacao() == null) {
            stmt.setNull(2, Types.BOOLEAN);
            stmt.setNull(3, Types.VARCHAR);
        } else {
            stmt.setBoolean(2, solicitacaoSeguro.isAprovadaSolicitacao());
            if (solicitacaoSeguro.getMotivoReprovacao() == null)
                stmt.setNull(3, Types.VARCHAR);
            else
                stmt.setString(3, solicitacaoSeguro.getMotivoReprovacao());
        }
        if (solicitacaoSeguro.getAlteracoes() == null) {
            stmt.setNull(4, Types.VARCHAR);
            stmt.setNull(5, Types.DATE);
        } else {
            stmt.setString(4, solicitacaoSeguro.getAlteracoes());
            Date d = solicitacaoSeguro.getDataVisitaCandidato();
            stmt.setString(5, (d.getYear() + 1900) + "-" + (d.getMonth() + 1) + "-" + d.getDate());
        }
        stmt.setInt(6, solicitacaoSeguro.getId());
        stmt.execute();
    }


    @Override
    public void insert(Model model) throws SQLException, IllegalStateException, IOException {
        SolicitacaoSeguro solicitacaoSeguro = this.converter(model);
        conexao.abrirConexao();

        PreparedStatement stmt = conexao.prepararDeclaracao(
                "INSERT INTO solicitacoes ( datasolicitacao, valorpremio, valorsolicitacao, fumante, alcoolista, idpessoa)"
                        + "VALUES ( ?, ?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);

        Date data = solicitacaoSeguro.getDataSolicitacao();
        //stmt.setLong(1, solicitacaoSeguro.getCandidato().getID());
        stmt.setString(1, (data.getYear() + 1900) + "-" + (data.getMonth() + 1) + "-" + data.getDate());
        stmt.setDouble(2, solicitacaoSeguro.getValorPremio());
        stmt.setDouble(3, solicitacaoSeguro.getValorSolicitacao());
        stmt.setBoolean(4, solicitacaoSeguro.isFumante());
        stmt.setBoolean(5, solicitacaoSeguro.isAlcoolista());
        stmt.setInt(6, solicitacaoSeguro.getCandidato().getID());

        stmt.executeUpdate();
        ResultSet rs = stmt.getGeneratedKeys();
        rs.first();
        int idSol = rs.getInt(1);
        stmt.close();

        for (ProblemaSaude problemaSaude : solicitacaoSeguro.getProblemasSaude()) {
            PreparedStatement stmtP = conexao.prepararDeclaracao(
                    "INSERT INTO problemasaude (nome, descricao, idsolicitacao)"
                            + "VALUES (?, ?, ?)");
            stmtP.setString(1, problemaSaude.getNomeProblema());
            stmtP.setString(2, problemaSaude.getDescricaoProblema());
            stmtP.setInt(3, idSol);
            stmtP.executeUpdate();
            stmtP.close();
        }
        for (Predisposicoes pred : solicitacaoSeguro.getPredisposicoes()) {
            PreparedStatement stmtS = conexao.prepararDeclaracao(
                    "select id from predisposicoes where nome like ?");
            stmtS.setString(1, "%" + pred.toString() + "%");

            ResultSet resultado = stmtS.executeQuery();
            if (resultado.first()) {
                int idPred = resultado.getInt(1);
                PreparedStatement stmtP = conexao.prepararDeclaracao(
                        "INSERT INTO sol_pred(idpred, idsolicitacao)"
                                + "VALUES (?, ?)");
                stmtP.setInt(1, idPred);
                stmtP.setInt(2, idSol);
                stmtP.executeUpdate();
                stmtS.close();
                stmtP.close();
            }
        }
        conexao.fecharConexao();
    }

    private SolicitacaoSeguro converter(Model model) {
        SolicitacaoSeguro modelToConverter;

        if (model instanceof SolicitacaoSeguro) {
            modelToConverter = (SolicitacaoSeguro) model;
        } else {
            throw new ClassCastException("A model não é uma solicitação de seguro");
        }

        return modelToConverter;
    }

    /*
     */
    @Override
    public List<Model> select() throws SQLException {

        conexao.abrirConexao();
        List<Model> retorno = new ArrayList<>();
        PreparedStatement stmt = conexao.prepararDeclaracao("select * from solicitacoes");

        ResultSet rsSol = stmt.executeQuery();
        while (rsSol.next()) {
            List<Predisposicoes> predisposicoes = new ArrayList<>();
            PreparedStatement stmtPreds = conexao.prepararDeclaracao("select idpred from sol_pred where idsolicitacao=?");
            stmtPreds.setInt(1, rsSol.getInt("id"));
            stmtPreds.executeQuery();
            ResultSet rsPreds = stmtPreds.executeQuery();

            while (rsPreds.next()) {
                PreparedStatement stmtInterno = conexao.prepararDeclaracao("select nome from predisposicoes where id=?");
                stmtInterno.setInt(1, rsPreds.getInt("idpred"));
                stmtInterno.executeQuery();
                ResultSet rs3 = stmtInterno.executeQuery();
                rs3.first();
                predisposicoes.add(Predisposicoes.getPredisposicao(rs3.getString("nome")));
            }


            Candidato candidato = null;
            try {
                candidato = CandidatoFactory.getCandidatoById(rsSol.getInt("idpessoa"));
            } catch (NotFoundOnDBException e) {
                System.out.println("Inconsistencia no DB: " + e.getMessage());
            }
            SolicitacaoSeguro s = new SolicitacaoSeguro(rsSol.getInt("id"),
                    rsSol.getDate("datasolicitacao"),
                    rsSol.getDouble("valorpremio"),
                    rsSol.getDouble("valorsolicitacao"),
                    rsSol.getBoolean("fumante"),
                    rsSol.getBoolean("alcoolista"),
                    predisposicoes,
                    candidato);

            s.setId(rsSol.getInt("id"));
            java.sql.Date date = rsSol.getDate("datavisitacandidato");
            s.setDataVisitaCandidato(date);
            s.setAprovadaSolicitacao(rsSol.getBoolean("aprovadasolicitacao"));
            s.setMotivoReprovacao(rsSol.getString("motivoreprovacao"));
            s.setAlteracoes(rsSol.getString("alteracoes"));
            s.setDataAlteracao(rsSol.getDate("dataalteracao"));



            PreparedStatement stmtProbs = conexao.prepararDeclaracao("select nome, descricao from problemasaude where idsolicitacao=?");
            stmtProbs.setInt(1, rsSol.getInt("id"));
            stmtProbs.executeQuery();
            ResultSet rsProbs = stmtProbs.executeQuery();
            while (rsProbs.next()) {
                s.adicionarProblema(new ProblemaSaude(rsProbs.getString("nome"), rsProbs.getString("descricao")));
            }


            retorno.add(s);
        }
        return retorno;
    }

}
