/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package models.dao;

import bancoDeDados.ConexaoNotInfinity;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Model;
import models.Usuario;

/**
 * 
 * @author Michael Martins
 */
public class CadastroDAO{

   public void create(Usuario user) throws SQLException{
    Connection c = ConexaoNotInfinity.getConexaoNotInfinity();
    PreparedStatement ps = null;    
       try {

        ps = c.prepareStatement("CREATE IF NOT EXISTS usuarios( "
                + "username VARCHAR(60) NOT NULL, "
                + "password VARCHAR(60) NOT NULL, "
                + "acesstoken VARCHAR(60) NOT NULL, "
                + "salt VARCHAR (60) NOT NULL");
        
        ps = c.prepareStatement("INSERT INTO usuarios (username,password, acesstoken, salt) VALUES(?,?,?,?)");
        ps.setString(0, user.getUsuario());
        ps.setString(1, user.getSenha());
        ps.setString(2, user.getEmail());
        
        ps.executeUpdate();
       } catch (SQLException ex) {
           Logger.getLogger(CadastroDAO.class.getName()).log(Level.SEVERE, null, ex);
       }finally{
           ConexaoNotInfinity.fecharConexao(c, ps);
       }
   }
}
