/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.dao;

import models.*;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Gustavo Bittecourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public class CandidatoDao extends DAO {

    public CandidatoDao() {
        super();
    }

    @Override
    public void delete(Model model) {

    }

    @Override
    public void update(Model model) {

    }


    @Override
    public void insert(Model model) throws SQLException, IllegalStateException, IOException {
    }

    private Candidato converter(Model model) {
        Candidato modelToConverter = null;

        if (model instanceof Candidato) {
            modelToConverter = (Candidato) model;
        } else {
            throw new ClassCastException("A model não é uma solicitação de seguro");
        }

        return modelToConverter;
    }

    @Override
    public List<Model> select() throws SQLException {
        conexao.abrirConexao();
        List<Model> retorno = new ArrayList<>();
        PreparedStatement stmt = conexao.prepararDeclaracao("select pessoas.id as id, nome, cpf, email, telefone, " +
                "datanascimento, sexo, cep, endereco, cidade, bairro " +
                "from pessoas, tipopessoa where tipopessoa.id=pessoas.tipopessoa and tipopessoa.descricao=?");
        stmt.setString(1, "Candidato");
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            //id, nome, cpf, email, telefone, datanascimento, sexo, cep
            Candidato c = new Candidato(
                    rs.getString("nome"),
                    rs.getString("cpf"), //cpf
                    rs.getString("email"),
                    rs.getString("telefone"),
                    rs.getDate("datanascimento"), //nasc
                    rs.getString("sexo").toUpperCase().charAt(0), //sexo
                    rs.getString("cep"), //cep
                    rs.getString("cidade"),//cidade
                    rs.getString("endereco"), //endereco
                    rs.getString("bairro"));//bairro
            c.setId(rs.getInt("id"));

            PreparedStatement stmtBen = conexao.prepararDeclaracao("select tipoparentesco.descricao as nomeparentesco, " +
                    "pessoas.id, nome, cpf, email, telefone, datanascimento, sexo, cep, endereco, cidade, bairro " +
                    "from beneficiarios, pessoas, tipoparentesco " +
                    "where idbeneficiario=pessoas.id " +
                    "and parentesco=tipoparentesco.id " +
                    "and idcandidato=?");

            stmtBen.setInt(1, rs.getInt("id"));

            ResultSet rsBen = stmtBen.executeQuery();
            while (rsBen.next()) {
                Beneficiario b = new Beneficiario(
                        Parentesco.getParentesco(rsBen.getString("nomeparentesco")),
                        rsBen.getString("nome"),
                        rsBen.getString("cpf"), //cpf
                        rsBen.getString("email"),
                        rsBen.getString("telefone"),
                        rsBen.getDate("datanascimento"), //nasc
                        rsBen.getString("sexo").charAt(0), //sexo
                        rsBen.getString("cep"), //cep
                        rsBen.getString("cidade"),//cidade
                        rsBen.getString("endereco"), //endereco
                        rsBen.getString("bairro"));//bairro


                c.setId(rs.getInt("id"));
                c.addBeneficiario(b);
            }
            retorno.add(c);
        }
        return retorno;
    }


}
