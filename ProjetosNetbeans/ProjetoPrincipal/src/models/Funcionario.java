package models;

import java.util.Date;


public class Funcionario extends Pessoa {
    private Date dataContratacao;
    private boolean ativo;

    public Funcionario(String nome, String cpf, String email, String telefone, Date dataNascimento, char sexo, String cep, String cidade, String endereco, String bairro, Date dataContratacao, boolean ativo) {
        super(nome, cpf, email, telefone, dataNascimento, sexo, cep, cidade, endereco, bairro);
        this.setDataContratacao(dataContratacao);
        this.setAtivo(ativo);
    }

    public Date getDataContratacao() {
        return dataContratacao;
    }

    public void setDataContratacao(Date dataContratacao) {
        this.dataContratacao = dataContratacao;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }
}
