package models.factories;

import exceptions.NotFoundOnDBException;
import models.Candidato;
import models.Model;
import models.dao.CandidatoDao;

import java.sql.SQLException;

public class CandidatoFactory {
   static public Candidato getCandidatoById(int id) throws NotFoundOnDBException {
       CandidatoDao cd = new CandidatoDao();
       try {
           for (Model m : cd.select()) {
               if (m instanceof Candidato) {
                   if (((Candidato) m).getId() == id) {
                       return (Candidato) m;
                   }
               }
           }
       } catch (SQLException e) {
           e.printStackTrace();
       }
       throw new NotFoundOnDBException("Não foi possível encontrar o Candidato pelo ID informado");

   }
}
