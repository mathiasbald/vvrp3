/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Gustavo Bittecourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public class InvalidValueException extends RuntimeException {

    public InvalidValueException(String message) {
        super(message);
    }

    public static void validarTexto(String text, boolean charactersOnly, String exceptionMessage) throws InvalidValueException {
        text = removerEspaco(text, exceptionMessage);

        if (charactersOnly && (text.isEmpty() || text.matches("[^a-zA-Z]"))) {
            throw new InvalidValueException(exceptionMessage);
        }

        if (text.isEmpty()) {
            throw new InvalidValueException(exceptionMessage);
        }
    }

    public static void validarEmail(String text, String exceptionMessage) throws InvalidValueException {
        InvalidValueException.validarTexto(text, false, exceptionMessage);

        Pattern pattern = Pattern.compile("^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(text);
        if (!matcher.matches()) {
            throw new InvalidValueException(exceptionMessage);
        }
    }

    public static void validarNumero(String text, String exceptionMessage) throws InvalidValueException {
        text = removerEspaco(text, exceptionMessage);
        if (text.isEmpty() || !text.matches("^[0-9]{1,}$")) {
            throw new InvalidValueException(exceptionMessage);
        }
    }

    public static void validarQuantidadeDigitos(int number, int numberDigitsExpected, String exceptionMessage) throws InvalidValueException {
        number = Math.abs(number);

        if (((int) Math.log10(number) + 1) != numberDigitsExpected) {
            System.out.println((Math.log10(number) + 1));
            throw new InvalidValueException(exceptionMessage);
        }
    }

    public static void validarQuantidadeDigitos(long number, int numberDigitsExpected, String exceptionMessage) throws InvalidValueException {
        number = Math.abs(number);

        if (((int) Math.log10(number) + 1) != numberDigitsExpected) {
            throw new InvalidValueException(exceptionMessage);
        }
    }

    public static void validarArquivo(File file, String exceptionMessage) throws InvalidValueException {
        if (file == null) {
            throw new InvalidValueException(exceptionMessage);
        }
    }

    public static void validarComboBox(String value, String exceptionMessage) throws InvalidValueException {
        value = removerEspaco(value, exceptionMessage);
        if (value.isEmpty()) {
            throw new InvalidValueException(exceptionMessage);
        }
    }

    public static void validarTelefone(String text, String exceptionMessage) throws InvalidValueException {
        text = removerEspaco(text, exceptionMessage);
        if (text.isEmpty() || (text.length() < 10 || text.length() > 11)) {
            throw new InvalidValueException(exceptionMessage);
        }
    }
    
    public static void validarNomeCompleto(String text, String exceptionMessage) throws InvalidValueException {
        text = removerEspaco(text, exceptionMessage);
        InvalidValueException.validarTexto(text, true, exceptionMessage);
        
        if(text.split(" ").length < 2){
            throw new InvalidValueException(exceptionMessage);
        }
    }

    private static String removerEspaco(String text, String exceptionMessage) throws InvalidValueException {
        if (text == null) {
            throw new InvalidValueException(exceptionMessage);
        }

        return text.trim();
    }
}
