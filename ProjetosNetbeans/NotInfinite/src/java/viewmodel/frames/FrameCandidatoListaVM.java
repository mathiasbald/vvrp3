package viewmodel.frames;

import models.Predisposicoes;
import models.ProblemaSaude;
import models.Session;
import models.SolicitacaoSeguro;
import models.dao.SolicitacaoSeguroDAO;
import viewmodel.ViewModel;
import views.frames.*;
import views.panels.PainelView;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;

public class FrameCandidatoListaVM extends ViewModel {

    private SolicitacaoSeguro selecionadaFase1;
    private SolicitacaoSeguro selecionadaFase2;

    public void requestForSolicitacoes() throws SQLException {
        ((FrameCandidatoListaSolicitacoesView) myView).setListaSolicitacoes(new SolicitacaoSeguroDAO().getSolicitacoesCandidato(Session.getInstance().getPessoa()));
    }

    public void verHistoricoSolicitacaoSelecionada() throws IOException {
        SolicitacaoSeguro s = ((FrameCandidatoListaSolicitacoesView) myView).getSolicitacaoSelecionada();
        if (s != null) {
            selecionadaFase1 = s;
            ((Frame) myView).getPainelView().setContentFrame(new FrameCandidatoListaHistoricoView(this, ((Frame) myView).getPainelView()));
        }
    }


    public void voltarParaLista() throws IOException {
        selecionadaFase1=null;
        selecionadaFase2=null;
        ((Frame) myView).getPainelView().setContentFrame(new FrameCandidatoListaSolicitacoesView(this, ((Frame) myView).getPainelView()));
    }

    public void requestForHistoricorSolicitacoes() throws SQLException {
        ((FrameCandidatoListaHistoricoView) myView).setListaSolicitacoes(new SolicitacaoSeguroDAO().getHistoricoSolicitacoes(selecionadaFase1));
    }

    public void exibirInformacoesSolicitacaoSelecionada() throws IOException {
        SolicitacaoSeguro s = ((FrameCandidatoListaHistoricoView) myView).getSolicitacaoSelecionada();
        if (s != null) {
            selecionadaFase2 = s;
            ((Frame) myView).getPainelView().setContentFrame(new FrameCandidatoVisualizarSolicitacaoView(this, ((Frame) myView).getPainelView()));

        }

    }

    public void requestFieldsValueSet() {
        FrameCandidatoVisualizarSolicitacaoView view = (FrameCandidatoVisualizarSolicitacaoView) myView;
        view.setNumeroSolicitacao(selecionadaFase2.getNumeroSolicitacao() + "");
        view.setValorPremio(selecionadaFase2.getValorPremioString());
        view.setValorDesejado(selecionadaFase2.getValorSolicitacaoString());
        view.setIsFumanteCandidato(selecionadaFase2.isFumante() ? "Sim" : "Não");
        view.setIsAlcoolistaCandidato(selecionadaFase2.isAlcoolista() ? "Sim" : "Não");

        for (ProblemaSaude p : selecionadaFase2.getProblemasSaude()) {
            view.addProblema(p);
        }
        for (Predisposicoes p : selecionadaFase2.getPredisposicoes()) {
            view.addPredisposicao(p);
        }
        if (!selecionadaFase2.isValido()) {
            view.setEditarBtnDisabled();
        }
    }

    public void editarSolicitacao() throws IOException {
        ((Frame) myView).getPainelView().setContentFrame(new FrameCandidatoEditarSolicitacaoView(this, ((Frame) myView).getPainelView()));
    }

    public void voltarParaHisorico() throws IOException {
        ((Frame) myView).getPainelView().setContentFrame(new FrameCandidatoListaHistoricoView(this, ((Frame) myView).getPainelView()));
        selecionadaFase2=null;
    }

    public void requestFieldsValueSetOnEditScreen() {
        FrameCandidatoEditarSolicitacaoView view = (FrameCandidatoEditarSolicitacaoView) myView;

        view.setNumeroSolicitacao(selecionadaFase2.getNumeroSolicitacao() + "");

        for (ProblemaSaude p : selecionadaFase2.getProblemasSaude()) {
            view.adicionarProblema(p.getNomeProblema(), p.getDescricaoProblema());
        }
        for (Predisposicoes p : selecionadaFase2.getPredisposicoes()) {
            switch (p) {
                case CANCER:
                    view.checkPredCancer(true);
                    break;
                case CORACAO:
                    view.checkPredCardio(true);
                    break;
                case DIABETE:
                    view.checkPredDiabete(true);
                    break;
                case DEMENCIA:
                    view.checkPredDemencia(true);
                    break;
                case PULMONAR:
                    view.checkPredPulmo(true);
                    break;
                case DEGENERACAO:
                    view.checkPredDegeneracao(true);
                    break;
                case HIPERTENSAO:
                    view.checkPredHiper(true);
                    break;
                case OSTEOPOROSE:
                    view.checkPredOsteo(true);
                    break;

            }
        }
        view.checkAlcoolista(selecionadaFase2.isAlcoolista());
        view.checkFumante(selecionadaFase2.isFumante());

        System.out.println(selecionadaFase2.getValorSolicitacao());
        view.setValorDesejado(selecionadaFase2.getValorSolicitacao());
    }

    public void pedirAlteracaoValorPremio() {
        FrameCandidatoEditarSolicitacaoView view = (FrameCandidatoEditarSolicitacaoView) myView;
        float r = selecionadaFase2.calcularValorPremio(view.getValorDesejado(), view.getTotalPredisposicoesSelecionadas(), view.isSelectedFumante(), view.isSelectedAlcoolista());
        view.setValorSimulado(r);
    }

    public void voltarParaInformacoes() throws IOException {
        ((Frame) myView).getPainelView().setContentFrame(new FrameCandidatoVisualizarSolicitacaoView(this, ((Frame) myView).getPainelView()));
    }

    public boolean salvarAlteracoes() throws SQLException, IOException {
        FrameCandidatoEditarSolicitacaoView view = (FrameCandidatoEditarSolicitacaoView) myView;


        HashMap<String, String> hm = view.getProblemas();
        selecionadaFase2.getProblemasSaude().clear();
        hm.forEach((s, s2) -> selecionadaFase2.adicionarProblema(new ProblemaSaude(s, s2)));

        selecionadaFase2.getPredisposicoes().clear();
        if (view.isSelectedPredCancer()) {
            selecionadaFase2.addPredisposicao(Predisposicoes.CANCER);
        }
        if (view.isSelectedPredCardio()) {
            selecionadaFase2.addPredisposicao(Predisposicoes.CORACAO);
        }
        if (view.isSelectedPredDegeneracao()) {
            selecionadaFase2.addPredisposicao(Predisposicoes.DEGENERACAO);
        }
        if (view.isSelectedPredDemencia()) {
            selecionadaFase2.addPredisposicao(Predisposicoes.DEMENCIA);
        }
        if (view.isSelectedPredDiabete()) {
            selecionadaFase2.addPredisposicao(Predisposicoes.DIABETE);
        }
        if (view.isSelectedPredHiper()) {
            selecionadaFase2.addPredisposicao(Predisposicoes.HIPERTENSAO);
        }
        if (view.isSelectedPredOsteo()) {
            selecionadaFase2.addPredisposicao(Predisposicoes.OSTEOPOROSE);
        }
        if (view.isSelectedPredPulmo()) {
            selecionadaFase2.addPredisposicao(Predisposicoes.PULMONAR);
        }

        selecionadaFase2.setFumante(view.isSelectedFumante());
        selecionadaFase2.setAlcoolista(view.isSelectedAlcoolista());

        selecionadaFase2.setValorSolicitacao(view.getValorDesejado());

        selecionadaFase2.calcularValorPremio(true);
        if (selecionadaFase2.getValorSolicitacao() < 0) {
            view.showAlert("ERRO EM CAMPO OBRIGATÓRIO", "Digite um valor maior que 0 no campo \"Valor desejado\"");
            return false;
        }
        selecionadaFase2.setAlterador(Session.getInstance().getPessoa());
        ((Frame) myView).getPainelView().setContentFrame(new FrameCandidatoListaSolicitacoesView(this, ((Frame) myView).getPainelView()));
        new SolicitacaoSeguroDAO().salvarAlteracoesNaSolicitacao(selecionadaFase2);
        selecionadaFase1=null;
        selecionadaFase2=null;
        return true;
    }

}
