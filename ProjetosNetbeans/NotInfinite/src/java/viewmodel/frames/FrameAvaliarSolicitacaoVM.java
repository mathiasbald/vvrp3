package viewmodel.frames;

import models.*;
import models.dao.ISolicitacaoSeguroDAO;
import models.dao.SolicitacaoSeguroDAO;
import viewmodel.ViewModel;
import views.frames.Frame;
import views.frames.FrameAvaliarSolicitacaoListaView;
import views.frames.FrameAvaliarSolicitacaoVisualizarView;

import java.io.IOException;
import java.sql.SQLException;

public class FrameAvaliarSolicitacaoVM extends ViewModel {

    private SolicitacaoSeguro solicitacaoSeguro;

    public void requestForSolicitacoes() throws SQLException {
        ISolicitacaoSeguroDAO ssd = new SolicitacaoSeguroDAO();
        ((FrameAvaliarSolicitacaoListaView) myView).setListaSolicitacoes(ssd.getSolicitacoesSemDataDeConsultaMarcada());
    }

    public void avaliar() throws IOException {
        SolicitacaoSeguro s = ((FrameAvaliarSolicitacaoListaView) myView).getSolicitacaoSelecionada();
        if (s != null) {
            solicitacaoSeguro = s;
            ((Frame) myView).getPainelView().setContentFrame(new FrameAvaliarSolicitacaoVisualizarView(this, ((Frame) myView).getPainelView()));
        }
    }




    //tela visualizar
    public void voltarParaLista() throws IOException {
        ((Frame) myView).getPainelView().setContentFrame(new FrameAvaliarSolicitacaoListaView(this, ((Frame) myView).getPainelView()));
    }

    public void marcarConsulta(int dia, int mes, int ano, int hora, int minuto) throws SQLException, IOException {
        solicitacaoSeguro.setDataVisitaCandidato(dia, mes, ano  , hora, minuto);

        new SolicitacaoSeguroDAO().atualizarSolicitacao(solicitacaoSeguro);
        voltarParaLista();
    }

    public void reprovarSolicitacao(String motivo) throws SQLException, IOException {
        solicitacaoSeguro.reprovar(motivo);
        new SolicitacaoSeguroDAO().atualizarSolicitacao(solicitacaoSeguro);
        voltarParaLista();
    }


    public void requestFieldsValueSetOnViewScreen() {
        FrameAvaliarSolicitacaoVisualizarView view = (FrameAvaliarSolicitacaoVisualizarView) myView;

        view.setNomeCandidato(solicitacaoSeguro.getCandidato().getNome());
        view.setEnderecoCandidato(solicitacaoSeguro.getCandidato().getEndereco() + ", " + solicitacaoSeguro.getCandidato().getBairro() + ", " + solicitacaoSeguro.getCandidato().getCidade());
        view.setNumeroSolicitacao(solicitacaoSeguro.getNumeroSolicitacao() + "");
        view.setValorPremio(solicitacaoSeguro.getValorPremioString());
        view.setTelefoneCandidato(solicitacaoSeguro.getCandidato().getTelefone());
        view.setIsFumanteCandidato(solicitacaoSeguro.isFumante() ? "Sim" : "Não");
        view.setIsAlcoolistaCandidato(solicitacaoSeguro.isAlcoolista() ? "Sim" : "Não");
        view.setValorDesejado(solicitacaoSeguro.getValorSolicitacaoString());

        for (ProblemaSaude p : solicitacaoSeguro.getProblemasSaude()) {
            view.addProblema(p);
        }
        for (Predisposicoes p : solicitacaoSeguro.getPredisposicoes()) {
            view.addPredisposicao(p);
        }
        Pair<Boolean, String> p=solicitacaoSeguro.podeSerAprovada();
        if(!p.getFirst()){
            view.setAvaliarBtnDisabled(p.getSecond());
        }
        String s = "";
        for (Beneficiario b:solicitacaoSeguro.getCandidato().getBeneficiarios()) {
            s+=b.getNome()+" - "+b.getParentesco()+"\n";
        }
        view.setBeneficiariosBtnPopupMessage("Beneficiarios", s);

        view.setDetalhesBtn("Detalhes do candidato:" ,solicitacaoSeguro.getCandidato().toString());
    }

}
