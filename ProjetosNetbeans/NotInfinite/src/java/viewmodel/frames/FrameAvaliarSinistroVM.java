/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewmodel.frames;

import com.sun.javaws.exceptions.InvalidArgumentException;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import models.Session;
import models.Sinistro;
import models.dao.SinistroDAO;
import viewmodel.ViewModel;
import views.frames.Frame;
import views.frames.FrameListarSinistrosView;

/**
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public class FrameAvaliarSinistroVM extends ViewModel {

    public void visualizarCertidao(File certidao) throws IOException {
        if(!certidao.exists()){
            throw new IOException("Certidão não pôde ser encontrada.");
        }

        Desktop.getDesktop().open(certidao);
    }

    public void cancelar(Sinistro sinistro) throws IOException {
        sinistro.destruir();
        ((Frame) myView).getPainelView().setContentFrame(new FrameListarSinistrosView(new FrameListarSinistrosVM(), ((Frame) myView).getPainelView()));
    }

    public void avaliarSinistro(Sinistro sinistro, String parecer, boolean autorizado) throws SQLException {
        parecer = parecer.trim();
        if(parecer.isEmpty()){
            throw new IllegalArgumentException("Para prosseguir é necessário informar o parecer.");
        }
        
        SinistroDAO sinistroDAO = new SinistroDAO();
        sinistroDAO.avaliarSinistro(sinistro, Session.getInstance().getPessoa().getID(), parecer, autorizado);
    }
}
