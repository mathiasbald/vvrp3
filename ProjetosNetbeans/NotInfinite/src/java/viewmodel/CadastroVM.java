/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewmodel;

import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Pessoa;
import models.Usuario;
import models.dao.CadastroDAO;
import views.Alerta;
import views.CadastroView;
import views.LoginView;

/**
 *
 * @author martins
 */
public class CadastroVM extends ViewModel {

    public void telaLogin() throws IOException {
        new LoginView(new LoginVM()).show();
    }

    public void cadastrarUsuario(Usuario user, Pessoa pessoa) {
        try {
            CadastroDAO dao = new CadastroDAO();
            dao.adicionarUsuario(user, pessoa);
        } catch (SQLException ex) {
           new Alerta().exibir("ERRO", "Ocorreu um erro enquanto cadastrava. Tente novamente!");
            Logger.getLogger(CadastroView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
