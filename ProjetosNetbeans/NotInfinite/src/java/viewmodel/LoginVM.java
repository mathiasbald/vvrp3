/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewmodel;

import exceptions.EmptyFieldException;
import exceptions.IncorrectPasswordException;
import exceptions.IncorrectUsernameException;
import exceptions.NullFieldException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import models.Session;
import models.dao.FuncionarioDAO;

import viewmodel.panels.*;
import views.CadastroView;
import views.panels.*;
import views.RelatarMorteView;
import models.dao.LoginDAO;
import viewmodel.panels.PainelCandidatoVM;
import views.LoginView;
import views.panels.PainelAvaliadorView;

/**
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public class LoginVM extends ViewModel {

    public LoginVM() {
    }

    public void telaRelatarMorte() throws IOException {
        new RelatarMorteView(new RelatarMorteVM()).show();
    }

    public void telaCadastro() throws IOException {
        new CadastroView(new CadastroVM()).show();
    }

    public void autenticar(String usuario, String senha) throws IllegalArgumentException {

    }

    public void autenticar() throws SQLException, NullFieldException, EmptyFieldException, IncorrectUsernameException, IncorrectPasswordException, IOException {
        LoginDAO ld = new LoginDAO();
        LoginView lv = (LoginView) myView;
        ld.autenticar(lv.getUsername(), lv.getPassword(), Session.getInstance());
        List<String> s = Session.getInstance().getPessoa().getPrivilegios();
        System.out.println(s);
        if (s.contains("Funcionário")) {
            String tipo = new FuncionarioDAO().getTipoFuncionario(Session.getInstance().getPessoa());
            System.out.println(tipo);
            if (tipo.equalsIgnoreCase("Avaliador")) {
                new PainelAvaliadorView(new PainelAvaliadorVM()).show();
            } else {
                new PainelCorretorView(new PainelCorretorVM()).show();

            }
        } else if (s.contains("Segurado")) {
            new PainelSeguradoView(new PainelAvaliadorVM()).show();
        } else {
            new PainelCandidatoView(new PainelCandidatoVM()).show();
        }

    }
}
