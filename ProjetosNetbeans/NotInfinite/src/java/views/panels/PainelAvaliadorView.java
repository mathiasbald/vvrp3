/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views.panels;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import viewmodel.ViewModel;
import viewmodel.panels.PainelAvaliadorVM;

import java.io.IOException;

/**
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public class PainelAvaliadorView extends PainelView {

    public PainelAvaliadorView(ViewModel viewModel) {
        super(viewModel);
    }

    @Override
    protected void initializePanelButtons() {
        addButtonToMenuRoot("Avaliar Candidato", FontAwesomeIcon.USER, event -> {
            try {
                ((PainelAvaliadorVM) myViewModel).showFrameAvaliarCandidato();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        addButtonToMenuRoot("Avaliar sinistros", FontAwesomeIcon.ASTERISK, event -> {
            try {
                ((PainelAvaliadorVM) myViewModel).showFrameListarSinistros();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
