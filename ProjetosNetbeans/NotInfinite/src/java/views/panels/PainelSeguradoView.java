/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views.panels;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.scene.layout.VBox;
import viewmodel.panels.PainelSeguradoVM;
import viewmodel.ViewModel;

/**
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public class PainelSeguradoView extends PainelView {

    public PainelSeguradoView(ViewModel viewModel) {
        super(viewModel);
    }

    @Override
    protected void initializePanelButtons() {
        VBox mg1 = createMenuGruop("Contrato", FontAwesomeIcon.FILE_TEXT);
        addButtonToMenuGroup(mg1, "Meu Contrato", FontAwesomeIcon.PLUS_CIRCLE, event -> {
            System.out.println("BTN Contratar");
        });

        addButtonToMenuGroup(mg1, "Alterar", FontAwesomeIcon.PENCIL, event -> {
            System.out.println("BTN ALTERAR");
        });

        addButtonToMenuRoot("Beneficiários", FontAwesomeIcon.USERS, event -> {
            try {
                ((PainelSeguradoVM) myViewModel).frameBeneficiarios();
            } catch (IOException ex) {
                Logger.getLogger(PainelSeguradoView.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        VBox mg2 = createMenuGruop("Sinistro", FontAwesomeIcon.ASTERISK);

        addButtonToMenuGroup(mg2, "Relatar", FontAwesomeIcon.INFO_CIRCLE, event -> {
        });

    }
}
