/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views.panels;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import viewmodel.ViewModel;
import viewmodel.panels.PainelCorretorVM;

import java.io.IOException;

/**
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public class PainelCorretorView extends PainelView {

    public PainelCorretorView(ViewModel viewModel) {
        super(viewModel);
    }

    @Override
    protected void initializePanelButtons() {
        addButtonToMenuRoot("Avaliar Solicitacoes", FontAwesomeIcon.USER, event -> {
            try {
                ((PainelCorretorVM) myViewModel).showFrameAvaliarSolicitacao();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
