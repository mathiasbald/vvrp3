/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import exceptions.InvalidValueException;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import models.TextFieldFormatter;
import viewmodel.RelatarMorteVM;
import viewmodel.ViewModel;

/**
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public class RelatarMorteView extends View {

    private boolean registrado;

    @FXML
    private JFXTextField numeroApoliceIpt;

    @FXML
    private JFXTextField nomeCompletoSeguradoIpt;

    @FXML
    private JFXTextField causaMorteIpt;

    @FXML
    private JFXButton anexarBtn;

    @FXML
    private Label arquivoLbl;

    @FXML
    private JFXTextField nomeCompletoContatoIpt;

    @FXML
    private JFXComboBox<String> parentescoCbx;

    @FXML
    private JFXTextField telefoneIpt;

    @FXML
    private JFXTextField emailIpt;

    private TextFieldFormatter mascaraNumero;
    private TextFieldFormatter mascaraTelefone;

    public RelatarMorteView(ViewModel viewModel) {
        super(viewModel);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            parentescoCbx.getItems().addAll(((RelatarMorteVM) myViewModel).recuperarParentescos());
        } catch (SQLException ex) {
            Logger.getLogger(RelatarMorteView.class.getName()).log(Level.SEVERE, null, ex);
        }

        this.registrado = false;

        mascaraNumero = new TextFieldFormatter();
        mascaraNumero.setMask("##########");
        mascaraNumero.setCaracteresValidos("0123456789");
        mascaraNumero.setTf(numeroApoliceIpt);

        mascaraTelefone = new TextFieldFormatter();
        mascaraTelefone.setMask("(##)#####-####");
        mascaraTelefone.setCaracteresValidos("0123456789");
        mascaraTelefone.setTf(telefoneIpt);
    }

    @FXML
    private void anexarBtnClick() {
        Alerta alerta = new Alerta();
        try {
            alerta.exibirEspera("Esperando arquivo");
            String arquivoSelecionado = ((RelatarMorteVM) myViewModel).anexarCertidao();
            new Alerta().exibir("Sucesso!", "Arquivo importado com sucesso.");
            anexarBtn.setStyle("-fx-background-color: #2f7929;");
            arquivoLbl.setText(arquivoSelecionado);
        } catch (IllegalArgumentException ex) {
            new Alerta().exibir("Ops...", ex.getMessage());
        } finally {
            alerta.fechar();
        }
    }

    @FXML
    private void enviarBtnClick() {
        if (registrado) {
            new Alerta().exibir("Atenção", "Identificamos um envio de formulário recentemente. Tente mais tarde ou aguarde a nossa equipe entrar em contato.");
            return;
        }

        String numeroApolice = this.numeroApoliceIpt.getText();
        String nomeCompletoSegurado = this.nomeCompletoSeguradoIpt.getText();
        String causaMorte = this.causaMorteIpt.getText();
        String nomeCompletoContato = this.nomeCompletoContatoIpt.getText();
        String parentesco = this.parentescoCbx.getValue();
        String telefone = this.telefoneIpt.getText();
        String email = this.emailIpt.getText();

        try {
            ((RelatarMorteVM) myViewModel).registrarMorte(numeroApolice, nomeCompletoSegurado, causaMorte, nomeCompletoContato, parentesco, telefone, email);
            new Alerta().exibir("Sinistro relatado com sucesso", "O formulário foi enviado com sucesso.\nAguarde a nossa entrar em contato.");
            registrado = true;
        } catch (InvalidValueException ex) {
            new Alerta().exibir("Verifique as informações", ex.getMessage());
        } catch (SQLException ex) {
            switch (ex.getErrorCode()) {
                default:
                    System.out.println(ex.getErrorCode());
                    new Alerta().exibir("Ops...", ex.getMessage());
                    break;
            }
        }
    }

    @FXML
    private void voltarBtnClick() {
        try {
            ((RelatarMorteVM) myViewModel).telaLogin();
        } catch (IOException ex) {
            Logger.getLogger(RelatarMorteView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void mascaraNumero() {
        mascaraNumero.formatter();
    }

    @FXML
    private void mascaraTelefone() {
        mascaraTelefone.formatter();
    }
}
