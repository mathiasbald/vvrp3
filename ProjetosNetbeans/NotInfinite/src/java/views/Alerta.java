/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialog.DialogTransition;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.JFXSpinner;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public class Alerta {

    private final JFXDialog alerta;
    private final List<Node> botoes = new ArrayList<>();


    public Alerta() {
        this.alerta = new JFXDialog();
        this.alerta.setOverlayClose(false);
    }

    public void exibir(String titulo, DialogTransition transicao) {
        alerta.setTransitionType(transicao);
        JFXDialogLayout layout = new JFXDialogLayout();
        layout.setHeading(new Label(titulo));
        layout.setBody(new JFXSpinner());

        alerta.setOnKeyPressed(teclado -> {
            if (teclado.getCode() == KeyCode.ESCAPE) {
                this.fechar();
            }
        });

        alerta.setContent(layout);
        alerta.show(View.rootView);
    }

    public void exibir(String titulo, String mensagem) {
        alerta.setTransitionType(DialogTransition.CENTER);
        alerta.setContent(this.gerarLayout(titulo, mensagem));
        alerta.show(View.rootView);
    }

    public void exibirComNode(String titulo, Node node) {
        JFXDialogLayout layout = new JFXDialogLayout();
        layout.setHeading(new Label(titulo));

        alerta.setTransitionType(DialogTransition.CENTER);

        alerta.setOnKeyPressed(teclado -> {
            if (teclado.getCode() == KeyCode.ESCAPE) {
                this.fechar();
            }
        });
        addBtn("Cancelar", "#7088ff", "black", acao -> {
            fechar();
        });
        layout.setBody(node);
        layout.setActions(botoes);
        alerta.setContent(layout);
        alerta.show(View.rootView);
    }


    public void exibir(String titulo, String mensagem, DialogTransition transicao) {
        alerta.setTransitionType(transicao);
        alerta.setContent(this.gerarLayout(titulo, mensagem));
        alerta.show(View.rootView);
    }

    public void exibirEspera(String titulo) {
        alerta.setTransitionType(DialogTransition.CENTER);

        JFXDialogLayout layout = new JFXDialogLayout();
        layout.setHeading(new Label(titulo));
        layout.setBody(new JFXSpinner());
        alerta.setContent(layout);
        alerta.show(View.rootView);
    }

    private JFXDialogLayout gerarLayout(String titulo, String mensagem) {
        JFXDialogLayout layout = new JFXDialogLayout();
        layout.setHeading(new Label(titulo));
        layout.setBody(new Label(mensagem));

        alerta.setOnKeyPressed(teclado -> {
            if (teclado.getCode() == KeyCode.ESCAPE) {
                this.fechar();
            }
        });
        if (botoes.isEmpty()) {
            addBtn("OK", acao -> {
                fechar();
            });
        }
            layout.setActions(botoes);
        return layout;
    }

    public void addBtn(String texto, String buttonColor, String textButtonColor, EventHandler<ActionEvent> acao) {
        JFXButton btn = new JFXButton(texto);
        btn.setStyle("-fx-font-size: 14.0; -fx-text-fill: " + textButtonColor + "; -fx-background-color: " + buttonColor + ";");
        this.configBtn(btn, acao);
    }

    public void addNoClosableBtn(String texto, String buttonColor, String textButtonColor, EventHandler<ActionEvent> acao) {
        JFXButton btn = new JFXButton(texto);
        btn.setStyle("-fx-font-size: 14.0; -fx-text-fill: " + textButtonColor + "; -fx-background-color: " + buttonColor + ";");
        btn.setOnAction(acao);
        botoes.add(btn);
    }

    public void addBtn(String texto, EventHandler<ActionEvent> acao) {
        JFXButton btn = new JFXButton(texto);
        btn.setStyle("-fx-font-size: 14.0; -fx-text-fill: black;");
        this.configBtn(btn, acao);
    }

    private void configBtn(JFXButton btn, EventHandler<ActionEvent> acao) {
        btn.setOnMouseClicked(mouse -> this.fechar());
        btn.setOnAction(acao);
        botoes.add(btn);
    }

    public void limparBtns() {
        this.botoes.clear();
    }

    public void fechar() {
        alerta.close();
        botoes.clear();
    }

}
