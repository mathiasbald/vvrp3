/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views.frames;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTimePicker;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableRow;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import models.Predisposicoes;
import models.ProblemaSaude;
import viewmodel.ViewModel;
import viewmodel.frames.FrameAvaliarSolicitacaoVM;
import views.Alerta;
import views.panels.PainelView;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ResourceBundle;

/**
 * FXML Controller class
 *
 * @author marti
 */
public class FrameAvaliarSolicitacaoVisualizarView extends Frame {

    @FXML
    private javafx.scene.control.TableView<ProblemaSaude> problemaSaudeTableView;
    @FXML
    private javafx.scene.control.TableView<Predisposicoes> predisposicoesTableView;
    @FXML
    private Label nomeCandidatoLbl, enderecoLbl, idSolicitacaoLbl, valorPremioLbl,
            telefoneLbl, isFumanteLbl, isAlcoolistaLbl, valorDesejadoLbl;
    @FXML
    private JFXButton aprovarBtn, beneficiariosBtn, detalhesBtn;

    private final ObservableList<ProblemaSaude> prob = FXCollections.observableArrayList();
    private final ObservableList<Predisposicoes> pred = FXCollections.observableArrayList();

    public FrameAvaliarSolicitacaoVisualizarView(FrameAvaliarSolicitacaoVM viewModel, PainelView painelView) {
        super(viewModel, painelView);
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {


        problemaSaudeTableView.getColumns().clear();
        problemaSaudeTableView.setEditable(true);
        javafx.scene.control.TableColumn nome = new javafx.scene.control.TableColumn("Nome");
        nome.setCellValueFactory(new PropertyValueFactory<ProblemaSaude, String>("nomeProblema"));
        javafx.scene.control.TableColumn descricao = new javafx.scene.control.TableColumn("Descricao");
        descricao.setCellValueFactory(new PropertyValueFactory<ProblemaSaude, String>("descricaoProblema"));
        problemaSaudeTableView.setItems(prob);
        problemaSaudeTableView.getColumns().addAll(nome, descricao);

        problemaSaudeTableView.setRowFactory( tv -> {
            TableRow<ProblemaSaude> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                    new Alerta().exibir("Infromações do problema",row.getItem().toString());
                }
            });
            return row ;
        });


        predisposicoesTableView.getColumns().clear();
        predisposicoesTableView.setEditable(true);
        javafx.scene.control.TableColumn nomep = new javafx.scene.control.TableColumn("Nome");
        nomep.setCellValueFactory(new PropertyValueFactory<Predisposicoes, String>("nome"));
        predisposicoesTableView.setItems(pred);
        predisposicoesTableView.getColumns().addAll(nomep);


        ((FrameAvaliarSolicitacaoVM)myViewModel).requestFieldsValueSetOnViewScreen();
    }


    public void addProblema(ProblemaSaude p) {
        prob.add(p);
    }

    public void addPredisposicao(Predisposicoes p) {
        pred.add(p);
    }


    public void setAvaliarBtnDisabled(String message){
        aprovarBtn.setOnAction(event -> new Alerta().exibir("Você não pode aprovar esse candidato", message));
    }

    @FXML
    void cancelarBtnClick(ActionEvent event) throws IOException {
        ((FrameAvaliarSolicitacaoVM) myViewModel).voltarParaLista();
    }

    @FXML
    void verBeneficiariosBtnClick(ActionEvent event) throws IOException {
        System.out.println("verBeneficiariosBtnClick");

    }

    @FXML
    void detalhesBtnClick(ActionEvent event) throws IOException {
        System.out.println("detalhesBtnClick");

    }

    @FXML
    void avaliarBtnClick(ActionEvent event) throws IOException, SQLException {
        JFXDatePicker datePicker = new JFXDatePicker();
        datePicker.setEditable(false);
        JFXTimePicker timePicker = new JFXTimePicker();
        timePicker.setEditable(false);
        VBox v = new VBox(datePicker, timePicker);
        Alerta alerta = new Alerta();
        alerta.addBtn("Marcar Consulta", "#7088ff","black",event1 -> {

            LocalDate date = datePicker.getValue();
            int dia, mes, ano, hora, minuto;
            try {
                dia = date.getDayOfMonth();
                mes = date.getMonthValue()-1;
                ano = date.getYear();
            } catch (Exception exception) {

                Alerta alerta2 = new Alerta();
                alerta2.addBtn("OK", event2 -> {
                    try {
                        avaliarBtnClick(event);
                    } catch (IOException | SQLException e) {
                        e.printStackTrace();
                    }
                });
                alerta2.exibir("Erro ao agendar", "Coloque uma data valida");
                return;

            }
            LocalTime time = timePicker.getValue();
            try {
                hora = time.getHour();
                minuto = time.getMinute();
            } catch (Exception exception) {
                Alerta alerta2 = new Alerta();
                alerta2.addBtn("OK", event2 -> {
                    try {
                        avaliarBtnClick(event);
                    } catch (IOException | SQLException e) {
                        e.printStackTrace();
                    }
                });
                alerta2.exibir("Erro ao agendar", "Coloque um horario valido");
                return;
            }
            try {
                ((FrameAvaliarSolicitacaoVM) myViewModel).marcarConsulta(dia, mes, ano, hora, minuto);
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            new Alerta().exibir("Consulta marcada com sucesso","");
        });
        alerta.exibirComNode("Selecione uma data e uma hora pra consulta", v);

    }

    @FXML
    void reprovarBtnClick(ActionEvent event) throws IOException {
//        Alerta.getInstance().exibir("NotInfinite", "Proposta aprovada com sucesso", true);
        JFXTextArea textArea = new JFXTextArea();
        textArea.setStyle("-fx-background-color: white; -fx-border-width: 1px; -fx-border-color: gray; -fx-padding: 2px; -fx-border-radius: 5px");
        Alerta alerta = new Alerta();
        alerta.addBtn("Reprovar", "#7088ff","black",event1 -> {
            try {

                ((FrameAvaliarSolicitacaoVM)myViewModel).reprovarSolicitacao(textArea.getText());
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            alerta.fechar();
            new Alerta().exibir("Candidato reprovado com sucesso","");
        });
       alerta.exibirComNode("Digite o motivo da reprovação", textArea);
    }


    public void setNomeCandidato(String s) {
        nomeCandidatoLbl.setText(s);
    }

    public void setEnderecoCandidato(String s) {
        enderecoLbl.setText(s);
    }

    public void setNumeroSolicitacao(String s) {
        idSolicitacaoLbl.setText(s);
    }

    public void setValorPremio(String s) {
        valorPremioLbl.setText(s);
    }


    public void setTelefoneCandidato(String s) {
        telefoneLbl.setText(s);
    }

    public void setIsFumanteCandidato(String s) {
        isFumanteLbl.setText(s);
    }

    public void setIsAlcoolistaCandidato(String s) {
        isAlcoolistaLbl.setText(s);
    }

    public void setValorDesejado(String s) {
        valorDesejadoLbl.setText(s);
    }


    public void setBeneficiariosBtnPopupMessage(String t, String s) {
        beneficiariosBtn.setOnAction(event -> new Alerta().exibir(t,s));
    }

    public void setDetalhesBtn(String t, String s) {
        this.detalhesBtn.setOnAction(event -> new Alerta().exibir(t,s));
    }


}


