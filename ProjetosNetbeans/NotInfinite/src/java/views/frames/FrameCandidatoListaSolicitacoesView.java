/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views.frames;

import com.jfoenix.controls.JFXCheckBox;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import models.SolicitacaoSeguro;
import viewmodel.frames.FrameAvaliarSolicitacaoVM;
import viewmodel.frames.FrameCandidatoListaVM;
import views.panels.PainelView;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

/**
 * FXML Controller class
 *
 * @author marti
 */
public class FrameCandidatoListaSolicitacoesView extends Frame {

    //////////////////
    @FXML
    VBox problemsContainer;
    @FXML
    TableView<SolicitacaoSeguro> tv;

    private Set<JFXCheckBox> cbSet = new HashSet<JFXCheckBox>();

    public FrameCandidatoListaSolicitacoesView(FrameCandidatoListaVM viewModel, PainelView painelView) {
        super(viewModel, painelView);
    }


    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        (tv.getColumns().get(0)).setCellValueFactory(new PropertyValueFactory<>("numeroSolicitacao"));
        (tv.getColumns().get(1)).setCellValueFactory(new PropertyValueFactory<>("dataSolicitacao"));
        (tv.getColumns().get(2)).setCellValueFactory(new PropertyValueFactory<>("valorPremioString"));
        (tv.getColumns().get(3)).setCellValueFactory(new PropertyValueFactory<>("valorSolicitacaoString"));

        try {
            ((FrameCandidatoListaVM) myViewModel).requestForSolicitacoes();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void setListaSolicitacoes(List<SolicitacaoSeguro> ls){
        tv.getItems().addAll(ls);
    }
    @FXML
    void acaoVoltar(ActionEvent event) throws IOException {
    }

    @FXML
    void verHistoricoBtnClick(ActionEvent event) throws IOException {
        ((FrameCandidatoListaVM) myViewModel).verHistoricoSolicitacaoSelecionada();
    }

    public SolicitacaoSeguro getSolicitacaoSelecionada() {
        return tv.getSelectionModel().getSelectedItem();
    }
}
