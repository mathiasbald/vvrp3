/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views.frames;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableRow;
import javafx.scene.control.cell.PropertyValueFactory;
import models.Predisposicoes;
import models.ProblemaSaude;
import viewmodel.ViewModel;
import viewmodel.frames.FrameAvaliarCandidatoVM;
import views.Alerta;
import views.panels.PainelView;
//import models.mocks.MockCandidato;
//import models.mocks.MockSolicitacaoSeguro1;
//import models.mocks.MockSolicitacaoSeguro2;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.*;

/**
 * FXML Controller class
 *
 * @author marti
 */
public class FrameAvaliarCandidatoVisualizarView extends Frame {

    @FXML
    private javafx.scene.control.TableView<ProblemaSaude> problemaSaudeTableView;
    @FXML
    private javafx.scene.control.TableView<Predisposicoes> predisposicoesTableView;
    @FXML
    private Label nomeCandidatoLbl, enderecoLbl, idSolicitacaoLbl, valorPremioLbl,
            telefoneLbl, isFumanteLbl, isAlcoolistaLbl, valorDesejadoLbl;
    @FXML
    private JFXButton aprovarBtn, detalhesBtn;



    private final ObservableList<ProblemaSaude> prob = FXCollections.observableArrayList();
    private final ObservableList<Predisposicoes> pred = FXCollections.observableArrayList();

    public FrameAvaliarCandidatoVisualizarView(FrameAvaliarCandidatoVM viewModel, PainelView painelView) {
        super(viewModel, painelView);
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {


        problemaSaudeTableView.getColumns().clear();
        problemaSaudeTableView.setEditable(true);
        javafx.scene.control.TableColumn nome = new javafx.scene.control.TableColumn("Nome");
        nome.setCellValueFactory(new PropertyValueFactory<ProblemaSaude, String>("nomeProblema"));
        javafx.scene.control.TableColumn descricao = new javafx.scene.control.TableColumn("Descricao");
        descricao.setCellValueFactory(new PropertyValueFactory<ProblemaSaude, String>("descricaoProblema"));
        problemaSaudeTableView.setItems(prob);
        problemaSaudeTableView.getColumns().addAll(nome, descricao);


        problemaSaudeTableView.setRowFactory(tv -> {
            TableRow<ProblemaSaude> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!row.isEmpty())) {
                    new Alerta().exibir("Infromações do problema", row.getItem().toString());
                }
            });
            return row;
        });


        predisposicoesTableView.getColumns().clear();
        predisposicoesTableView.setEditable(true);
        javafx.scene.control.TableColumn nomep = new javafx.scene.control.TableColumn("Nome");
        nomep.setCellValueFactory(new PropertyValueFactory<Predisposicoes, String>("nome"));
        predisposicoesTableView.setItems(pred);
        predisposicoesTableView.getColumns().addAll(nomep);


        ((FrameAvaliarCandidatoVM) myViewModel).requestFieldsValueSetOnViewScreen();
    }


    public void addProblema(ProblemaSaude p) {
        prob.add(p);
    }

    public void addPredisposicao(Predisposicoes p) {
        pred.add(p);
    }


    public void setAvaliarBtnDisabled(String message) {
        aprovarBtn.setOnAction(event -> {
            new Alerta().exibir("Você não pode aprovar esse candidato", message);
        });
    }

    @FXML
    void cancelarBtnClick(ActionEvent event) throws IOException {
        ((FrameAvaliarCandidatoVM) myViewModel).voltarParaLista();
    }

    @FXML
    void detalhesBtnClick(ActionEvent event) throws IOException {
        System.out.println("detalhesBtnClick");

    }

    @FXML
    void editarBtnClick(ActionEvent event) throws IOException {
        ((FrameAvaliarCandidatoVM) myViewModel).editarSolicitacao();


    }

    @FXML
    void avaliarBtnClick(ActionEvent event) throws IOException, SQLException {
        Alerta alerta = new Alerta();
        alerta.addBtn("Não", "#7088ff", "black", event1 -> alerta.fechar());
        alerta.addBtn("Sim", "#7088ff", "black", event1 -> {
            try {
                ((FrameAvaliarCandidatoVM) myViewModel).aprovarSolicitacao();
            } catch (SQLException | IOException e) {
                e.printStackTrace();
            }
            alerta.fechar();
            new Alerta().exibir("Candidato aprovado com sucesso", "");
        });
        alerta.exibir("Aprovar Candidato", "Tem certeza que deseja aprovar o candidato?");

    }

    @FXML
    void reprovarBtnClick(ActionEvent event) throws IOException {
//        Alerta.getInstance().exibir("NotInfinite", "Proposta aprovada com sucesso", true);
        JFXTextArea textArea = new JFXTextArea();
        textArea.setStyle("-fx-background-color: white; -fx-border-width: 1px; -fx-border-color: gray; -fx-padding: 2px; -fx-border-radius: 5px");

        Alerta ac = new Alerta();
        ac.addNoClosableBtn("Reprovar", "#7088ff", "black", event1 -> {
            if (textArea.getText().isEmpty())
                textArea.setStyle("-fx-background-color: white; -fx-border-width: 1px; -fx-border-color: red; -fx-padding: 2px; -fx-border-radius: 5px");
            else {
                ac.fechar();
                try {
                    ((FrameAvaliarCandidatoVM) myViewModel).reprovarSolicitacao(textArea.getText());
                } catch (SQLException | IOException e) {
                    e.printStackTrace();
                }
                new Alerta().exibir("Candidato reprovado com sucesso", "");
            }
        });
        ac.exibirComNode("Digite o motivo da reprovação", textArea);
    }


    public void setNomeCandidato(String s) {
        nomeCandidatoLbl.setText(s);
    }

    public void setEnderecoCandidato(String s) {
        enderecoLbl.setText(s);
    }

    public void setNumeroSolicitacao(String s) {
        idSolicitacaoLbl.setText(s);
    }

    public void setValorPremio(String s) {
        valorPremioLbl.setText(s);
    }


    public void setTelefoneCandidato(String s) {
        telefoneLbl.setText(s);
    }

    public void setIsFumanteCandidato(String s) {
        isFumanteLbl.setText(s);
    }

    public void setIsAlcoolistaCandidato(String s) {
        isAlcoolistaLbl.setText(s);
    }

    public void setValorDesejado(String s) {
        valorDesejadoLbl.setText(s);
    }


    public void setDetalhesBtn(String t, String s) {
        this.detalhesBtn.setOnAction(event -> new Alerta().exibir(t,s));
    }

}
