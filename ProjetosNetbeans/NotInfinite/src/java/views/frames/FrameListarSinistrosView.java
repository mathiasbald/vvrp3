/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views.frames;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.transformation.FilteredList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import models.Sinistro;
import viewmodel.ViewModel;
import viewmodel.frames.FrameListarSinistrosVM;
import views.Alerta;
import views.panels.PainelView;

/**
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public class FrameListarSinistrosView extends Frame {

    @FXML
    private TableView<Sinistro> tabelaSinistro;

    @FXML
    private TableColumn<Sinistro, String> colunaDataSolicitacao;

    @FXML
    private TableColumn<Sinistro, String> colunaApolice;

    @FXML
    private TableColumn<Sinistro, String> colunaTipoSinistro;
    
    @FXML
    private TableColumn<Sinistro, String> colunaNomeSegurado;

    private FilteredList<Sinistro> sinistros;

    private boolean mostrarTodos;

    public FrameListarSinistrosView(ViewModel viewModel, PainelView painelView) {
        super(viewModel, painelView);
        this.mostrarTodos = false;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        colunaDataSolicitacao.setCellValueFactory(data -> {
            return new SimpleStringProperty(
                    new SimpleDateFormat("dd/MM/yyyy").format(data.getValue().getData())
            );
        });

        colunaApolice.setCellValueFactory(valor -> {
            return new SimpleStringProperty(
                    String.valueOf(valor.getValue().getApolice().getNumeroApolice())
            );
        });
        
        colunaNomeSegurado.setCellValueFactory(nome -> {
            return new SimpleStringProperty(nome.getValue().getApolice().getNomeSegurado());
        });

        colunaTipoSinistro.setCellValueFactory(new PropertyValueFactory<>("tipoSinistro"));

        try {
            sinistros = new FilteredList(((FrameListarSinistrosVM) myViewModel).recuperarSinistros());
            tabelaSinistro.setItems(sinistros);
            this.mostrarBtnClick();
        } catch (SQLException ex) {
            Logger.getLogger(FrameListarSinistrosView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void avaliarBtnClick() {
        try {
            ((FrameListarSinistrosVM) myViewModel).avaliar(this.getSinistroSelecionado());
        } catch (IllegalArgumentException ex) {
           new Alerta().exibir("Ops...", ex.getMessage());
        } catch (IOException ex) {
            Logger.getLogger(FrameListarSinistrosView.class.getName()).log(Level.SEVERE, null, ex);
            new Alerta().exibir("Ops...", "Ocorreu um erro inesperado.");
        }
        this.finalize();
    }

    @FXML
    private void mostrarBtnClick() {
        if (mostrarTodos) {
            this.sinistros.setPredicate(sinistros -> sinistros.getIdAvaliador() > 0);
        } else {
            this.sinistros.setPredicate(sinistros -> sinistros.getIdAvaliador() == 0);
        }
        
        colunaDataSolicitacao.setSortable(true);
        
        mostrarTodos = !mostrarTodos;
    }

    @FXML
    private void avaliarBtnDblClick(MouseEvent event) {
        if (event.getClickCount() == 2) {
            this.avaliarBtnClick();
        }
    }

    private Sinistro getSinistroSelecionado() {
        return tabelaSinistro.getSelectionModel().getSelectedItem();
    }

    @Override
    protected void finalize() {
        System.out.println("Desconstrutor -> FrameListarSinistrosView");
        this.sinistros.removeAll();
        try {
            super.finalize();
        } catch (Throwable ex) {
            Logger.getLogger(FrameListarSinistrosView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
