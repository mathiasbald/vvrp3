/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views.frames;

import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextFormatter;
import javafx.scene.layout.*;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import viewmodel.frames.FrameCandidatoListaVM;
import views.Alerta;
import views.panels.PainelView;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.function.UnaryOperator;

//import models.mocks.MockCandidato;
//import models.mocks.MockSolicitacaoSeguro1;
//import models.mocks.MockSolicitacaoSeguro2;

/**
 * FXML Controller class
 *
 * @author marti
 */
public class FrameCandidatoEditarSolicitacaoView extends Frame {

    @FXML
    private Label idSolicitacaoLbl, valorSimuladoLbl;

    @FXML
    private VBox containerProblemasVBOX;

    @FXML
    private GridPane checkBoxesContainer;

    @FXML
    private JFXTextField valorDesejadtoTxt;

    @FXML
    private JFXCheckBox predCancerCbx, predDiabeteCbx, predCardioCbx, predPulmoCbx, predHiperCbx, predDemenciaCbx, predDegeneCbx, predOsteoCbx, alcoolistaCbx, fumanteCbx;
    private Set<JFXCheckBox> cbSet = new HashSet<JFXCheckBox>();


    public FrameCandidatoEditarSolicitacaoView(FrameCandidatoListaVM viewModel, PainelView painelView) {
        super(viewModel, painelView);
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {


        UnaryOperator<TextFormatter.Change> filter = (TextFormatter.Change t) -> {
            if (t.isReplaced()) {
                if (t.getText().matches("[^0-9]")) {
                    t.setText(t.getControlText().substring(t.getRangeStart(), t.getRangeEnd()));
                }
            }
            if (t.isAdded()) {
                if (t.getText().matches("[,]") && !(t.getControlText().contains("."))) {
                    if (t.getControlText().isEmpty()) {
                        t.setText("0.");
                    } else {
                        t.setText(".");
                    }
                } else if (t.getControlText().contains(".")) {
                    if (t.getText().matches("[^0-9]")) {
                        t.setText("");
                    }
                } else if (t.getText().matches("[^0-9.]")) {
                    t.setText("");
                }
            }
            return t;
        };
        valorDesejadtoTxt.setTextFormatter(new TextFormatter<>(filter));

        ((FrameCandidatoListaVM) myViewModel).requestFieldsValueSetOnEditScreen();

        EventHandler eh = (EventHandler<ActionEvent>) (ActionEvent event) -> {
            if (event.getSource() instanceof JFXCheckBox) {
                JFXCheckBox cb = (JFXCheckBox) event.getSource();
                modifyList(cb);
                ((FrameCandidatoListaVM) myViewModel).pedirAlteracaoValorPremio();
            }
        };
        checkBoxesContainer.getChildren()
                .forEach((t) -> {
                    if (t instanceof JFXCheckBox) {
                        JFXCheckBox cb = (JFXCheckBox) t;
                        cb.setOnAction(eh);
                        if (cb.isSelected())
                            modifyList(cb);
                    }
                });

        EventHandler afeh = (EventHandler<ActionEvent>) (ActionEvent event) -> {
            if (event.getSource() instanceof JFXCheckBox) {
                JFXCheckBox cb = (JFXCheckBox) event.getSource();
                ((FrameCandidatoListaVM) myViewModel).pedirAlteracaoValorPremio();
            }
        };
        fumanteCbx.setOnAction(afeh);
        alcoolistaCbx.setOnAction(afeh);

        valorDesejadtoTxt.textProperty().addListener(observable ->
                {
                    ((FrameCandidatoListaVM) myViewModel).pedirAlteracaoValorPremio();
                }
        );

        ((FrameCandidatoListaVM) myViewModel).pedirAlteracaoValorPremio();

    }

    public int getTotalPredisposicoesSelecionadas() {
        return cbSet.size();
    }


    @FXML
    void cancelarBtnClick(ActionEvent event) throws IOException {
        ((FrameCandidatoListaVM) myViewModel).voltarParaInformacoes();
    }

    @FXML
    void salvarBtnClick(ActionEvent event) throws IOException, SQLException {
        if (((FrameCandidatoListaVM) myViewModel).salvarAlteracoes())
            new Alerta().exibir("NotInfinite", "Proposta editada com sucesso");
    }

    @FXML
    void addCampoProblemaBtnClick(Event event) {
        containerProblemasVBOX.getChildren().add(new MyVBox(containerProblemasVBOX));
    }


    public void adicionarProblema(String nome, String problema) {
        MyVBox v = new MyVBox(containerProblemasVBOX);
        v.setNomeProblema(nome);
        v.setDescProblema(problema);
        containerProblemasVBOX.getChildren().add(v);

    }


    public HashMap<String, String> getProblemas() {
        HashMap<String, String> hm = new HashMap<>();
        for (Node n : containerProblemasVBOX.getChildren()) {
            if (n instanceof MyVBox) {
                hm.put(((MyVBox) n).getNomeProblema(), ((MyVBox) n).getDescProblema());
            }

        }
        return hm;

    }


    public void setNumeroSolicitacao(String s) {
        this.idSolicitacaoLbl.setText(s);
    }

    public void setValorSimulado(float f) {
        this.valorSimuladoLbl.setText(String.format("R$%#.2f", f));
    }

    public void setValorDesejado(float f) {
        valorDesejadtoTxt.setText(String.valueOf(f));

    }

    public float getValorDesejado() {
        try {
            return Float.parseFloat(valorDesejadtoTxt.getText());
        } catch (Exception e) {
            return 0;
        }
    }

    private void modifyList(JFXCheckBox cb) {
        if (cb.isSelected()) {
            cbSet.add(cb);
        } else {
            cbSet.remove(cb);
        }
    }

    public void showAlert(String t, String s) {
        new Alerta().exibir(t, s);
    }

    public void checkPredCancer(boolean b) {
        predCancerCbx.setSelected(b);
    }

    public void checkPredDiabete(boolean b) {
        predDiabeteCbx.setSelected(b);
    }

    public void checkPredCardio(boolean b) {
        predCardioCbx.setSelected(b);
    }

    public void checkPredPulmo(boolean b) {
        predPulmoCbx.setSelected(b);
    }

    public void checkPredHiper(boolean b) {
        predHiperCbx.setSelected(b);
    }

    public void checkPredDemencia(boolean b) {
        predDemenciaCbx.setSelected(b);
    }

    public void checkPredDegeneracao(boolean b) {
        predDegeneCbx.setSelected(b);
    }

    public void checkPredOsteo(boolean b) {
        predOsteoCbx.setSelected(b);
    }

    public void checkAlcoolista(boolean b) {
        alcoolistaCbx.setSelected(b);
    }

    public void checkFumante(boolean b) {
        fumanteCbx.setSelected(b);
    }


    public boolean isSelectedPredCancer() {
        return predCancerCbx.isSelected();
    }

    public boolean isSelectedPredDiabete() {
        return predDiabeteCbx.isSelected();
    }

    public boolean isSelectedPredCardio() {
        return predCardioCbx.isSelected();
    }

    public boolean isSelectedPredPulmo() {
        return predPulmoCbx.isSelected();
    }

    public boolean isSelectedPredHiper() {
        return predHiperCbx.isSelected();
    }

    public boolean isSelectedPredDemencia() {
        return predDemenciaCbx.isSelected();
    }

    public boolean isSelectedPredDegeneracao() {
        return predDegeneCbx.isSelected();
    }

    public boolean isSelectedPredOsteo() {
        return predOsteoCbx.isSelected();
    }

    public boolean isSelectedAlcoolista() {
        return alcoolistaCbx.isSelected();
    }

    public boolean isSelectedFumante() {
        return fumanteCbx.isSelected();
    }


    private class MyVBox extends VBox {
        private JFXTextField nomeProblema;
        private JFXTextArea descProblema;


        MyVBox(Pane parent) {
            super();
            HBox containerTopo = new HBox();
            nomeProblema = new JFXTextField();
            StackPane botaoAddSP = new StackPane();
            Circle c = new Circle();
            Label l = new Label();
            descProblema = new JFXTextArea();

            //mudança atributos
            nomeProblema.prefWidth(375.0);
            nomeProblema.maxWidth(getWidth());
            HBox.setHgrow(nomeProblema, Priority.ALWAYS);

            nomeProblema.setStyle("    -fx-padding: 5px; -fx-border-insets: 5px; -fx-background-insets: 5px;");
            nomeProblema.setPromptText("Digite aqui o nome do problema");
            HBox.setHgrow(botaoAddSP, Priority.NEVER);
            c.setFill(Paint.valueOf("#999999"));
            c.setStrokeWidth(0);
            c.setRadius(10.0);
            l.setFont(Font.font(23));
            l.setText("-");
            Pane p = new Pane();
            p.setMinWidth(50);
            p.setMaxWidth(50);
            p.prefWidth(50);
            botaoAddSP.prefHeight(15.0);
            botaoAddSP.prefWidth(35.0);
            botaoAddSP.setStyle("    -fx-padding: 5px; -fx-border-insets: 5px; -fx-background-insets: 5px;");
            botaoAddSP.getChildren().addAll(c, l);
            containerTopo.getChildren().addAll(nomeProblema, p, botaoAddSP);

            descProblema.setStyle("    -fx-padding: 5px; -fx-border-insets: 5px; -fx-background-insets: 5px;");
            descProblema.setMaxHeight(65);
            descProblema.setPromptText("Digite aqui a descrição do problema");
            this.setStyle("    -fx-padding: 5 0 5 0; -fx-border-insets: 5 0 5 0; -fx-background-insets: 5 0 5 0; -fx-border-color: #999; -fx-border-width: 0 0 1px 0; -fx-border-style: dashed");
            this.getChildren().addAll(containerTopo, descProblema);
            this.getStyleClass().addAll("box-simple-border");
            botaoAddSP.setOnMouseClicked(mouseEvent -> {
                parent.getChildren().remove(this);
            });
        }

        String getNomeProblema() {
            return nomeProblema.getText();
        }

        String getDescProblema() {
            return descProblema.getText();
        }

        void setNomeProblema(String s) {
            nomeProblema.setText(s);
        }

        void setDescProblema(String s) {
            descProblema.setText(s);
        }


    }

}
