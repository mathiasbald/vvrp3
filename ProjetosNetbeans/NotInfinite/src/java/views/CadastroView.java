/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import java.io.IOException;
import java.net.URL;
import java.time.ZoneId;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import models.Crypt;
import models.Pessoa;
import models.TextFieldFormatter;
import models.Usuario;
import viewmodel.CadastroVM;
import viewmodel.ViewModel;

/**
 *
 * @author martins
 */
public class CadastroView extends View {

    @FXML
    private TextField usuario, email, CPF, nomeCompleto, endereco, bairro, cidade, CEP, nroTelCEL;

    @FXML
    private PasswordField senha, confirmarSenha;

    @FXML
    private JFXButton btnRegistrar;

    @FXML
    private JFXComboBox<String> sexo, estadoCivil;

    @FXML
    private JFXDatePicker nascimento;

    ObservableList<String> sexoList = FXCollections.observableArrayList("M", "F");

    public CadastroView(ViewModel viewModel) {
        super(viewModel);
//            String CRYPTER = Base64.getEncoder().encodeToString("Michael".getBytes());

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        sexo.setItems(sexoList);
        sexo.setValue(sexoList.get(0));
    }

    @FXML
    void registrar(ActionEvent event) throws IOException {
        if (validarCampos()) {

            String crypt = Crypt.sha512(senha.getText(), CPF.getText());

            Usuario user = new Usuario(usuario.getText(), email.getText(), crypt);

            Date date = Date.from(nascimento.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant());

            Pessoa pessoa = new Pessoa(
                    nomeCompleto.getText(),
                    CPF.getText(),
                    email.getText(),
                    nroTelCEL.getText(),
                    date,
                    sexo.getValue().charAt(0),
                    CEP.getText(),
                    cidade.getText(),
                    endereco.getText(),
                    bairro.getText());

            Alerta alerta = new Alerta();
            ((CadastroVM) myViewModel).cadastrarUsuario(user, pessoa);

            alerta.addBtn("OK", acao -> {
                try {
                    ((CadastroVM) myViewModel).telaLogin();
                } catch (IOException ex) {
                    Logger.getLogger(CadastroView.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
            alerta.exibir("SUCESSO", "Cadastro realizado com sucesso. Realize o Login");
        }
    }

    @FXML
    void voltar(ActionEvent event) throws IOException {
        try {
            ((CadastroVM) myViewModel).telaLogin();
        } catch (Exception e) {
            System.out.println("Algo de errado aconteceu: " + e.getMessage());
        }
    }

    @FXML
    void maskCEP(KeyEvent event) {
        TextFieldFormatter mask = new TextFieldFormatter();
        mask.setMask("#####-###");
        mask.setCaracteresValidos("0123456789");
        mask.setTf(CEP);
        mask.formatter();
    }

    @FXML
    void maskCPF(KeyEvent event) {
        TextFieldFormatter mask = new TextFieldFormatter();
        mask.setMask("###.###.###-##");
        mask.setCaracteresValidos("0123456789");
        mask.setTf(CPF);
        mask.formatter();
    }

    @FXML
    void maskTELCELL(KeyEvent event) {
        TextFieldFormatter mask = new TextFieldFormatter();
        mask.setMask("(##)#####-####");
        mask.setCaracteresValidos("0123456789");
        mask.setTf(nroTelCEL);
        mask.formatter();
    }

    private boolean validarCampos() {
        if (nomeCompleto.getText().trim().equals("")) {
            alert("Atenção", "Informe seu nome completo.");
            return false;
        }

        if (TemDigitosExcessivos(nomeCompleto.getText())) {
            alert("Atenção", "Nome completo não pode conter mais de 80 caracteres.");
            return false;
        }

        if (nroTelCEL.getText().trim().equals("")) {
            alert("Atenção", "Informe um número de telefone válido.");
            return false;
        }

        if (CEP.getText().trim().equals("")) {
            alert("Atenção", "Informe um CEP válido.");
            return false;
        }

        if (endereco.getText().trim().equals("")) {
            alert("Atenção", "Informe seu endereço.");
            return false;
        }

        if (TemDigitosExcessivos(endereco.getText())) {
            alert("Atenção", "Endereço não pode conter mais de 80 caracteres.");
            return false;
        }

        if (usuario.getText().trim().equals("")) {
            alert("Atenção", "Informe um nome de usuário.");
            return false;
        }

        if (TemDigitosExcessivos(usuario.getText())) {
            alert("Atenção", "Usuario não pode conter mais de 80 caracteres.");
            return false;
        }
        if (email.getText().trim().equals("")) {
            alert("Atenção", "Informe um e-mail de usuário.");
            return false;
        }

        if (!email.getText().trim().contains("@")) {
            alert("Atenção", "Informe um e-mail válido.");
            return false;
        }

        if (TemDigitosExcessivos(email.getText())) {
            alert("Atenção", "E-mail não pode conter mais de 80 caracteres.");
            return false;
        }

        if (senha.getText().trim().equals("")) {
            alert("Atenção", "Informe sua senha.");
            return false;
        }

        if (confirmarSenha.getText().trim().equals("")) {
            alert("Atenção", "Confirme sua senha.");
            return false;
        }

        if (!senha.getText().trim().equals(confirmarSenha.getText().trim())) {
            alert("Atenção", "As senhas não conferem.");
            return false;
        }

        return true;
    }

    private void alert(String titulo, String msg) {
        if (this.getRoot() != null) {
            new Alerta().exibir(titulo, msg);
        } else {
            System.out.println("é nulo, meu caro!");
        }
    }

    private boolean TemDigitosExcessivos(String txt) {
        return txt.length() > 80;
    }

}
