package models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Pessoa {

    protected List<String> privilegios = new ArrayList<>();
    protected int id;
    protected String nome;
    protected String cpf;
    protected String email;
    protected String telefone;
    protected Date dataNascimento;
    protected char sexo;
    protected String cep;
    protected String cidade;
    protected String endereco;
    protected String bairro;

    public Pessoa(String nome, String cpf, String email, String telefone, Date dataNascimento, char sexo, String cep, String cidade, String endereco, String bairro) {
        this.nome = nome;
        this.cpf = cpf;
        this.email = email;
        this.telefone = telefone;
        this.dataNascimento = dataNascimento;
        this.sexo = sexo;
        this.cep = cep;
        this.cidade = cidade;
        this.endereco = endereco;
        this.bairro = bairro;
    }

    public Pessoa() {
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the cpf
     */
    public String getCpf() {
        return cpf;
    }

    /**
     * @param cpf the cpf to set
     */
    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the telefone
     */
    public String getTelefone() {
        return telefone;
    }

    /**
     * @param telefone the telefone to set
     */
    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    /**
     * @return the dataNascimento
     */
    public Date getDataNascimento() {
        return dataNascimento;
    }

    /**
     * @param dataNascimento the dataNascimento to set
     */
    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    /**
     * @return the sexo
     */
    public char getSexo() {
        return sexo;
    }

    /**
     * @param sexo the sexo to set
     */
    public void setSexo(char sexo) {
        this.sexo = sexo;
    }

    /**
     * @return the cep
     */
    public String getCep() {
        return cep;
    }

    /**
     * @param cep the cep to set
     */
    public void setCep(String cep) {
        this.cep = cep;
    }

    /**
     * @return the cidade
     */
    public String getCidade() {
        return cidade;
    }

    /**
     * @param cidade the cidade to set
     */
    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    /**
     * @return the endereco
     */
    public String getEndereco() {
        return endereco;
    }

    /**
     * @param endereco the endereco to set
     */
    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    /**
     * @return the bairro
     */
    public String getBairro() {
        return bairro;
    }

    /**
     * @param bairro the bairro to set
     */
    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public void setId(int id) {
        this.id = id;
    }


    @Override
    public String toString() {
        return "Nome: " + nome
                + "\n CPF: " + cpf
                + "\n Email=" + email
                + "\n Telefone=" + telefone
                + "\n Data de Nascimento=" + dataNascimento
                + "\n Sexo=" + sexo
                + "\n CEP=" + cep
                + "\n Cidade=" + cidade
                + "\n Endereco=" + endereco
                + "\n Bairro=" + bairro;
    }

    protected void setID(int id) {
        this.id = id;
    }

    public int getID() {
        return this.id;
    }

    public void addPrivilegio(String s) {
        privilegios.add(s);
    }

    public List<String> getPrivilegios() {
        return privilegios;
    }

    public void cleanPrivilegios() {
        privilegios = new ArrayList<>();
    }

    public int calcularIdade() {
        Date dataNascimento = getDataNascimento();
        System.out.println(dataNascimento.toString());
        Date dataAtual = new Date();
        System.out.println(dataAtual.toString());
        int dataAtualI = dataAtual.getYear();
        int dataNascimentoI = dataNascimento.getYear();
        int idade = dataAtualI - dataNascimentoI - 1;
        if (dataNascimento.getMonth() <= dataAtual.getMonth() + 1
                && dataNascimento.getDate() <= dataAtual.getDate()) {
            idade++;
        }
        return idade;
    }
}
