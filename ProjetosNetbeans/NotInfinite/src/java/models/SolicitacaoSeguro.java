package models;

import java.util.*;

public class SolicitacaoSeguro {

    private int id;

    private long numeroSolicitacao;

    private Date dataSolicitacao;

    private float valorSolicitacao;

    private float valorPremio;

    private Date dataVisitaCandidato;

    private boolean aprovadaSolicitacao;

    private String motivoReprovacao;

    private boolean fumante;

    private boolean alcoolista;

    private Set<Predisposicoes> predisposicoes;

    private List<ProblemaSaude> problemasSaude;

    private Candidato candidato;

    private Pessoa alterador;

    private boolean isvalido;

    private int idSolicitacaoOriginal=-1;

    private Date dataAlteracao;

    public SolicitacaoSeguro(Candidato candidato) {
        this.candidato = candidato;
    }

    public void setMultiple(int nSol, Date dataSolicitacao, float valorSolicitacao, float valorPremio, boolean fumante, boolean alcoolista, Set<Predisposicoes> predisposicoes) {
        this.setNumeroSolicitacao(nSol);
        this.setDataSolicitacao(dataSolicitacao);
        this.setValorSolicitacao(valorSolicitacao);
        this.setValorPremio(valorPremio);
        this.setFumante(fumante);
        this.setAlcoolista(alcoolista);
        this.setPredisposicoes(predisposicoes);
        this.setProblemasSaude(new ArrayList<>());
    }

    public SolicitacaoSeguro(int nSol, Date dataSolicitacao, float valorSolicitacao, float valorPremio, boolean fumante, boolean alcoolista, Set<Predisposicoes> predisposicoes, Candidato c) {

        this.setNumeroSolicitacao(nSol);
        this.setDataSolicitacao(dataSolicitacao);
        this.setValorSolicitacao(valorSolicitacao);
        this.setValorPremio(valorPremio);
        this.setFumante(fumante);
        this.setAlcoolista(alcoolista);
        this.setPredisposicoes(predisposicoes);
        this.setProblemasSaude(new ArrayList<>());
        this.setCandidato(c);
    }

    public void adicionarProblema(ProblemaSaude p) {
        if(problemasSaude==null)
            problemasSaude=new ArrayList<>();
        getProblemasSaude().add(p);
    }

    public static int gerarNumeroSolicitacao(int dayCode) {
        Calendar c = Calendar.getInstance();
        int ano = c.get(Calendar.YEAR);
        int mes = c.get(Calendar.MONTH);
        int dia = c.get(Calendar.DAY_OF_MONTH);
        return Integer.valueOf("" + ano + "" + mes + "" + dia + "" + dayCode);
    }


    public String getNomeCandidato() {
        return getCandidato().getNome();
    }

    public Candidato getCandidato() {
        return candidato;

    }

    public long getNumeroSolicitacao() {
        return numeroSolicitacao;
    }

    public Date getDataSolicitacao() {
        return dataSolicitacao;
    }

    public float getValorSolicitacao() {
        return valorSolicitacao;
    }

    public Date getDataVisitaCandidato() {
        return dataVisitaCandidato;
    }

    public boolean isAprovadaSolicitacao() {
        return aprovadaSolicitacao;
    }

    public String getMotivoReprovacao() {
        return motivoReprovacao;
    }

    public Date getDataAlteracao() {
        return dataAlteracao;
    }

    public boolean isFumante() {
        return fumante;
    }

    public boolean isAlcoolista() {
        return alcoolista;
    }

    public Set<Predisposicoes> getPredisposicoes() {
        return predisposicoes;
    }

    public List<ProblemaSaude> getProblemasSaude() {
        return problemasSaude;
    }

    public void setMotivoReprovacao(String motivoReprovacao) {
        this.motivoReprovacao = motivoReprovacao;
    }

    public void setDataAlteracao(Date dataAlteracao) {
        this.dataAlteracao = dataAlteracao;
    }

    public void setAprovadaSolicitacao(boolean aprovadaSolicitacao) {
        this.aprovadaSolicitacao = aprovadaSolicitacao;
    }

    public void setDataVisitaCandidato(Date dataVisitaCandidato) {
        this.dataVisitaCandidato = dataVisitaCandidato;
    }

    public void setDataVisitaCandidato(int dia, int mes, int ano, int hora, int minuto) {
        if (getDataVisitaCandidato() == null) {
            setDataVisitaCandidato(new Date());
        }
        getDataVisitaCandidato().setDate(dia);
        getDataVisitaCandidato().setMonth(mes);
        getDataVisitaCandidato().setYear(ano);
        getDataVisitaCandidato().setHours(hora);
        getDataVisitaCandidato().setMinutes(minuto);
    }

    public float getValorPremio() {
        return valorPremio;
    }


    public void reprovar(String motivo) {
        this.setAprovadaSolicitacao(false);
        this.setMotivoReprovacao(motivo);
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }


    public void setNumeroSolicitacao(long numeroSolicitacao) {
        this.numeroSolicitacao = numeroSolicitacao;
    }

    public void setDataSolicitacao(Date dataSolicitacao) {
        this.dataSolicitacao = dataSolicitacao;
    }

    public void setValorSolicitacao(float valorSolicitacao) {
        this.valorSolicitacao = valorSolicitacao;
    }

    public void setValorPremio(float valorPremio) {
        this.valorPremio = valorPremio;
    }


    public void setFumante(boolean fumante) {
        this.fumante = fumante;
    }

    public void setAlcoolista(boolean alcoolista) {
        this.alcoolista = alcoolista;
    }

    public void setPredisposicoes(Set<Predisposicoes> predisposicoes) {
        this.predisposicoes = predisposicoes;
    }

    public void setProblemasSaude(List<ProblemaSaude> problemasSaude) {
        this.problemasSaude = problemasSaude;
    }

    public void setCandidato(Candidato candidato) {
        this.candidato = candidato;
    }


    public int calcularIdade() {
       return candidato.calcularIdade();
    }

    public float calcularValorPremio(float valorDesejado, int qntddPredisposicoes, boolean isFumante, boolean isAlcoolista) {
        int idade = calcularIdade();
        float valorPremio = 0;
        if (idade >= 18 && idade <= 25) {
            if (candidato.getSexo() == 'M') {
                valorPremio = 617.00f;
            } else {
                valorPremio = 535.50f;
            }
        } else if (idade >= 26 && idade <= 35) {
            if (candidato.getSexo() == 'M') {
                valorPremio = 585.80f;
            } else {
                valorPremio = 516.20f;
            }
        } else if (idade >= 36 && idade <= 45) {
            if (candidato.getSexo() == 'M') {
                valorPremio = 516.95f;
            } else {
                valorPremio = 578.95f;
            }
        } else if (idade >= 46 && idade <= 50) {
            if (candidato.getSexo() == 'M') {
                valorPremio = 557.00f;
            } else {
                valorPremio = 482.17f;
            }
        } else if (idade >= 51 && idade <= 55) {
            if (candidato.getSexo() == 'M') {
                valorPremio = 589.94f;
            } else {
                valorPremio = 488.90f;
            }
        } else if (idade >= 56 && idade <= 60) {
            if (candidato.getSexo() == 'M') {
                valorPremio = 624.90f;
            } else {
                valorPremio = 524.30f;
            }
        } else if (idade >= 61 && idade <= 65) {
            if (candidato.getSexo() == 'M') {
                valorPremio = 656.66f;
            } else {
                valorPremio = 544.41f;
            }
        } else if (idade >= 66 && idade <= 75) {
            if (candidato.getSexo() == 'M') {
                valorPremio = 687.00f;
            } else {
                valorPremio = 581.50f;
            }
        } else if (idade >= 76 && idade <= 85) {
            if (candidato.getSexo() == 'M') {
                valorPremio = 714.32f;
            } else {
                valorPremio = 644.12f;
            }
        } else if (idade >= 86 && idade <= 90) {
            if (candidato.getSexo() == 'M') {
                valorPremio = 851.60f;
            } else {
                valorPremio = 792.70f;
            }
        }

        float valorAcrecimadoPeloValorDesejado = 0;
        if (valorDesejado > 150000) {
            valorDesejado -= 150000;
            valorAcrecimadoPeloValorDesejado = (float) (Math.ceil(valorDesejado / 25000) * (valorPremio * 0.1));
            System.out.println("valor base: " + valorDesejado / 25000 + ", fator multiplicador: " + Math.ceil(valorDesejado / 25000));
        }
        valorPremio += valorAcrecimadoPeloValorDesejado;
        valorPremio += qntddPredisposicoes * 31.64f;
        if (isFumante) {
            valorPremio += 296.36;
        }
        if (isAlcoolista) {
            valorPremio += 203.09;
        }

        return valorPremio;
    }


    public String getValorSolicitacaoString() {
        return String.format("R$%#.2f", valorSolicitacao);
    }

    public String getDataSolicitacaoString() {
        int ano = dataSolicitacao.getYear() + 1900;
        int mes = dataSolicitacao.getMonth() + 1;
        int dia = dataSolicitacao.getDate();
        return dia + "/" + mes + "/" + ano;
    }

    public String getValorPremioString() {
        return String.format("R$%#.2f", valorPremio);
    }

    public void aprovar() {
        this.aprovadaSolicitacao = true;
    }

    public Pessoa getAlterador() {
        return alterador;
    }

    public void setAlterador(Pessoa alterador) {
        this.alterador = alterador;
    }

    public boolean isValido() {
        return isvalido;
    }

    public void setIsvalido(boolean isvalido) {
        this.isvalido = isvalido;
    }

    public int getIdSolicitacaoOriginal() {
        return idSolicitacaoOriginal;
    }

    public void setIdSolicitacaoOriginal(int idSolicitacaoOriginal) {
        this.idSolicitacaoOriginal = idSolicitacaoOriginal;
    }

    public void addPredisposicao(Predisposicoes predisposicao) {
        if(predisposicoes==null)
            predisposicoes=new HashSet<>();
        if(!predisposicoes.contains(predisposicao))
            predisposicoes.add(predisposicao);
    }

    public float calcularValorPremio(boolean inplace) {
        int predSize=0;
        if(predisposicoes!=null)
            predSize=predisposicoes.size();
        float retorno=calcularValorPremio(valorSolicitacao, predSize, isFumante(), isAlcoolista());
        if (inplace)
            this.valorPremio=retorno;
        return retorno;
    }

    public Pair<Boolean, String> podeSerAprovada() {

        if(candidato.getBeneficiarios()==null || candidato.getBeneficiarios().size()<1){
            return new Pair<>(false, "O candidato não possui beneficiarios cadastrados");
        }
        if(calcularIdade()<18){
            return new Pair<>(false, "O candidato não possui a idade minima para efetuar um seguro (18 anos)");
        }
        if(calcularIdade()>90){
            return new Pair<>(false, "O candidato já passou da idade maxima para efetuar um seguro (90 anos)");
        }
        if(predisposicoes!=null && predisposicoes.size()>=4){
            return new Pair<>(false, "O candidato possui 4 ou mais predisposições");
        }
        if (predisposicoes!=null && predisposicoes.size()>=2 && isFumante() && isAlcoolista()){
            return new Pair<>(false, "O candidato é alcoolista e fumante, além de possuir 2 ou mais predisposições");
        }
        return new Pair<>(true, "");

    }
    public String getNomeAlterador(){
        return alterador==null ? "":alterador.getNome();
    }

    public String getIsIsvalida() {
        return isvalido?"Sim":"Não";
    }
}

