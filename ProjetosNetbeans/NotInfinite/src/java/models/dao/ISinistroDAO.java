/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.dao;

import java.sql.SQLException;
import java.util.List;
import models.Contato;
import models.Sinistro;

/**
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
interface ISinistroDAO {
    
    /**
     * Registrar Morte
     * 
     * @param sinistro
     * @param contato
     * @throws SQLException 
     */
    void registarSinistro(Sinistro sinistro) throws SQLException;
    
    /**
     * Pegar sinistros
     * 
     * @return
     * @throws SQLException 
     */
    List<Sinistro> buscarSinistros() throws SQLException;
    
    /**
     * Avaliar sinistro
     * 
     * @param sinistro
     * @param parecer
     * @param autorizado
     * @throws SQLException 
     */
    void avaliarSinistro(Sinistro sinistro, int avaliador, String parecer, boolean autorizado) throws SQLException;
    
}
