/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.dao;

import java.sql.SQLException;
import java.util.List;

import models.SolicitacaoSeguro;

/**
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public interface ISolicitacaoSeguroDAO {

    void atualizarSolicitacao(SolicitacaoSeguro solicitacaoSeguro) throws SQLException;

    void registrarSolicitacao(SolicitacaoSeguro solicitacaoSeguro) throws SQLException;
    
    SolicitacaoSeguro getSolicitacaoPorCandidato(int idPessoa) throws SQLException;

    void salvarAlteracoesNaSolicitacao(SolicitacaoSeguro solicitacaoSeguro) throws SQLException;

    List<SolicitacaoSeguro> getSolicitacoesSeguroComConsultaMarcadaNaoAvaliada() throws SQLException;

    List<SolicitacaoSeguro> getSolicitacoesSemDataDeConsultaMarcada() throws SQLException;
}
