/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.dao;

import exceptions.NotFoundOnDatabaseException;
import java.sql.SQLException;
import models.ApoliceSeguro;
import models.SolicitacaoSeguro;

/**
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
interface IApoliceDAO {
    
    ApoliceSeguro registrarApolice(SolicitacaoSeguro seguro, int idCartao) throws SQLException;
    // adiciona oq vc precisa aq
    String consultarPorNumero(long numero) throws SQLException;
    
    ApoliceSeguro recuperarApolice(long numeroApolice, String nomeCompleto) throws SQLException, NotFoundOnDatabaseException;
}
