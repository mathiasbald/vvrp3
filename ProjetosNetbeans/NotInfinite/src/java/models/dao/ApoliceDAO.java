/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.dao;

import exceptions.NotFoundOnDatabaseException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Date;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.ApoliceSeguro;
import models.Session;
import models.SolicitacaoSeguro;

/**
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public class ApoliceDAO extends DAO implements IApoliceDAO {

    @Override
    public String consultarPorNumero(long numero) throws SQLException {
        conexao.abrirConexao();
        String resultado = null;
//        Statement stmt = conexao.prepararDeclaracao();
//        
//        ResultSet result = stmt.executeQuery("SELECT pessoas.nome "
//                + "FROM apolices, segurados, pessoas "
//                + "WHERE apolices.numero = ? "
//                + "AND segurados.idApolice = apolices.idApolice "
//                + "AND segurados.idUsuario = pessoas.idUsuario");
//
//        resultado = result.getString(1);
//        stmt.close();

        String query = "SELECT pessoas.nome "
                + "FROM apolices, segurados, pessoas "
                + "WHERE apolices.numero = ? "
                + "AND segurados.idApolice = apolices.idApolice "
                + "AND segurados.idUsuario = pessoas.idUsuario";

        PreparedStatement pstmt = conexao.prepararDeclaracao(query);
        pstmt.setLong(1, numero);
        ResultSet rs = pstmt.executeQuery();
        if (rs != null && rs.next()) {
            resultado = rs.getString("nome");
        }

        conexao.fecharConexao();
        return resultado;
    }

    @Override
    public ApoliceSeguro recuperarApolice(long numeroApolice, String nomeCompleto) throws SQLException, NotFoundOnDatabaseException {
        conexao.abrirConexao();

        ApoliceSeguro apolice;

        String query = "SELECT apolices.id, apolices.numeroapolice, apolices.datacontratacao, apolices.premio, apolices.valorsinistromorte, apolices.valorsinistroincapacitado, apolices.ativo "
                + "FROM apolices, pessoas, usuarios, vinculo "
                + "WHERE BINARY apolices.numeroapolice = ? "
                + "AND pessoas.nome = ? "
                + "AND vinculo.idtipo = (SELECT tipopessoa.id FROM tipopessoa WHERE tipopessoa.descricao = 'Segurado') "
                + "AND pessoas.id = usuarios.idpessoa "
                + "AND usuarios.idpessoa = apolices.idcandidato "
                + "LIMIT 1--";

        PreparedStatement stmt = conexao.prepararDeclaracao(query);
        stmt.setLong(1, numeroApolice);
        stmt.setString(2, nomeCompleto);

        ResultSet rs = stmt.executeQuery();

        if (!rs.first()) {
            throw new NotFoundOnDatabaseException("Número da apólice e/ou nome do segurado estão incorretos.\nPor favor, verifique e tente novamente.");
        } else if (!rs.getBoolean("ativo")) {
            throw new NotFoundOnDatabaseException("Esta apólice encontra-se inativa.\nPor favor, entre em contato com a seguradora para mais detalhes.");
        }

        apolice = new ApoliceSeguro(rs.getLong("numeroapolice"), rs.getDate("datacontratacao"), rs.getDouble("premio"), rs.getDouble("valorsinistromorte"), rs.getDouble("valorsinistroincapacitado"));
        apolice.setId(rs.getInt("id"));

        conexao.fecharConexao();
        return apolice;
    }

    @Override
    public ApoliceSeguro registrarApolice(SolicitacaoSeguro seguro, int idCartao) throws SQLException {
        conexao.abrirConexao();
        PreparedStatement stmt = null;
        
        long numeroApolice = seguro.getNumeroSolicitacao();
        Calendar c = Calendar.getInstance();
        java.util.Date datacontratacao = c.getTime();
        
        double valorApolice = seguro.getValorPremio();
        double valorSinistro = seguro.getValorSolicitacao();
        double valorIncapacitado = seguro.getValorSolicitacao() * 0.20;
        
        ApoliceSeguro as = new ApoliceSeguro(numeroApolice, datacontratacao, valorApolice, valorSinistro, valorIncapacitado);
        
        String sql = "INSERT INTO apolices (numeroapolice, datacontratacao, premio, valorsinistromorte, valorsinistroincapacitado, ativo, idcandidato, idcartaocredito, solicitacaooriginal) "
                + "VALUES(?,?,?,?,?,?,?,?,?)";

        stmt = conexao.prepararDeclaracao(sql, Statement.RETURN_GENERATED_KEYS);
        stmt.setLong(1,as.getNumeroApolice());
        stmt.setDate(2, new Date(as.getDataContratacaoApolice().getTime()));
        stmt.setDouble(3, as.getPremioApolice());
        stmt.setDouble(4, as.getValorSinistroMorte());
        stmt.setDouble(5, as.getValorSinistoIncapacitado());
        stmt.setBoolean(6, true);
        stmt.setInt(7, Session.getInstance().getPessoa().getID());
        stmt.setInt(8, idCartao);
        stmt.setInt(9, seguro.getId());

        stmt.execute();

        ResultSet rs = stmt.getGeneratedKeys();

        int idApoliceGerada = 0;

        if (rs.next()) {
            idApoliceGerada = rs.getInt(1);
        }

        conexao.fecharConexao();
        
        as.setId(idApoliceGerada);

        return as;

    }
    
    public void destruir(){
        try {
            this.finalize();
            System.gc();
        } catch (Throwable ex) {
            Logger.getLogger(ApoliceDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
