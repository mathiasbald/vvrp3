/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import models.CartaoCredito;

/**
 *
 * @author Michael Martins <michaelmartins096@gmail.com>
 */
public class CartaoCreditoDAO extends DAO implements ICartaoCreditoDAO {

    @Override
    public int registrarCartaoCredito(int idBandeira, CartaoCredito cartaoCredito) throws SQLException {

        int idCartaoExistente = getIdCartaoCredito(cartaoCredito.getNumero());
        
        if (idCartaoExistente == 0) {
            conexao.abrirConexao();
            PreparedStatement stmt = null;

            String sql = "INSERT INTO cartaocredito (numero, datavencimento, nome, codigoseguranca, idbandeira) "
                    + "VALUES(?, ?, ?, ?, ?)";

            stmt = conexao.prepararDeclaracao(sql, Statement.RETURN_GENERATED_KEYS);
            stmt.setLong(1, cartaoCredito.getNumero());
            stmt.setDate(2, new Date(cartaoCredito.getVencimento().getTime()));
            stmt.setString(3, cartaoCredito.getNome());
            stmt.setInt(4, cartaoCredito.getCvv());
            stmt.setInt(5, idBandeira);

            stmt.execute();
            
            ResultSet rs = stmt.getGeneratedKeys();

            int idCartaoCriado = 0;

            if (rs.next()) {
                idCartaoCriado = rs.getInt(1);
            }
            
            conexao.fecharConexao();
            return idCartaoCriado;
        }
        return idCartaoExistente;
    }

    @Override
    public int getIdCartaoCredito(long numeroCartao) throws SQLException {
        conexao.abrirConexao();
        PreparedStatement stmt = null;
        int idCartaoExistente = 0;

        String sql = "SELECT cartaocredito.id FROM cartaocredito WHERE cartaocredito.numero = ? LIMIT 1";

        stmt = conexao.prepararDeclaracao(sql);
        stmt.setLong(1, numeroCartao);

        ResultSet rs = stmt.executeQuery();

        if (rs.first()) {
            idCartaoExistente = rs.getInt("id");
        } 

        stmt.execute();

        conexao.fecharConexao();
        return idCartaoExistente;

    }

}
