/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.dao;

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;
import exceptions.NotFoundOnDatabaseException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import static java.sql.Statement.RETURN_GENERATED_KEYS;
import java.sql.Types;
import java.util.*;

import models.*;

/**
 * @author Gustavo Bittecourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public class SolicitacaoSeguroDAO extends DAO implements ISolicitacaoSeguroDAO {

    @Override
    public void atualizarSolicitacao(SolicitacaoSeguro solicitacaoSeguro) throws SQLException {
        conexao.abrirConexao();
        PreparedStatement stmt = conexao.prepararDeclaracao(
                "UPDATE solicitacoes SET datavisitacandidato=?, "
                + "aprovadasolicitacao=?, "
                + "motivoreprovacao=?    "
                + "WHERE id=?"
        );

        if (solicitacaoSeguro.getDataVisitaCandidato() == null) {
            stmt.setNull(1, Types.DATE);
        } else {
            Date d = solicitacaoSeguro.getDataVisitaCandidato();
            if (d.getYear() < 1900) {
                stmt.setString(1, (d.getYear() + 1900) + "-" + (d.getMonth() + 1) + "-" + d.getDate() + " " + d.getHours() + ":" + d.getMinutes() + ":00");
            } else {
                stmt.setString(1, (d.getYear()) + "-" + (d.getMonth() + 1) + "-" + d.getDate() + " " + d.getHours() + ":" + d.getMinutes() + ":00");
            }
        }
        if (!solicitacaoSeguro.isAprovadaSolicitacao() && solicitacaoSeguro.getMotivoReprovacao() == null) {
            stmt.setNull(2, Types.BOOLEAN);
            stmt.setNull(3, Types.VARCHAR);
        } else {
            stmt.setBoolean(2, solicitacaoSeguro.isAprovadaSolicitacao());
            if (solicitacaoSeguro.getMotivoReprovacao() == null) {
                stmt.setNull(3, Types.VARCHAR);
            } else {
                stmt.setString(3, solicitacaoSeguro.getMotivoReprovacao());
            }
        }
        stmt.setInt(4, solicitacaoSeguro.getId());
        stmt.execute();

        conexao.fecharConexao();
    }

    @Override
    public void salvarAlteracoesNaSolicitacao(SolicitacaoSeguro solicitacaoSeguro) throws SQLException {
        conexao.abrirConexao();
        PreparedStatement stmt = conexao.prepararDeclaracao(
                "INSERT INTO solicitacoes ( datasolicitacao, valorpremio, valorsolicitacao, fumante, alcoolista, idpessoa,"
                + "datavisitacandidato, aprovadasolicitacao, motivoreprovacao, "
                + "isvalido, idalterador, idsolicitacaooriginal, dataalteracao, numerosolicitacao) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
                Statement.RETURN_GENERATED_KEYS);

        Date data = solicitacaoSeguro.getDataSolicitacao();

        stmt.setString(1, (data.getYear() + 1900) + "-" + (data.getMonth() + 1) + "-" + data.getDate());
        stmt.setDouble(2, solicitacaoSeguro.getValorPremio());
        stmt.setDouble(3, solicitacaoSeguro.getValorSolicitacao());
        stmt.setBoolean(4, solicitacaoSeguro.isFumante());
        stmt.setBoolean(5, solicitacaoSeguro.isAlcoolista());
        stmt.setInt(6, solicitacaoSeguro.getCandidato().getID());

        if (solicitacaoSeguro.getDataVisitaCandidato() == null) {
            stmt.setNull(7, Types.DATE);
        } else {
            Date d = solicitacaoSeguro.getDataVisitaCandidato();
            stmt.setString(7, (d.getYear() + 1900) + "-" + (d.getMonth() + 1) + "-" + d.getDate() + " " + d.getHours() + ":" + d.getMinutes() + ":00");
        }
        if (!solicitacaoSeguro.isAprovadaSolicitacao() && solicitacaoSeguro.getMotivoReprovacao() == null) {
            stmt.setNull(8, Types.BOOLEAN);
            stmt.setNull(9, Types.VARCHAR);
        } else {
            stmt.setBoolean(8, solicitacaoSeguro.isAprovadaSolicitacao());
            if (solicitacaoSeguro.getMotivoReprovacao() == null) {
                stmt.setNull(9, Types.VARCHAR);
            } else {
                stmt.setString(9, solicitacaoSeguro.getMotivoReprovacao());
            }
        }

        stmt.setBoolean(10, true);
        if (solicitacaoSeguro.getAlterador() == null) {
            stmt.setNull(11, Types.INTEGER);
        } else {
            stmt.setInt(11, solicitacaoSeguro.getAlterador().getID());
        }
        if (solicitacaoSeguro.getIdSolicitacaoOriginal() <= 0) {
            stmt.setInt(12, solicitacaoSeguro.getId());
        } else {
            stmt.setInt(12, solicitacaoSeguro.getIdSolicitacaoOriginal());
        }

        Date d = new Date();
        stmt.setString(13, (d.getYear() + 1900) + "-" + (d.getMonth() + 1) + "-" + d.getDate());
        stmt.setLong(14, solicitacaoSeguro.getNumeroSolicitacao());
        stmt.execute();
        ResultSet rs = stmt.getGeneratedKeys();
        rs.first();
        int idSol = rs.getInt(1);
        int idSolAntiga = solicitacaoSeguro.getId();
        solicitacaoSeguro.setId(idSol);
        stmt.close();

        for (ProblemaSaude problemaSaude : solicitacaoSeguro.getProblemasSaude()) {
            PreparedStatement stmtP = conexao.prepararDeclaracao(
                    "INSERT INTO problemasaude (nome, descricao, idsolicitacao)"
                    + "VALUES (?, ?, ?)");
            stmtP.setString(1, problemaSaude.getNomeProblema());
            stmtP.setString(2, problemaSaude.getDescricaoProblema());
            stmtP.setInt(3, idSol);
            stmtP.executeUpdate();
            stmtP.close();
        }
        for (Predisposicoes pred : solicitacaoSeguro.getPredisposicoes()) {
            PreparedStatement stmtS = conexao.prepararDeclaracao(
                    "select id from predisposicoes where nome like ?");
            stmtS.setString(1, "%" + pred.toString() + "%");

            ResultSet resultado = stmtS.executeQuery();
            if (resultado.first()) {
                int idPred = resultado.getInt(1);
                PreparedStatement stmtP = conexao.prepararDeclaracao(
                        "INSERT INTO sol_pred(idpred, idsolicitacao)"
                        + "VALUES (?, ?)");
                stmtP.setInt(1, idPred);
                stmtP.setInt(2, idSol);
                stmtP.executeUpdate();
                stmtS.close();
                stmtP.close();
            }
        }

        PreparedStatement stmt2 = conexao.prepararDeclaracao("update solicitacoes set isvalido=? where solicitacoes.id=?");
        stmt2.setBoolean(1, false);
        stmt2.setInt(2, idSolAntiga);
        stmt2.execute();

    }

    @Override
    public void registrarSolicitacao(SolicitacaoSeguro solicitacaoSeguro) throws SQLException {
        conexao.abrirConexao();

        PreparedStatement stmtCountSolsOfDay = conexao.prepararDeclaracao("select count(id) as c from solicitacoes where solicitacoes.datasolicitacao=?");
        Date data = solicitacaoSeguro.getDataSolicitacao();

        stmtCountSolsOfDay.setString(1, (data.getYear() + 1900) + "-" + (data.getMonth() + 1) + "-" + data.getDate());
        ResultSet rsCountSolsOfDay = stmtCountSolsOfDay.executeQuery();

        long totalSolsOfDay = 0;
        if (rsCountSolsOfDay.first()) {
            totalSolsOfDay = rsCountSolsOfDay.getLong("c");
        }

        String nSolicitacao = (data.getYear() + 1900) + "" + String.format("%02d", (data.getMonth() + 1)) + "" + data.getDate() + String.format("%04d", totalSolsOfDay);
        System.out.println(nSolicitacao + "");
        PreparedStatement stmt = conexao.prepararDeclaracao(
                "INSERT INTO solicitacoes ( datasolicitacao, valorpremio, valorsolicitacao, fumante, alcoolista, idpessoa, numerosolicitacao)"
                + "VALUES ( ?, ?, ?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);

        //stmt.setLong(1, solicitacaoSeguro.getCandidato().getID());
        stmt.setString(1, (data.getYear() + 1900) + "-" + (data.getMonth() + 1) + "-" + data.getDate());
        stmt.setDouble(2, solicitacaoSeguro.getValorPremio());
        stmt.setDouble(3, solicitacaoSeguro.getValorSolicitacao());
        stmt.setBoolean(4, solicitacaoSeguro.isFumante());
        stmt.setBoolean(5, solicitacaoSeguro.isAlcoolista());
        stmt.setInt(6, solicitacaoSeguro.getCandidato().getID());
        stmt.setString(7, nSolicitacao);
        stmt.executeUpdate();
        ResultSet rs = stmt.getGeneratedKeys();
        rs.first();
        int idSol = rs.getInt(1);
        stmt.close();
        if (solicitacaoSeguro.getProblemasSaude() != null) {
            for (ProblemaSaude problemaSaude : solicitacaoSeguro.getProblemasSaude()) {
                PreparedStatement stmtP = conexao.prepararDeclaracao(
                        "INSERT INTO problemasaude (nome, descricao, idsolicitacao)"
                        + "VALUES (?, ?, ?)");
                stmtP.setString(1, problemaSaude.getNomeProblema());
                stmtP.setString(2, problemaSaude.getDescricaoProblema());
                stmtP.setInt(3, idSol);
                stmtP.executeUpdate();
                stmtP.close();
            }
        }
        if (solicitacaoSeguro.getPredisposicoes() != null) {
            for (Predisposicoes pred : solicitacaoSeguro.getPredisposicoes()) {
                PreparedStatement stmtS = conexao.prepararDeclaracao(
                        "select id from predisposicoes where nome like ?");
                stmtS.setString(1, "%" + pred.toString() + "%");

                ResultSet resultado = stmtS.executeQuery();
                if (resultado.first()) {
                    int idPred = resultado.getInt(1);
                    PreparedStatement stmtP = conexao.prepararDeclaracao(
                            "INSERT INTO sol_pred(idpred, idsolicitacao)"
                            + "VALUES (?, ?)");
                    stmtP.setInt(1, idPred);
                    stmtP.setInt(2, idSol);
                    stmtP.executeUpdate();
                    stmtS.close();
                    stmtP.close();
                }
            }
        }
        conexao.fecharConexao();
    }

    private List<SolicitacaoSeguro> select(PreparedStatement stmt) throws SQLException {

        conexao.abrirConexao();
        List<SolicitacaoSeguro> retorno = new ArrayList<>();

        ResultSet rsSol = stmt.executeQuery();
        while (rsSol.next()) {
            Set<Predisposicoes> predisposicoes = new HashSet<>();
            PreparedStatement stmtPreds = conexao.prepararDeclaracao("select idpred from sol_pred where idsolicitacao=?");
            stmtPreds.setInt(1, rsSol.getInt("id"));
            stmtPreds.executeQuery();
            ResultSet rsPreds = stmtPreds.executeQuery();

            while (rsPreds.next()) {
                PreparedStatement stmtInterno = conexao.prepararDeclaracao("select nome from predisposicoes where id=?");
                stmtInterno.setInt(1, rsPreds.getInt("idpred"));
                stmtInterno.executeQuery();
                ResultSet rs3 = stmtInterno.executeQuery();
                rs3.first();
                predisposicoes.add(Predisposicoes.getPredisposicao(rs3.getString("nome")));
            }

            Candidato candidato = new CandidatoDAO().getCandidatoById(rsSol.getInt("idpessoa"));

            SolicitacaoSeguro s = new SolicitacaoSeguro(rsSol.getInt("id"),
                    rsSol.getDate("datasolicitacao"),
                    rsSol.getFloat("valorsolicitacao"),
                    rsSol.getFloat("valorpremio"),
                    rsSol.getBoolean("fumante"),
                    rsSol.getBoolean("alcoolista"),
                    predisposicoes,
                    candidato);

            System.out.println("dao: " + rsSol.getFloat("valorsolicitacao"));

            s.setId(rsSol.getInt("id"));
            s.setNumeroSolicitacao(rsSol.getLong("numerosolicitacao"));
            java.sql.Timestamp date = rsSol.getTimestamp("datavisitacandidato");
            s.setDataVisitaCandidato(date);
            System.out.println(s.getDataVisitaCandidato());
            s.setAprovadaSolicitacao(rsSol.getBoolean("aprovadasolicitacao"));
            s.setMotivoReprovacao(rsSol.getString("motivoreprovacao"));

            s.setIsvalido(rsSol.getBoolean("isvalido"));
            int idalterador = rsSol.getInt("idalterador");
            if (idalterador > 0) {
                s.setAlterador(new PessoaDAO().getPessoaPorId(idalterador));
            }
            s.setIdSolicitacaoOriginal(rsSol.getInt("idsolicitacaooriginal"));
            s.setDataAlteracao(rsSol.getDate("dataalteracao"));

            PreparedStatement stmtProbs = conexao.prepararDeclaracao("select nome, descricao from problemasaude where idsolicitacao=?");
            stmtProbs.setInt(1, rsSol.getInt("id"));
            stmtProbs.executeQuery();
            ResultSet rsProbs = stmtProbs.executeQuery();
            while (rsProbs.next()) {
                s.adicionarProblema(new ProblemaSaude(rsProbs.getString("nome"), rsProbs.getString("descricao")));
            }

            retorno.add(s);
        }
        return retorno;
    }

    @Override
    public List<SolicitacaoSeguro> getSolicitacoesSeguroComConsultaMarcadaNaoAvaliada() throws SQLException {
        conexao.abrirConexao();
        PreparedStatement stmt = conexao.prepararDeclaracao("select * from solicitacoes "
                + "where datavisitacandidato is NOT NULL "
                + "and aprovadasolicitacao is NULL "
                + "and isvalido=true;");

        return select(stmt);

    }

    @Override
    public List<SolicitacaoSeguro> getSolicitacoesSemDataDeConsultaMarcada() throws SQLException {

        conexao.abrirConexao();
        PreparedStatement stmt = conexao.prepararDeclaracao("select * from solicitacoes "
                + "where datavisitacandidato is NULL "
                + "and aprovadasolicitacao is NULL "
                + "and isvalido=true;");

        return select(stmt);
    }

    @Override
    public SolicitacaoSeguro getSolicitacaoPorCandidato(int idPessoa) throws SQLException {
        conexao.abrirConexao();
        PreparedStatement stmt = null;

        String sql = "SELECT * "
                + "FROM solicitacoes "
                + "WHERE solicitacoes.idpessoa =? "
                + "AND solicitacoes.aprovadasolicitacao = true "
                + "AND solicitacoes.isvalido = true "
                + "LIMIT 1";

        stmt = conexao.prepararDeclaracao(sql);
        stmt.setInt(1, idPessoa);

        return select(stmt).get(0);
    }

    public boolean temSolicitacaoAprovada(int idPessoa) throws SQLException {
        conexao.abrirConexao();
        boolean result = false;
        PreparedStatement stmt = null;
        String sql = "SELECT solicitacoes.id FROM solicitacoes WHERE solicitacoes.idpessoa = ? AND solicitacoes.aprovadasolicitacao = true AND solicitacoes.isvalido = true";
        stmt = conexao.prepararDeclaracao(sql, RETURN_GENERATED_KEYS);
        stmt.setInt(1, idPessoa);
        stmt.execute();

        ResultSet rs = stmt.executeQuery();

        if (rs.next()) {
            result = true;
        }
        conexao.fecharConexao();
        return result;
    }

    public List<SolicitacaoSeguro> getSolicitacoesCandidato(Pessoa pessoa) throws SQLException {
        conexao.abrirConexao();

        PreparedStatement stmt = conexao.prepararDeclaracao("select * from solicitacoes "
                + "where isvalido=true and idpessoa=?;");
        stmt.setInt(1, pessoa.getID());
        return select(stmt);
    }

    public List<SolicitacaoSeguro> getHistoricoSolicitacoes(SolicitacaoSeguro selecionadaFase1) throws SQLException {
        conexao.abrirConexao();

        PreparedStatement stmt = conexao.prepararDeclaracao("select * from solicitacoes "
                + "where solicitacoes.id=? or solicitacoes.idsolicitacaooriginal=?");

        if (selecionadaFase1.getIdSolicitacaoOriginal() < 1) {
            stmt.setInt(1, selecionadaFase1.getId());
            stmt.setInt(2, selecionadaFase1.getId());
        } else {
            stmt.setInt(1, selecionadaFase1.getIdSolicitacaoOriginal());
            stmt.setInt(2, selecionadaFase1.getIdSolicitacaoOriginal());
        }

        return select(stmt);

    }
}
