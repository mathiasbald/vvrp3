package models.dao;

import java.sql.SQLException;
import models.Pessoa;
import models.Usuario;

/**
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
interface ICadastroDAO {
    
    void adicionarUsuario(Usuario usuario, Pessoa pessoa) throws SQLException;
    
    void updateUsuario(Usuario usuario, Pessoa pessoa) throws SQLException;
    
    Usuario getUsuario(int idPessoa) throws SQLException;
}
