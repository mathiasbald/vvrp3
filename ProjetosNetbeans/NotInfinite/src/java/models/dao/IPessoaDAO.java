/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.dao;

import java.sql.SQLException;
import models.Pessoa;

/**
 *
 * @author martins
 */
interface IPessoaDAO {

    int adicionarPessoa(Pessoa pessoa) throws SQLException;

    int updatePessoa(Pessoa pessoa) throws SQLException;

    public Pessoa getPessoaPorId(int id) throws SQLException;
}
