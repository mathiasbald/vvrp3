/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.dao;

import java.sql.SQLException;
import java.util.List;
import models.ApoliceSeguro;
import models.Parcela;

/**
 *
 * @author Michael Martins <michaelmartins096@gmail.com>
 */
public interface IParcelaDAO {
    
    List<Parcela> getParcelas(ApoliceSeguro apoliceSeguro) throws SQLException;
    
    void registrarParcelas(ApoliceSeguro apoliceSeguro, int qntParcelas) throws SQLException;
    
}
