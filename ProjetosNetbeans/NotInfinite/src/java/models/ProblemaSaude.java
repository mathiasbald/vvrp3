package models;

public class ProblemaSaude {

    private String nomeProblema;
    private String descricaoProblema;
    private int id=-1;
    public ProblemaSaude(String nomeProblema, String descricaoProblema) {
        this.nomeProblema = nomeProblema;
        this.descricaoProblema = descricaoProblema;
    }

    public String getNomeProblema() {
        return nomeProblema;
    }

    public String getDescricaoProblema() {
        return descricaoProblema;
    }

    @Override
    public String toString() {
        return "Nome do Problema: " + nomeProblema +"\n"+
                "Descrição do Problema: " + descricaoProblema;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
