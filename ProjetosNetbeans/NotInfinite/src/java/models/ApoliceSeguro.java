/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.Date;

/**
 *
 * @author Gustavo Bittecourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public class ApoliceSeguro {

    private int id;

    private long numeroApolice;
    private Date dataContratacaoApolice;
    private double premioApolice;
    private double valorSinistroMorte;
    private double valorSinistoIncapacitado;

    private boolean ativo;

    private String nomeSegurado;

    public ApoliceSeguro(long numeroApolice, Date dataContratacaoApolice, double premioApolice, double valorSinistroMorte, double valorSinistoIncapacitado) {
        this.numeroApolice = numeroApolice;
        this.dataContratacaoApolice = dataContratacaoApolice;
        this.premioApolice = premioApolice;
        this.valorSinistroMorte = valorSinistroMorte;
        this.valorSinistoIncapacitado = valorSinistoIncapacitado;
        this.ativo = true;
    }

    public long getNumeroApolice() {
        return numeroApolice;
    }

    public void setNumeroApolice(long numeroApolice) {
        this.numeroApolice = numeroApolice;
    }



    public double getPremioApolice() {
        return premioApolice;
    }

    public void setPremioApolice(double premioApolice) {
        this.premioApolice = premioApolice;
    }

    public double getValorSinistroMorte() {
        return valorSinistroMorte;
    }

    public void setValorSinistroMorte(double valorSinistroMorte) {
        this.valorSinistroMorte = valorSinistroMorte;
    }

    public double getValorSinistoIncapacitado() {
        return valorSinistoIncapacitado;
    }

    public void setValorSinistoIncapacitado(double valorSinistoIncapacitado) {
        this.valorSinistoIncapacitado = valorSinistoIncapacitado;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void toggleAtivo() {
        this.ativo = !this.ativo;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public Date getDataContratacaoApolice() {
        return dataContratacaoApolice;
    }

    public void setDataContratacaoApolice(Date dataContratacaoApolice) {
        this.dataContratacaoApolice = dataContratacaoApolice;
    }

    public String getNomeSegurado() {
        return nomeSegurado;
    }

    public void setNomeSegurado(String nomeSegurado) {
        this.nomeSegurado = nomeSegurado;
    }

    @Override
    public String toString() {
        return "ApoliceSeguro{" + "id=" + id + ", numeroApolice=" + numeroApolice + ", dataContratacaoApolice=" + dataContratacaoApolice + ", premioApolice=" + premioApolice + ", valorSinistroMorte=" + valorSinistroMorte + ", valorSinistoIncapacitado=" + valorSinistoIncapacitado + ", ativo=" + ativo + ", nomeSegurado=" + nomeSegurado + '}';
    }
    
    

}
