package models;

import java.text.Normalizer;

public enum Predisposicoes {

    CANCER ("Câncer"),

    DIABETE ("Diabetes"),

    DEMENCIA ("Demência"),

    CORACAO ("Coração"),

    HIPERTENSAO ("Hipertensão"),

    PULMONAR ("Pulmonar"),

    OSTEOPOROSE ("Osteoporose"),

    DEGENERACAO ("Degeneração");

    private final String name;

    private Predisposicoes(String s) {
        name = s;
    }

    static public Predisposicoes getPredisposicao(String s) {
        s = s.toLowerCase();
        s = Normalizer.normalize(s, Normalizer.Form.NFD);
        s = s.replaceAll("[^\\p{ASCII}]", "");
        System.out.println(s);
        if (s.equals("cancer"))
            return CANCER;
        else if (s.equals("diabete") || s.equals("diabetes"))
            return DIABETE;
        else if (s.equals("demencia"))
            return DEMENCIA;
        else if (s.equals("coracao"))
            return CORACAO;
        else if (s.equals("hipertensao"))
            return HIPERTENSAO;
        else if (s.equals("pulmonar"))
            return PULMONAR;
        else if (s.equals("osteoporose"))
            return OSTEOPOROSE;
        else if (s.equals("degeneracao"))
            return DEGENERACAO;
        return null;
    }

    public String getNome(){
        return this.name;
    }
}
