/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package models;

/**
 * 
 * @author Michael Martins <michaelmartins096@gmail.com>
 */
public enum BandeiraCartao {
    AMERICANEXPRESS (1), DINESCLUB(2), VISA(3), MASTERCARD(4), ELO(5), DISCOVER(6);
    private int idBandeira;
    
    BandeiraCartao(int idBandeira){
        this.idBandeira = idBandeira;
    }

    public int getIdBandeira() {
        return idBandeira;
    }
    
}
