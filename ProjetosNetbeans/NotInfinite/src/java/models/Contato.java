/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import exceptions.InvalidValueException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Gustavo Bittencourt Satheler
 * <gustavo.satheler@alunos.unipampa.edu.br>
 * <gustavosatheler@gmail.com>
 */
public class Contato {
    
    private int idContato;
    
    private String nome;
    private String parentesco;
    private String telefone;
    private String email;

    public Contato() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) throws InvalidValueException {
        InvalidValueException.validarNomeCompleto(nome, "Informe o nome do contato.");
        this.nome = nome;
    }

    public String getParentesco() {
        return parentesco;
    }

    public void setParentesco(String parentesco) throws InvalidValueException {
        InvalidValueException.validarComboBox(parentesco, "Selecione um parentesco.");
        this.parentesco = parentesco;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) throws InvalidValueException {
        InvalidValueException.validarTelefone(telefone, "Informe um telefone válido.");
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) throws InvalidValueException {
        InvalidValueException.validarEmail(email, "Informe um email válido.");
        this.email = email;
    }

    public int getIdContato() {
        return idContato;
    }

    public void setIdContato(int idContato) {
        this.idContato = idContato;
    }
    
    public void destruir(){
        try {
            this.finalize();
            System.gc();
        } catch (Throwable ex) {
            Logger.getLogger(Contato.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
